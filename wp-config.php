<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'newtelier');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'master');
/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ymhhkwyv5u0igvlnbvpv6lewbdtmmy1tpg373bgmsq1quwnt0wnl6nvnrlob02db');
define('SECURE_AUTH_KEY',  'pac6vnikr52lkdgqa575z5opyir3a3jdbvpsfdlds6w7wzsoxgg2qczwirncququ');
define('LOGGED_IN_KEY',    's1shmbep09wb1bi52h8vjj0freqqmbh2jjrvcgaajqktcsqslwxopwlstqwy5uti');
define('NONCE_KEY',        'yeqko8rgn0w6o1xsfnssztw7zpxjnjwg9zg5duuphqshj8h5npfjpxte8c4fmqsh');
define('AUTH_SALT',        'pykfezk35jhlrkk1bgqtz8ogtyp0djeaphlpl0hproz9a4ksno6mij7f9nrb9nrx');
define('SECURE_AUTH_SALT', 'biunvzdabghay045axjqo0k2iym3b5culkufg3luwy8ygzt9r6tap6t01p9xchdh');
define('LOGGED_IN_SALT',   'zwflyctgminmmdlwaybwugbl68bcimq5lrisdgubs00swtwdioxyxgn0z85jyay0');
define('NONCE_SALT',       'yth0dabksiznlbzzqb0kq4voxkz1mxpafnz4kvd5yfrjt90kppibz2ulunbygesd');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp5b_';


define('WP_HOME','http://telier.local');
define('WP_SITEURL','http://telier.local');


/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
