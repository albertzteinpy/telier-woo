<?php

if (!defined('ABSPATH')) {
    exit;
}

class mbe_shipping_method extends WC_Shipping_Method
{

    private $found_rates;


    public function __construct()
    {
        $this->id = MBE_E_LINK_ID;
        $this->method_title = __('Mail Boxes', 'mail-boxes-etc');
        $this->method_description = __('Obtains  real time shipping rates and Print shipping labels via MBE Shipping API.', 'mail-boxes-etc');
        $this->helper = new Mbe_Shipping_Helper_Data();
        $this->logger = new Mbe_Shipping_Helper_Logger();
        $this->init();
    }

    // override to back compatibility with wooCommerce 2.0
    function is_available($package)
    {
        global $woocommerce;

        if (isset($woocommerce->cart->cart_contents_total) && isset($this->min_amount) && $this->min_amount && $this->min_amount > $woocommerce->cart->cart_contents_total) {
            return false;
        }

        $ship_to_countries = '';

        if ($this->availability == 'specific') {
            $ship_to_countries = $this->countries;
        }
        else {
            if (get_option('woocommerce_allowed_countries') == 'specific') {
                $ship_to_countries = get_option('woocommerce_specific_allowed_countries');
            }
        }

        if (is_array($ship_to_countries)) {
            if (!in_array($package['destination']['country'], $ship_to_countries)) {
                return false;
            }
        }

        return apply_filters('woocommerce_shipping_' . $this->id . '_is_available', true, $package);
    }

    private function init()
    {
        // Load the settings.
        $this->init_form_fields();
        $this->init_settings();

        add_action('woocommerce_update_options_shipping_' . $this->id, array($this, 'process_admin_options'));
    }

    public function admin_messages()
    {


        settings_errors('myplugin-notices');

    }

    public function process_admin_options()
    {


        $mbeDir = $this->helper->mbeCsvUploadDir();

        $fileFieldName = 'woocommerce_wf_mbe_shipping_shipments_csv';


        if (isset($_FILES[$fileFieldName]) && $_FILES[$fileFieldName] && $_FILES[$fileFieldName]['name'] && $_FILES[$fileFieldName]['tmp_name']) {
            $path = $_FILES[$fileFieldName]['name'];

            $ext = pathinfo($path, PATHINFO_EXTENSION);
            if ($ext == "csv") {
                $nowDate = new DateTime();
                $fileName = date_format($nowDate, 'Y-m-d_H-i-s') . '_' . 'mbe_csv.csv';
                $filePath = $mbeDir . $fileName;

                move_uploaded_file($_FILES[$fileFieldName]['tmp_name'], $filePath);

                // VALIDATE AND INSERT FILE
                $this->validateCsvFileAndInsert($filePath);

                //save configuration containing the file uploaded and saved into database
                //Configuration::updateValue("shipments_csv", $fileName);
                //get_home_url() .

                $_POST["woocommerce_wf_mbe_shipping_shipments_csv_file"] = $fileName;

            }
            else {
                WC_Admin_Settings::add_error(__('File upload error: only CSV format is allowed', 'mail-boxes-etc'));
            }
        }

        parent::process_admin_options();


        return false;
    }

    public function init_form_fields()
    {
        $this->form_fields = include('data-mbe-settings.php');

    }

    public function calculate_shipping($package = array())
    {
        if (isset($package['destination']) && isset($package['destination']['country'])) {
            if ($this->helper->isEnabled() && $this->helper->enabledCountry($package['destination']['country'])) {
                $carrier = new Mbe_Shipping_Model_Carrier();
                $rates = $carrier->collectRates($package);

                foreach ($rates as $r) {
                    $rate = array(
                        'id'    => $this->id . ':' . $r['method'],
                        'label' => $r['label'],
                        'cost'  => $r['price'],
                        //                'calc_tax' => 'per_item'
                    );
                    // Register the rate
                    $this->add_rate($rate);
                }
            }
        }
    }


    public function validateCsvFileAndInsert($filePath)
    {

        $helper = new Mbe_Shipping_Helper_Csv();

        $rates = $helper->readFile($filePath);

        //VALIDATE

        $errors = false;
        $i = 1;

        $allowedShipmentServicesArray = $this->helper->getAllowedShipmentServices();
        $maxShipmentWeight = $this->helper->getMaxShipmentWeight();

        foreach ($rates as $rate) {
            if (strlen($rate["country"]) > 2) {
                WC_Admin_Settings::add_error(sprintf(__('File upload error: row %d: "%s", COUNTRY column. Use destination Country in 2 character ISO format (e.g. IT for Italy, ES for Spain, DE for Germany)', 'mail-boxes-etc'), $i, $rate["country"]));
                $errors = true;
            }

            if (is_array($allowedShipmentServicesArray) && !in_array($rate["delivery_type"], $allowedShipmentServicesArray)) {
                WC_Admin_Settings::add_error(sprintf(__('File upload error: row %d: "%s", SHIPMENT TYPE column. Input code is not a valid MBE Service', 'mail-boxes-etc'), $i, $rate["delivery_type"]));
                $errors = true;
            }

            if ($maxShipmentWeight) {
                if ($rate["weight_from"] > $maxShipmentWeight) {
                    WC_Admin_Settings::add_error(sprintf(__('File upload error: row %d: "%s", WEIGHT column. Input weight exceeds allowed', 'mail-boxes-etc'), $i, $rate["weight_from"]));
                    $errors = true;
                }
                if ($rate["weight_to"] > $maxShipmentWeight) {
                    WC_Admin_Settings::add_error(sprintf(__('File upload error: row %d: "%s", WEIGHT column. Input weight exceeds allowed', 'mail-boxes-etc'), $i, $rate["weight_to"]));
                    $errors = true;
                }
            }
            $i++;
        }

        if ($errors) {
            return false;
        }

        //INSERT

        $ratesHelper = new Mbe_Shipping_Helper_Rates();

        $truncateResult = $ratesHelper->truncate();

        if (!$truncateResult) {
            WC_Admin_Settings::add_error(__('Error executing truncate query', 'mail-boxes-etc'));
        }

        foreach ($rates as $rate) {
            $insertResult = $ratesHelper->insertRate($rate["country"], $rate["region"], $rate["city"], $rate["zip"], $rate["zip_to"], $rate["weight_from"], $rate["weight_to"], $rate["price"], $rate["delivery_type"]);

            if (!$insertResult) {
                WC_Admin_Settings::add_error(__('Error executing query', 'mail-boxes-etc'));
            }
        }

    }

}
