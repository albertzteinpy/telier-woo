<?php

if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class Mbe_E_Link_Order_List_Table extends WP_List_Table
{


    function __construct()
    {
        global $status, $page;

        parent::__construct(array(
            'singular' => 'order',     //singular name of the listed records
            'plural'   => 'orders',    //plural name of the listed records
            'ajax'     => true        //does this table support ajax?
        ));

    }

    function column_ID($item)
    {

        $actions = array(
            'edit' => sprintf('<a href="' . get_home_url() . '/wp-admin/post.php?post=%s&action=edit">%s</a>', $item['ID'], __('Edit', 'mail-boxes-etc')),
        );

        return sprintf('%s %s',
            $item['ID'],
            $this->row_actions($actions)
        );

        return sprintf('%1$s <span style="color:silver"></span>%2$s', $item['id'], $this->row_actions($actions));
    }

    function column_post_author($item)
    {
        $order = new WC_Order($item['ID']);

        if (version_compare(WC()->version, '3', '>=')) {
            $billingFirstName = $order->get_billing_first_name();
            $billingLastName = $order->get_billing_last_name();
        }
        else {
            $billingFirstName = $order->billing_first_name;
            $billingLastName = $order->billing_last_name;
        }
        return sprintf('%1$s %2$s', $billingFirstName, $billingLastName);
    }

    function column_carrier($item)
    {
        //TODO: verify
        $order = new WC_Order($item["ID"]);
        $helper = new Mbe_Shipping_Helper_Data();
        $serviceName = $helper->getServiceName($order);
        return $serviceName;
        //return sprintf('%s', get_post_meta($item['ID'], 'woocommerce_mbe_tracking_name', true));
    }


    function column_tracking($item)
    {
        $helper = new Mbe_Shipping_Helper_Data();
        $trackings = $helper->getTrackings($item['ID']);
        if (empty($trackings)) {
            return '';
        }
        else {
            $html = '';
            $url = get_post_meta($item['ID'], 'woocommerce_mbe_tracking_url', true);

            $trackingString = $helper->getTrackingsString($item['ID']);
            if (count($trackings) > 1) {
                $html .= "<a target='_blank' href=" . $url . $trackingString . ">" . __('Track all', 'mail-boxes-etc') . "</a><br/>";
            }

            $i = 0;
            foreach ($trackings as $t) {
                if ($i > 0) {
                    $html .= "<br/>";
                }
                $html .= "<a target='_blank' href=" . $url . $t . ">" . $t . "</a>";
                $i++;
            }
            return $html;
        }
    }

    function column_post_date($item)
    {
        return $item['post_date'];
    }

    function column_total($item)
    {
        $order = new WC_Order($item['ID']);
        return $order->get_total() . ' &euro;';
    }

    function column_payment($item)
    {
        $order = new WC_Order($item['ID']);
        if (version_compare(WC()->version, '3', '>=')) {
            $paymentMethodTitle = $order->get_payment_method_title();
        }
        else {
            $paymentMethodTitle = $order->payment_method_title;
        }
        return $paymentMethodTitle;
    }

    function column_status($item)
    {
        $helper = new Mbe_Shipping_Helper_Data();
        return $helper->isShippingOpen($item['ID']) ? __('Opened', 'mail-boxes-etc') : __('Closed', 'mail-boxes-etc');
    }

    function column_files($item)
    {
        $helper = new Mbe_Shipping_Helper_Data();
        $files = $helper->getFileNames($item['ID']);

        $trackings = $helper->getTrackings($item['ID']);
        if (empty($files)) {
            return '';
        }
        else {
            $html = '';
            for ($i = 0; $i < count($files); $i++) {
                $filename = __('Label', 'mail-boxes-etc') . " " . ($i + 1);
                $path = get_home_url() . '/wp-content/uploads/mbe/' . $files[$i];
                $html .= "<a target='_blank' href=" . $path . " style='margin-bottom:5px;display: inline-block;'>" . $filename . "</a></br>";
            }
            if (isset($trackings[0]) && !$helper->isTrackingOpen($trackings[0])) {
                $path = get_home_url() . '/wp-content/uploads/mbe/MBE_' . $trackings[0] . "_closed.pdf";
                $html .= "<a target='_blank' href=" . $path . " style='margin-bottom:5px;display: inline-block;'>" . __('Closure file', 'mail-boxes-etc') . "</a></br>";
            }
            return $html;
        }
    }

    function column_cb($item)
    {
        return sprintf(
            '<input type="checkbox" name="id[]" value="%s" />',
            $item['ID']
        );
    }

    function get_columns()
    {
        $columns = array('cb' => '<input type="checkbox" />');
        $columns['id'] = __('Id', 'mail-boxes-etc');
        $ws = new Mbe_Shipping_Model_Ws();
        if ($ws->mustCloseShipments()) {
            $columns['status'] = __('Status', 'mail-boxes-etc');
        }
        $columns['post_author'] = __('Customer', 'mail-boxes-etc');
        $columns['payment'] = __('Payment', 'mail-boxes-etc');
        $columns['post_date'] = __('Date', 'mail-boxes-etc');
        $columns['total'] = __('Total', 'mail-boxes-etc');
        $columns['carrier'] = __('Carrier', 'mail-boxes-etc');
        $columns['tracking'] = __('Tracking', 'mail-boxes-etc');
        $columns['files'] = __('Downloads', 'mail-boxes-etc');

        return $columns;
    }

    function get_sortable_columns()
    {
        $sortable_columns = array(
            'id'        => array('id', true),
            'post_date' => array('date', true),
        );

        return $sortable_columns;
    }

    function get_bulk_actions()
    {
        $actions = array();
        $helper = new Mbe_Shipping_Helper_Data();
        if (!$helper->isCreationAutomatically()) {
            $actions['creation'] = __('Create shipments in OnlineMBE', 'mail-boxes-etc');
        }
        if (!$helper->isClosureAutomatically()) {
            $ws = new Mbe_Shipping_Model_Ws();
            if ($ws->mustCloseShipments()) {
                $actions['closure'] = __('Close shipments in OnlineMBE', 'mail-boxes-etc');
            }
        }
        return $actions;
    }

    function process_bulk_action()
    {
        $helper = new Mbe_Shipping_Helper_Data();

        if (isset($_REQUEST['id'])) {
            $post_ids = array_map('absint', (array)$_REQUEST['id']);

            switch ($this->current_action()) {
                case 'creation':

                    $toCreationIds = array();
                    $alreadyCreatedIds = array();
                    $errorsIds = array();
                    foreach ($post_ids as $post_id) {
                        if ($helper->hasTracking($post_id)) {
                            array_push($alreadyCreatedIds, $post_id);
                        }
                        else {
                            $order = new WC_Order($post_id);
                            if ($this->process_order_meta_box_actions($order)) {
                                array_push($toCreationIds, $post_id);
                            }
                            else {
                                array_push($errorsIds, $post_id);
                            }

                        }
                    }
                    if (count($toCreationIds) > 0) {
                        echo '<div class="updated"><p>' . sprintf(__('Total of %d order shipment(s) have been created.', 'mail-boxes-etc'), $toCreationIds) . '</p></div>';
                    }
                    if (count($alreadyCreatedIds) > 0) {
                        echo '<div class="error"><p>' . sprintf(__('Total of %d order shipment(s) was already created.', 'mail-boxes-etc'), $alreadyCreatedIds) . '</p></div>';
                    }
                    if (count($errorsIds) > 0) {
                        echo '<div class="error"><p>' . sprintf(__('WARNING %d shipments couldn\'t be created due to an error.', 'mail-boxes-etc'), $errorsIds) . '</p></div>';
                    }
                    break;
                case 'closure':
                    if (!$helper->isClosureAutomatically()) {
                        $ws = new Mbe_Shipping_Model_Ws();
                        if ($ws->mustCloseShipments()) {

                            $toClosedIds = array();
                            $alreadyClosedIds = array();
                            $withoutTracking = array();

                            foreach ($post_ids as $post_id) {
                                if (!$helper->hasTracking($post_id)) {
                                    array_push($withoutTracking, $post_id);
                                }
                                elseif ($helper->isShippingOpen($post_id)) {
                                    array_push($toClosedIds, $post_id);
                                }
                                else {
                                    array_push($alreadyClosedIds, $post_id);
                                }
                            }
                            $ws->closeShipping($toClosedIds);
                            if (count($withoutTracking) > 0) {
                                echo '<div class="error"><p>' . sprintf(__('%s - Total of %d order(s) without tracking number yet.', 'mail-boxes-etc'), date('Y-m-d H:i:s'), $withoutTracking) . '</p></div>';
                            }
                            if (count($toClosedIds) > 0) {
                                echo '<div class="updated"><p>' . sprintf(__('%s - Total of %d order(s) have been closed.', 'mail-boxes-etc'), date('Y-m-d H:i:s'), $toClosedIds) . '</p></div>';
                            }

                            if (count($alreadyClosedIds) > 0) {
                                echo '<div class="error"><p>' . sprintf(__('%s - Total of %d order(s) was already closed', 'mail-boxes-etc'), date('Y-m-d H:i:s'), $alreadyClosedIds) . '</p></div>';
                            }
                        }
                    }
                    break;
            }
        }
        else {
            if ($this->current_action()) {
                echo '<div class="error"><p>' . sprintf(__('Please select items.', 'mail-boxes-etc')) . '</p></div>';
            }
        }
    }

    protected function process_order_meta_box_actions($order)
    {
        include_once 'class-mbe-tracking-factory.php';

        if (version_compare(WC()->version, '3', '>=')) {
            $orderId = $order->get_id();
        }
        else {
            $orderId = $order->id;
        }
        return mbe_tracking_factory::create($orderId);
    }

    function prepare_items()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'posts';

        $per_page = 10;

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        $this->_column_headers = array($columns, $hidden, $sortable);

        $this->process_bulk_action();

        $paged = isset($_REQUEST['paged']) ? max(0, intval($_REQUEST['paged']) - 1) : 0;
        $paged = $paged * $per_page;
        $orderby = (isset($_REQUEST['orderby']) && in_array($_REQUEST['orderby'], array_keys($this->get_sortable_columns()))) ? $_REQUEST['orderby'] : 'id';
        $order = (isset($_REQUEST['order']) && in_array($_REQUEST['order'], array('asc', 'desc'))) ? $_REQUEST['order'] : 'desc';

        $postmetaTableName = $wpdb->prefix . 'postmeta';
        $order_ids = $this->select_mbe_ids();
        if (isset($_REQUEST["order_search"]) && $_REQUEST["order_search"] != "") {
            $search = $_REQUEST["order_search"];

            $total_items = $wpdb->get_var("SELECT COUNT(DISTINCT(p.ID)) FROM $table_name AS p
                                           LEFT JOIN $postmetaTableName AS pm ON pm.post_id = p.ID
                                           WHERE post_type='shop_order'
                                           AND ID IN ({$order_ids})
                                           AND pm.meta_value LIKE '%$search%'
                                           ");

            $query = $wpdb->prepare("SELECT p.*, p.ID FROM $table_name AS p
                                       LEFT JOIN $postmetaTableName AS pm ON pm.post_id = p.ID
                                       WHERE post_type='shop_order'
                                       AND ID IN ({$order_ids})
                                       AND pm.meta_key IN ('_billing_last_name', '_billing_first_name', 'woocommerce_mbe_tracking_name','woocommerce_mbe_tracking_number', '_order_total', '_payment_method_title')
                                       AND (pm.meta_value LIKE '%%$search%%')
                                       GROUP BY p.ID
                                       ORDER BY $orderby $order LIMIT %d OFFSET %d", array($per_page, $paged));
            $this->items = $wpdb->get_results($query, ARRAY_A);
        }
        else {
            $total_items = $wpdb->get_var("SELECT COUNT(id) FROM $table_name
                                            WHERE post_type='shop_order'
                                            AND ID IN ({$order_ids})");
            $this->items = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name
                                                        WHERE post_type='shop_order'
                                                        AND ID IN ({$order_ids})
                                                        ORDER BY $orderby $order LIMIT %d OFFSET %d", $per_page, $paged), ARRAY_A);
        }

        $this->set_pagination_args(array(
            'total_items' => $total_items, // total items defined above
            'per_page'    => $per_page, // per page constant defined at top of method
            'total_pages' => ceil($total_items / $per_page) // calculate pages count
        ));
    }

    function select_mbe_ids()
    {
        global $wpdb;
        $postmetaTableName = $wpdb->prefix . 'postmeta';

        if (version_compare(WC()->version, '2.1', '>=')) {
            return "SELECT order_id FROM {$wpdb->prefix}woocommerce_order_items AS oi INNER JOIN $wpdb->order_itemmeta AS oim ON oi.order_item_id = oim.order_item_id WHERE oim.meta_key = 'method_id' AND oim.meta_value LIKE 'wf_mbe_shipping:%%'";
        }
        else {
            return "SELECT post_id FROM {$postmetaTableName} AS pm WHERE pm.meta_key = '_shipping_method' AND pm.meta_value LIKE 'wf_mbe_shipping:%%'";
        }
    }

}
