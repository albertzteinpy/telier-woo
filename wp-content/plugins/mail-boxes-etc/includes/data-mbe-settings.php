<?php
if (!defined('ABSPATH')) {
    exit;
}

$ws = new Mbe_Shipping_Model_Ws();
$availableShipping = $ws->getAllowedShipmentServices();

$serviceOptions = null;
if ($ws->isCustomerActive()) {
    if ($availableShipping) {
        foreach ($availableShipping as $key => $array) {
            $serviceOptions[$array['value']] = $array['label'];
        }
    }
    else {
        $serviceOptions = array('' => __('Set ws parameters and save to retrieve available shipment types', 'mail-boxes-etc'));
    }
}
else {
    $serviceOptions = array('' => __('Your user is not active', 'mail-boxes-etc'));
}


$shipmentTypeOptions = array('GENERIC' => __('Generic', 'mail-boxes-etc'));
$customer = $ws->getCustomer();
if ($customer && $customer->Permissions->canChooseMBEShipType) {
    $shipmentTypeOptions['ENVELOPE'] = __('Envelope', 'mail-boxes-etc');
}

$countries_obj = new WC_Countries();
$countries = version_compare(WC()->version, '2.1', '>=') ? $countries_obj->__get('countries') : $countries_obj->get_allowed_countries();

// Closure mode options
$closureModeOptions = array(
    Mbe_Shipping_Helper_Data::MBE_CLOSURE_MODE_AUTOMATICALLY => __('Automatically', 'mail-boxes-etc'),
    Mbe_Shipping_Helper_Data::MBE_CLOSURE_MODE_MANUALLY      => __('Manually', 'mail-boxes-etc'),
);

$closureTimeOptions = array();
for ($i = 0; $i < 24; $i++) {
    $closureTimeOptions[$i . ':00:00'] = $i . ':00';
    $closureTimeOptions[$i . ':30:00'] = $i . ':30';
}

// Closure mode options
$creationModeOptions = array(
    Mbe_Shipping_Helper_Data::MBE_CREATION_MODE_AUTOMATICALLY => __('Automatically', 'mail-boxes-etc'),
    Mbe_Shipping_Helper_Data::MBE_CREATION_MODE_MANUALLY      => __('Manually', 'mail-boxes-etc'),
);


$customInputs = array();
$helper = new Mbe_Shipping_Helper_Data();
$selectedServices = $helper->getAllowedShipmentServices();

if (!empty($selectedServices)) {
    foreach ($selectedServices as $t) {
        $index = array_search($t, array_column($availableShipping, 'value'));
        $shippingLabel = "";
        if (isset($availableShipping[$index]['label'])) {
            $shippingLabel = $availableShipping[$index]['label'];
        }
        $label = sprintf(__('Free shipping Thresholds %s', 'mail-boxes-etc'), $shippingLabel);
        $customInputs[] = array('label' => $label, 'name' => 'mbelimit_' . strtolower($t));
    }
}


$csvModeOptions = array(
    Mbe_Shipping_Helper_Data::MBE_CSV_MODE_DISABLED => __('Disabled', 'mail-boxes-etc'),
    Mbe_Shipping_Helper_Data::MBE_CSV_MODE_PARTIAL  => __('Partial', 'mail-boxes-etc'),
    Mbe_Shipping_Helper_Data::MBE_CSV_MODE_TOTAL    => __('Total', 'mail-boxes-etc'),
);

$insuranceModeOptions = array(
    Mbe_Shipping_Helper_Data::MBE_INSURANCE_WITH_TAXES    => __('With Taxes', 'mail-boxes-etc'),
    Mbe_Shipping_Helper_Data::MBE_INSURANCE_WITHOUT_TAXES => __('Without Taxes', 'mail-boxes-etc'),
);

$helper = new Mbe_Shipping_Helper_Data();
$shipmentsCsvFileUrl = "";
if ($helper->getShipmentsCsvFileUrl()) {
    $shipmentsCsvFileUrl = sprintf(__('<a href="%s" target="_blank">Download current file</a>', 'mail-boxes-etc'), $helper->getShipmentsCsvFileUrl());
}

$shipmentsCsvTemplateFileUrl = sprintf(__('<a href="%s" target="_blank">Download template file</a>', 'mail-boxes-etc'), $helper->getShipmentsCsvTemplateFileUrl());
/**
 * Array of settings
 */
$result = array(
    'mbe_debug'                    => array(
        'title'       => __('Debug', 'mail-boxes-etc'),
        'type'        => 'select',
        'options'     => array(1 => __('Yes', 'mail-boxes-etc'), 0 => __('No', 'mail-boxes-etc')),
        'label'       => __('Debug', 'mail-boxes-etc'),
        'description' => __('Activate Debug mode to save MBE e-Link logs', 'mail-boxes-etc'),
        'default'     => 'no',
    ),
    'mbe_active'                   => array(
        'title'   => __('Enabled', 'mail-boxes-etc'),
        'type'    => 'select',
        'options' => array(1 => __('Yes', 'mail-boxes-etc'), 0 => __('No', 'mail-boxes-etc')),
        'label'   => __('Enable', 'mail-boxes-etc'),
        'default' => 'no',
    ),
    'country'                      => array(
        'title'       => __('Country', 'mail-boxes-etc'),
        'type'        => 'select',
        'options'     => array('IT' => __('Italy', 'mail-boxes-etc'), 'ES' => __('Spain', 'mail-boxes-etc'), 'DE' => __('Germany', 'mail-boxes-etc'), 'FR' => __('France', 'mail-boxes-etc'), 'AT' => __('Austria', 'mail-boxes-etc')),
        'description' => __('Select your MBE Center\'s country', 'mail-boxes-etc'),
        'desc_tip'    => true,
    ),
    'url'                          => array(
        'title'       => __('OnlineMBE web-service URL', 'mail-boxes-etc'),
        'type'        => 'text',
        'description' => __('Italy: http://www.onlinembe.it/wsdl/OnlineMbeSOAP.wsdl - Spain: http://new.onlinembe.es/wsdl/OnlineMbeSOAP.wsdl - Germany: http://www.onlinembe.de/wsdl/OnlineMbeSOAP.wsdl - Austria: http://www.onlinembe.at/wsdl/OnlineMbeSOAP.wsdl - For other countries please contact your MBE Center"', 'mail-boxes-etc'),
    ),
    'username'                     => array(
        'title' => __('OnlineMBE Username', 'mail-boxes-etc'),
        'type'  => 'text',
    ),
    'password'                     => array(
        'title' => __('OnlineMBE web-service passphrase (provided by MBE Center)', 'mail-boxes-etc'),
        'type'  => 'password',
    ),
    'description'                  => array(
        'title' => __('MBE shipping description', 'mail-boxes-etc'),
        'type'  => 'text',
    ),
    'default_shipment_type'        => array(
        'title'   => __('Default Shipment type', 'mail-boxes-etc'),
        'label'   => __('Default Shipment type', 'mail-boxes-etc'),
        'type'    => 'select',
        'options' => $shipmentTypeOptions,
    ),
    'allowed_shipment_services'    => array(
        'title'       => __('MBE Services', 'mail-boxes-etc'),
        'label'       => __('MBE Services', 'mail-boxes-etc'),
        'type'        => 'multiselect',
        'options'     => $serviceOptions,
        'description' => __('Select MBE Services available to your Customers', 'mail-boxes-etc'),
    ),
    'default_shipment_mode'        => array(
        'title'       => __('Shipment configuration mode', 'mail-boxes-etc'),
        'description' => __('WARNING: activating the option \'Create one Shipment per Item\' with COD payment, the shopping cart\'s amount will be split and charged evenly on each shipment (based on number of items, not on their value)', 'mail-boxes-etc'),
        'label'       => __('Shipment configuration mode', 'mail-boxes-etc'),
        'type'        => 'select',
        'options'     => array(
            '1' => __('Create one Shipment per Item', 'mail-boxes-etc'),
            '2' => __('Create one Shipment per shopping cart (parcels calculated based on weight)', 'mail-boxes-etc'),
            '3' => __('Create one Shipment per shopping cart with one parcel per Item', 'mail-boxes-etc'),
        ),
    ),
    'default_length'               => array(
        'title' => __('Default Package Length', 'mail-boxes-etc'),
        'type'  => 'text',
    ),
    'default_width'                => array(
        'title' => __('Default Package Width', 'mail-boxes-etc'),
        'type'  => 'text',
    ),
    'default_height'               => array(
        'title' => __('Default Package Height', 'mail-boxes-etc'),
        'type'  => 'text',
    ),
    'max_package_weight'           => array(
        'title'       => __('Maximum Package Weight', 'mail-boxes-etc'),
        'type'        => 'text',
        'description' => __('Check if any limitation is applied with your MBE Center', 'mail-boxes-etc'),
    ),
    'max_shipment_weight'          => array(
        'title' => __('Maximum shipment weight', 'mail-boxes-etc'),
        'type'  => 'text',
    ),
    'handling_type'                => array(
        'title'   => __('Markup - Application rule', 'mail-boxes-etc'),
        'type'    => 'select',
        'options' => array(
            'P' => __('Percentage', 'mail-boxes-etc'),
            'F' => __('Fixed amount', 'mail-boxes-etc'),
        ),
    ),
    'handling_action'              => array(
        'title'   => __('Markup - Amount', 'mail-boxes-etc'),
        'type'    => 'select',
        'options' => array(
            'S' => __('Shipment', 'mail-boxes-etc'),
            'P' => __('Parcel', 'mail-boxes-etc'),
        ),
    ),
    'handling_fee'                 => array(
        'title' => __('Handling Fee', 'mail-boxes-etc'),
        'type'  => 'text',
    ),
    'handling_fee_rounding'        => array(
        'title'   => __('Markup - Apply rounding', 'mail-boxes-etc'),
        'type'    => 'select',
        'options' => array(
            '1' => __('No rounding', 'mail-boxes-etc'),
            '2' => __('Round up or down automatically', 'mail-boxes-etc'),
            '3' => __('Always round down', 'mail-boxes-etc'),
            '4' => __('Always round up', 'mail-boxes-etc'),
        ),
    ),
    'handling_fee_rounding_amount' => array(
        'title'   => __('Markup - Rounding unit (in €)', 'mail-boxes-etc'),
        'type'    => 'select',
        'options' => array(
            '1' => '1',
            '2' => '0.5',
        ),
    ),
    'sallowspecific'               => array(
        'title'   => __('Ship to Applicable Countries', 'mail-boxes-etc'),
        'type'    => 'select',
        'options' => array(
            '0' => __('All Allowed Countries', 'mail-boxes-etc'),
            '1' => __('Specific Countries', 'mail-boxes-etc'),
        ),
    ),
    'specificcountry'              => array(
        'title'   => __('Country', 'mail-boxes-etc'),
        'type'    => 'multiselect',
        'options' => $countries,
    ),
//	'sort_order' => array(
//		'title'           => __( 'Sort Order', 'mail-boxes-etc' ),
//		'type'            => 'text',
//	),
//	'mbe_shipping_specificerrmsg' => array(
//		'title'           => __( 'Displayed Error Message', 'mail-boxes-etc' ),
//		'type'            => 'textarea',
//	),
    'shipments_closure_mode'       => array(
        'title'   => __('OnlineMBE daily shipments closure - Mode', 'mail-boxes-etc'),
        'type'    => 'select',
        'options' => $closureModeOptions,
    ),
    'shipments_closure_time'       => array(
        'title'   => __('OnlineMBE daily time shipments closure (automatic mode only)', 'mail-boxes-etc'),
        'type'    => 'select',
        'options' => $closureTimeOptions,
    ),
    'shipments_creation_mode'      => array(
        'title'   => __('Shipments creation in OnlineMBE - Mode', 'mail-boxes-etc'),
        'type'    => 'select',
        'options' => $creationModeOptions,
    ),
    'shipments_csv'                => array(
        'title' => __('Custom prices via csv - File upload', 'mail-boxes-etc'),
        'type'  => 'file',
    ),
    'shipments_csv_file'           => array(
        'title'       => '',
        'type'        => 'hidden',
        'description' => $shipmentsCsvFileUrl,
    ),
    'shipments_csv_template_file'  => array(
        'title'       => '',
        'type'        => 'hidden',
        'description' => $shipmentsCsvTemplateFileUrl,
    ),
    'shipments_csv_mode'           => array(
        'title'   => __('Custom prices via csv - File mode', 'mail-boxes-etc'),
        'type'    => 'select',
        'options' => $csvModeOptions,
    ),


    'mbe_shipments_csv_insurance_min' => array(
        'title' => __('Custom prices via csv - Min price for insurance extra-service', 'mail-boxes-etc'),
        'type'  => 'text',
    ),
    'mbe_shipments_csv_insurance_per' => array(
        'title' => __('Custom prices via csv - Percentage for insurance extra-service price calculation', 'mail-boxes-etc'),
        'type'  => 'text',
    ),
    'mbe_shipments_ins_mode'          => array(
        'title'   => __('Insurance extra-service - Declared value calculation', 'mail-boxes-etc'),
        'type'    => 'select',
        'options' => $insuranceModeOptions,
    ),

);

foreach ($customInputs as $input) {
    $result[$input['name']] = array(
        'title' => __($input['label'], 'mail-boxes-etc'),
        'type'  => 'text',
    );
}

return $result;