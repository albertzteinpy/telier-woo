<?php

include_once 'class-mbe-tracking-admin.php';

class mbe_tracking_factory
{

    const ITALIAN_URL = "https://www.mbe.it/it/tracking?c=";
    const SPAIN_URL = "https://www.mbe.es/es/tracking?c=";
    const GERMANY_URL = "http://www.mbe.de/sendung-verfolgen/?accio=track&param1=";
    const AUSTRIA_URL = "http://www.mbe.at/sendung-verfolgen/?accio=track&param1=";
    const FRANCE_URL = "http://www.mbefrance.fr/nc/utility-menu/suivi-de-votre-expedition/?accio=track&param1=";

    public static function create($order_id)
    {

        $result = true;

        $logger = new Mbe_Shipping_Helper_Logger();
        $helper = new Mbe_Shipping_Helper_Data();
        $logger->log("CREATESHIPMENT");
        $order = new WC_Order($order_id);


        $shippingMethod = $helper->getShippingMethod($order);
        $serviceName = $helper->getServiceName($order);

        if (!$shippingMethod) {
            return;
        }

        $val = explode(':', $shippingMethod);
        $shippingMethod = $val[1];

        $insurance = $helper->isShippingWithInsurance($shippingMethod);
        if ($insurance) {
            $shippingMethod = $helper->convertShippingCodeWithoutInsurance($shippingMethod);
        }

        $shippingMethodArray = explode('_', $shippingMethod);

        $service = $shippingMethodArray[0];
        $subzone = $shippingMethodArray[1];

        $orderTotal = $order->get_total();


        $isCod = false;

        if (version_compare(WC()->version, '3', '>=')) {
            $paymentMethod = $order->get_payment_method();
        }
        else {
            $paymentMethod = $order->payment_method;
        }
        if ($paymentMethod == "cod") {
            $isCod = true;
        }

        $shipmentConfigurationMode = $helper->getShipmentConfigurationMode();


        if ($shipmentConfigurationMode == Mbe_Shipping_Model_Carrier::SHIPMENT_CONFIGURATION_MODE_ONE_SHIPMENT_PER_ITEM) {

            $productsAmount = 0;
            foreach ($order->get_items() as $item) {
                $productsAmount += $item['qty'];
            }

            $codValue = $orderTotal / $productsAmount;

            foreach ($order->get_items() as $item) {
                $itemQty = $item['qty'];
                $id_product = $item['product_id'];
                $product = $helper->getProductFromItem($item);

                if (version_compare(WC()->version, '3', '>=')) {
                    $itemWeight = $product->get_weight();
                }
                else {
                    $itemWeight = $product->weight;
                }

                $products = array();
                $p = new stdClass;
                $p->SKUCode = $helper->getProductSkuFromItem($item);
                $p->Description = $helper->getProductTitleFromItem($item);
                $p->Quantity = 1;
                array_push($products, $p);

                if (isset($item["subtotal"])) {
                    $subTotal = $item["subtotal"];
                    $subTotalTax = $item["subtotal_tax"];
                }
                else {
                    $subTotal = $item["line_subtotal"];
                    $subTotalTax = $item["line_subtotal_tax"];
                }

                $goodsValue = ($subTotal) / $itemQty;

                if ($helper->getShipmentsInsuranceMode() == Mbe_Shipping_Helper_Data::MBE_INSURANCE_WITH_TAXES) {
                    $insuranceValue = ($subTotal + $subTotalTax) / $itemQty;
                }
                else {
                    $insuranceValue = ($subTotal) / $itemQty;
                }

                for ($i = 1; $i <= $itemQty; $i++) {
                    $qty = array();
                    $qty[$id_product] = 1;

                    $result = $result && mbe_tracking_factory::createSingleShipment($order, $service, $itemWeight, $products, 1, array(), $goodsValue, $isCod, $codValue, $insurance, $insuranceValue);
                }
            }
        }
        elseif ($shipmentConfigurationMode == Mbe_Shipping_Model_Carrier::SHIPMENT_CONFIGURATION_MODE_ONE_SHIPMENT_PER_SHOPPING_CART_SINGLE_PARCEL) {
            $maxPackageWeight = $helper->getMaxPackageWeight();
            $boxesWeights = array();
            $products = array();
            $goodsValue = 0.0;
            $insuranceValue = 0.0;
            $codValue = $orderTotal;

            foreach ($order->get_items() as $item) {
                $itemQty = $item['qty'];
                $id_product = $item['product_id'];
                $product = $helper->getProductFromItem($item);


                $p = new stdClass;
                $p->SKUCode = $helper->getProductSkuFromItem($item);
                $p->Description = $helper->getProductTitleFromItem($item);
                $p->Quantity = $itemQty;
                array_push($products, $p);

                for ($i = 1; $i <= $itemQty; $i++) {
                    $canAddToExistingBox = false;
                    for ($j = 0; $j <= count($boxesWeights); $j++) {
                        $val = isset($boxesWeights[$j]) ? $boxesWeights[$j] : 0;

                        if (version_compare(WC()->version, '3', '>=')) {
                            $productWeight = $product->get_weight();
                            $productPrice = $product->get_price();
                        }
                        else {
                            $productWeight = $product->weight;
                            $productPrice = $product->price;
                        }

                        $newWeight = $val + $productWeight;
                        if ($newWeight < $maxPackageWeight) {
                            $canAddToExistingBox = true;
                            $boxesWeights[$j] = $newWeight;
                            break;
                        }
                    }
                    if (!$canAddToExistingBox) {
                        $boxesWeights[] = $productWeight;;
                    }
                    $goodsValue = $goodsValue + $productPrice;
                }


                if (isset($item["subtotal"])) {
                    $subTotal = $item["subtotal"];
                    $subTotalTax = $item["subtotal_tax"];
                }
                else {
                    $subTotal = $item["line_subtotal"];
                    $subTotalTax = $item["line_subtotal_tax"];
                }

                if ($helper->getShipmentsInsuranceMode() == Mbe_Shipping_Helper_Data::MBE_INSURANCE_WITH_TAXES) {
                    $insuranceValue += $subTotal + $subTotalTax;
                }
                else {
                    $insuranceValue += $subTotal;
                }

            }
            $numBoxes = count($boxesWeights);

            $logger->logVar($numBoxes, "boxes amount");
            $logger->logVar($boxesWeights, "boxes weights");
            $logger->logVar($goodsValue, "goods value");

            $result = $result && mbe_tracking_factory::createSingleShipment($order, $service, $boxesWeights, $products, $numBoxes, array(), $goodsValue, $isCod, $codValue, $insurance, $insuranceValue);

        }
        elseif ($shipmentConfigurationMode == Mbe_Shipping_Model_Carrier::SHIPMENT_CONFIGURATION_MODE_ONE_SHIPMENT_PER_SHOPPING_CART_MULTI_PARCEL) {
            $boxesWeights = array();
            $products = array();
            $numBoxes = 0;
            $goodsValue = 0.0;
            $insuranceValue = 0.0;
            $codValue = $orderTotal;

            foreach ($order->get_items() as $item) {
                $itemQty = $item['qty'];
                $numBoxes += $itemQty;
                $id_product = $item['product_id'];
                $product = $helper->getProductFromItem($item);


                $p = new stdClass;
                $p->SKUCode = $helper->getProductSkuFromItem($item);
                $p->Description = $helper->getProductTitleFromItem($item);
                $p->Quantity = $itemQty;
                array_push($products, $p);

                if (version_compare(WC()->version, '3', '>=')) {
                    $productWeight = $product->get_weight();
                    $productPrice = $product->get_price();
                }
                else {
                    $productWeight = $product->weight;
                    $productPrice = $product->price;
                }

                $logger->logVar($productPrice, "product price");

                for ($i = 1; $i <= $itemQty; $i++) {
                    $boxesWeights[] = $productWeight;
                    $goodsValue = $goodsValue + $productPrice;
                }

                if (isset($item["subtotal"])) {
                    $subTotal = $item["subtotal"];
                    $subTotalTax = $item["subtotal_tax"];
                }
                else {
                    $subTotal = $item["line_subtotal"];
                    $subTotalTax = $item["line_subtotal_tax"];
                }

                if ($helper->getShipmentsInsuranceMode() == Mbe_Shipping_Helper_Data::MBE_INSURANCE_WITH_TAXES) {
                    $insuranceValue += $subTotal + $subTotalTax;
                }
                else {
                    $insuranceValue += $subTotal;
                }
            }

            $logger->logVar($numBoxes, "boxes amount");
            $logger->logVar($boxesWeights, "boxes weights");
            $logger->logVar($goodsValue, "goods value");

            $result = $result && mbe_tracking_factory::createSingleShipment($order, $service, $boxesWeights, $products, $numBoxes, array(), $goodsValue, $isCod, $codValue, $insurance, $insuranceValue);

        }
        return $result;
    }

    /**
     * @param $order WC_Order
     * @param $weight mixed
     * @param $boxes integer
     * @param array $qty
     */
    public static function createSingleShipment($order, $service, $weight, $products, $boxes, $qty = array(), $goodsValue = 0.0, $isCod = false, $codValue = 0.0, $insurance, $insuranceValue)
    {

        $logger = new Mbe_Shipping_Helper_Logger();
        $helper = new Mbe_Shipping_Helper_Data();
        $logger->log('CREATE SINGLE SHIPMENT');
        $logger->logVar(func_get_args(), 'CREATE SINGLE SHIPMENT ARGS');
        try {

            $shippingMethod = $helper->getShippingMethod($order);
            $serviceName = $helper->getServiceName($order);

            if ($shippingMethod) {
                $val = explode(':', $shippingMethod);
                $shippingMethod = $val[1];
                $shippingMethodArray = explode('_', $shippingMethod);

                $service = $shippingMethodArray[0];
                $subzone = $shippingMethodArray[1];

                if (version_compare(WC()->version, '3', '>=')) {
                    $firstName = $order->get_shipping_first_name();
                    $lastName = $order->get_shipping_last_name();
                    $address = trim($order->get_shipping_address_1() . ' ' . $order->get_shipping_address_2());
                    $phone = $order->get_billing_phone();
                    $city = $order->get_shipping_city();
                    $countryId = $order->get_shipping_country();
                    $region = $order->get_shipping_state();
                    $postCode = $order->get_shipping_postcode();
                    $email = $order->get_billing_email();

                    $companyName = $order->get_shipping_company();
                    $notes = $order->get_customer_note();
                }
                else {
                    $firstName = $order->shipping_first_name;
                    $lastName = $order->shipping_last_name;
                    $address = trim($order->shipping_address_1 . ' ' . $order->shipping_address_2);
                    $phone = $order->billing_phone;
                    $city = $order->shipping_city;
                    $countryId = $order->shipping_country;
                    $region = $order->shipping_state;
                    $postCode = $order->shipping_postcode;
                    $email = $order->billing_email;

                    $notes = $order->customer_note;
                    $companyName = $order->shipping_company;
                }

                $ws = new Mbe_Shipping_Model_Ws();
                if (version_compare(WC()->version, '3', '>=')) {
                    $orderId = $order->get_id();
                }
                else {
                    $orderId = $order->id;
                }
                $reference = $orderId;
                $mbeShipment = $ws->createShipping($countryId, $region, $postCode, $weight, $boxes, $products, $service, $subzone, $notes, $firstName, $lastName, $companyName, $address, $phone, $city, $email, $goodsValue, $reference, $isCod, $codValue, $insurance, $insuranceValue);


                $logger->logVar($mbeShipment, "MBE SHIPMENT");


                if ($mbeShipment) {
                    $trackingNumber = $mbeShipment->MasterTrackingMBE;
                    $label = $mbeShipment->Labels->Label;

                    if (is_array($label)) {
                        $i = 1;
                        foreach ($label as $l) {
                            $fileName = 'MBE_' . $orderId . '_' . $trackingNumber . '_' . $i;
                            mbe_tracking_factory::saveShipmentDocument($l->Type, $l->Stream, $fileName);
                            mbe_tracking_factory::saveMultipleShipmentInfo($orderId, woocommerce_mbe_tracking_admin::SHIPMENT_SOURCE_TRACKING_FILENAME, $fileName . '.' . strtolower($l->Type), true);
                            $i++;
                        }
                    }
                    else {
                        $fileName = 'MBE_' . $orderId . '_' . $trackingNumber;
                        mbe_tracking_factory::saveShipmentDocument($label->Type, $label->Stream, $fileName);

                        mbe_tracking_factory::saveMultipleShipmentInfo($orderId, woocommerce_mbe_tracking_admin::SHIPMENT_SOURCE_TRACKING_FILENAME, $fileName . '.' . strtolower($label->Type), true);
                    }

                    mbe_tracking_factory::saveMultipleShipmentInfo($orderId, woocommerce_mbe_tracking_admin::SHIPMENT_SOURCE_TRACKING_NUMBER, $trackingNumber);
                    update_post_meta($orderId, woocommerce_mbe_tracking_admin::SHIPMENT_SOURCE_TRACKING_NUMBER, $trackingNumber, true);
                    update_post_meta($orderId, woocommerce_mbe_tracking_admin::SHIPMENT_SOURCE_TRACKING_NAME, $serviceName, true);
                    update_post_meta($orderId, woocommerce_mbe_tracking_admin::SHIPMENT_SOURCE_TRACKING_SERVICE, $service, true);
                    update_post_meta($orderId, woocommerce_mbe_tracking_admin::SHIPMENT_SOURCE_TRACKING_ZONE, $subzone, true);
                    update_post_meta($orderId, woocommerce_mbe_tracking_admin::SHIPMENT_SOURCE_TRACKING_URL, mbe_tracking_factory::getTrackingUrlBySystem());
                    return true;
                }
            }

        }
        catch (Exception $e) {
            $logger->log($e->getMessage());
        }
        return false;
    }

    public static function saveMultipleShipmentInfo($post_id, $key, $value)
    {
        $old_meta = get_post_meta($post_id, $key, true);
        // Update post meta
        if (!empty($old_meta)) {
            $old_meta = $old_meta . Mbe_Shipping_Helper_Data::MBE_SHIPPING_TRACKING_SEPARATOR . $value;
            update_post_meta($post_id, $key, $old_meta);
        }
        else {
            add_post_meta($post_id, $key, $value, true);
        }
    }


    public static function saveShipmentDocument($type, $content, $filename)
    {
        $helper = new Mbe_Shipping_Helper_Data();
        $logger = new Mbe_Shipping_Helper_Logger();

        $ext = "txt";
        if ($type == "HTML") {
            $ext = "html";
        }
        elseif ($type == "PDF") {
            $ext = "pdf";
        }
        elseif ($type == "GIF") {
            $ext = "gif";
        }
        $filePath = $helper->getShipmentFilePath($filename, $ext);
        $saveResult = file_put_contents($filePath, $content);

        $message = "Saving shipping document :" . $filePath;

        if ($saveResult) {
            $message .= " OK";
        }
        else {
            $message .= " FAILURE";
        }

        $logger->log($message);
    }

    private static function getTrackingUrlBySystem()
    {
        $helper = new Mbe_Shipping_Helper_Data();
        $system = $helper->getCountry();

        $result = "";
        if ($system == "IT") {
            $result = self::ITALIAN_URL;
        }
        elseif ($system == "ES") {
            $result = self::SPAIN_URL;
        }
        elseif ($system == "DE") {
            $result = self::GERMANY_URL;
        }
        elseif ($system == "AT") {
            $result = self::AUSTRIA_URL;
        }
        elseif ($system == "FR") {
            $result = self::FRANCE_URL;
        }
        return $result;
    }

}

?>