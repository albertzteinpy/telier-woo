<?php
/*
	Plugin Name: MBE e-Link
	Description: Mail Boxes Etc. Online MBE Plugin integration for main Ecommerce platforms.
	Version: 1.1.0
	Author: MBE Worldwide S.p.A.
	Author URI: https://www.mbeglobal.com/
	Text Domain: mail-boxes-etc
	Domain Path: /languages
*/

if (!defined('MBE_E_LINK_ID')) {
    define("MBE_E_LINK_ID", "wf_mbe_shipping");
}

if (!defined('MBE_E_LINK_PLUGIN_DIR')) {
    define("MBE_E_LINK_PLUGIN_DIR", plugin_dir_path(__FILE__));
}

if (!defined('MBE_E_LINK_DEBUG_LOG')) {
    define('MBE_E_LINK_DEBUG_LOG', true);
}

require_once(MBE_E_LINK_PLUGIN_DIR . '/lib/Helper/Data.php');
require_once(MBE_E_LINK_PLUGIN_DIR . '/lib/Helper/Logger.php');
require_once(MBE_E_LINK_PLUGIN_DIR . '/lib/Helper/Tracking.php');
require_once(MBE_E_LINK_PLUGIN_DIR . '/lib/Helper/Csv.php');
require_once(MBE_E_LINK_PLUGIN_DIR . '/lib/Helper/Rates.php');

require_once(MBE_E_LINK_PLUGIN_DIR . '/lib/Mbe/MbeWs.php');
require_once(MBE_E_LINK_PLUGIN_DIR . '/lib/Model/Ws.php');
require_once(MBE_E_LINK_PLUGIN_DIR . '/lib/Model/Carrier.php');
require_once(MBE_E_LINK_PLUGIN_DIR . '/backward_compatibility/array_column.php');

/**
 * Plugin activation check
 */
function mbe_e_link_activation_check()
{
    if (is_plugin_active('mbe-woocommerce-shipping/mbe-woocommerce-shipping.php')) {
        deactivate_plugins(basename(__FILE__));
        wp_die("Is everything fine? You already have the Premium version installed in your website. For any issues, kindly raise a ticket via <a target='_blank' href='http://support.wooforce.com/'>support.wooforce.com</a>", "", array('back_link' => 1));
    }

    if (!class_exists('SoapClient')) {
        deactivate_plugins(basename(__FILE__));
        wp_die('Sorry, but you cannot run this plugin, it requires the <a href="http://php.net/manual/en/class.soapclient.php">SOAP</a> support on your server/hosting to function.');
    }
}

register_activation_hook(__FILE__, 'mbe_e_link_activation_check');

/**
 * Check if WooCommerce is active
 */
if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
    if (!function_exists('mbe_e_link_get_settings_url')) {
        function mbe_e_link_get_settings_url()
        {
            return version_compare(WC()->version, '2.1', '>=') ? "wc-settings" : "woocommerce_settings";
        }
    }

    if (!function_exists('mbe_e_link_plugin_override')) {
        add_action('plugins_loaded', 'mbe_e_link_plugin_override');
        function mbe_e_link_plugin_override()
        {
            if (!function_exists('WC')) {
                function WC()
                {
                    return $GLOBALS['woocommerce'];
                }
            }
        }
    }

    if (!class_exists('mbe_e_link_wooCommerce_shipping_setup')) {
        class mbe_e_link_wooCommerce_shipping_setup
        {

            public function __construct()
            {
                // Add settings link in the plugin list
                add_filter('plugin_action_links_' . plugin_basename(__FILE__), array($this, 'plugin_action_links'));

                // Add settings tab and page in the WooCommerce => Settings => Shipping
                add_filter('woocommerce_shipping_methods', array($this, 'wf_mbe_wooCommerce_shipping_methods'));
                add_action('woocommerce_shipping_init', array($this, 'wf_mbe_wooCommerce_shipping_init'));
                add_action('closure_event', array($this, 'automatic_closure'));
                add_action('woocommerce_settings_saved', array($this, 'add_cron_job'));


                $helper = new Mbe_Shipping_Helper_Data();
                if ($helper->isEnabled()) {
                    // Add box in the order detail
                    add_action('init', array($this, 'wf_mbe_wooCommerce_shipping_init_box'));

                    // Add menu
                    add_action('admin_menu', array($this, 'add_mbe_tab'));

                    // Add label "Free" to Frontend if the price is 0
                    add_filter('woocommerce_cart_shipping_method_full_label', array($this, 'wf_mbe_wooCommerce_set_free_label'), 10, 2);

                    // Creation shipping automatically action
                    add_filter('woocommerce_order_status_processing', array($this, 'mbe_update_tracking'), 10, 2);

                    // Creation shipping manually action
                    add_action('woocommerce_order_actions', array($this, 'add_order_meta_box_actions'));
                    add_action('woocommerce_order_action_mbe_shipping_creation', array($this, 'process_order_meta_box_actions'));

                    // Load translations
                    add_action('plugins_loaded', array($this, 'wan_load_wf_shipping_mbe'));

                    // Add frontend tracking
                    add_action('woocommerce_order_details_after_order_table', array($this, 'woocommerce_order_details_tracking'));
                }
            }

            /**
             * Plugin cron job
             */

            function automatic_closure()
            {
                $logger = new Mbe_Shipping_Helper_Logger();
                $logger->log('Cron automatic_closure');
                include_once 'includes/cron.php';
            }

            function remove_cron_job()
            {
                wp_clear_scheduled_hook('closure_event');
            }

            public function add_cron_job()
            {
                $helper = new Mbe_Shipping_Helper_Data();
                $time = $helper->getShipmentsClosureTime();
                if ($helper->isEnabled() && $helper->isClosureAutomatically()) {
                    $cron_jobs = get_option('cron');
                    $array = array_column($cron_jobs, 'closure_event');
                    if (!empty($array)) {
                        $index = array_search(array('closure_event' => $array[0]), $cron_jobs);
                        $scheduledTime = date('H:i:s', $index);
                        if ($scheduledTime != $time) {
                            wp_clear_scheduled_hook('closure_event');
                            wp_schedule_event(strtotime("today $time"), 'daily', 'closure_event');
                        }
                    }
                    else {
                        wp_schedule_event(strtotime("today $time"), 'daily', 'closure_event');
                    }
                }
                else {
                    wp_clear_scheduled_hook('closure_event');
                }
            }

            /**
             * Add settings link in the plugin list
             */

            public function plugin_action_links($links)
            {
                $plugin_links = array(
                    '<a href="' . admin_url('admin.php?page=' . mbe_e_link_get_settings_url() . '&tab=shipping&section=' . MBE_E_LINK_ID) . '">' . __('Settings', 'mail-boxes-etc') . '</a>',
                );
                return array_merge($plugin_links, $links);
            }

            /**
             * Add settings tab and page in the WooCommerce => Settings => Shipping
             */

            public function wf_mbe_wooCommerce_shipping_methods($methods)
            {
                $methods[] = 'mbe_shipping_method';
                return $methods;
            }

            public function wf_mbe_wooCommerce_shipping_init()
            {
                include_once('includes/class-mbe-shipping.php');
            }

            /**
             * Add box in the order detail
             */

            public function wf_mbe_wooCommerce_shipping_init_box()
            {
                include_once('includes/class-mbe-tracking-admin.php');
            }

            /**
             * Add menu tab in WooCommerce
             */

            function add_mbe_tab()
            {
                add_submenu_page('woocommerce', __('MBE Shipments List', 'mail-boxes-etc'), __('MBE Shipments List', 'mail-boxes-etc'), 'manage_woocommerce', 'woocommerce_mbe_tabs', array($this, 'add_order_list'));
            }

            /**
             * Add order list in MBE tab
             */

            function add_order_list()
            {
                require_once 'includes/class-mbe-order.php';
                $orders = new Mbe_E_Link_Order_List_Table();

                $orders->prepare_items(); ?>

				<div class="wrap">
					<h2><?php __('MBE Shipments List', 'mail-boxes-etc') ?></h2>

					<form id="certificates-filter" method="get">

						<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>"/>
                        <?php $orders->display() ?>
					</form>
				</div>
                <?php
            }

            /**
             * Creation shipping manually action
             */

            function add_order_meta_box_actions($actions)
            {
                if (is_array($actions)) {
                    $helper = new Mbe_Shipping_Helper_Data();
                    if ($_GET['post']) {
                        $order = new WC_Order($_GET['post']);
                        if ($helper->isMbeShipping($order)) {
                            if (!$helper->isCreationAutomatically() && !$helper->hasTracking()) {
                                $actions['mbe_shipping_creation'] = __('Create MBE shipping', 'mail-boxes-etc');
                            }
                        }
                    }
                }
                return $actions;
            }

            function process_order_meta_box_actions($order)
            {
                $helper = new Mbe_Shipping_Helper_Data();
                if ($helper->isMbeShipping($order)) {
                    include_once 'includes/class-mbe-tracking-factory.php';

                    if (version_compare(WC()->version, '3', '>=')) {
                        $orderId = $order->get_id();
                    }
                    else {
                        $orderId = $order->id;
                    }
                    mbe_tracking_factory::create($orderId);
                }
            }


            public function mbe_update_tracking($order_id)
            {

                $helper = new Mbe_Shipping_Helper_Data();
                $order = new WC_Order($order_id);
                if ($helper->isEnabled() && $helper->isMbeShipping($order) && $helper->isCreationAutomatically()) {
                    include_once 'includes/class-mbe-tracking-factory.php';
                    mbe_tracking_factory::create($order_id);
                }
            }

            public function init_admin_pages()
            {
                include_once('includes/class-mbe-tracking-admin.php');

                global $wct_admin_page;
                $wct_admin_page = new wf_mbe_woocommerce_admin_page;
                do_action('wf_mbe_woocommerce_init', $this);
            }

            /**
             * Set Free Label to Carriers
             */

            public function wf_mbe_wooCommerce_set_free_label($full_label, $method)
            {
                if ((float)$method->cost == 0.0) {
                    return $full_label . ': ' . __("Free", 'mail-boxes-etc');
                }
                else {
                    return $full_label;
                }
            }

            public function wan_load_wf_shipping_mbe()
            {
                load_plugin_textdomain('mail-boxes-etc', false, dirname(plugin_basename(__FILE__)) . '/languages/');
            }

            /*
             * Add front tracking
             */

            public function woocommerce_order_details_tracking($order)
            {
                $helper = new Mbe_Shipping_Helper_Data();
                if (version_compare(WC()->version, '3', '>=')) {
                    $orderId = $order->get_id();
                }
                else {
                    $orderId = $order->id;
                }
                $trackings = $helper->getTrackings($orderId);
                $tracking_url = get_post_meta($orderId, 'woocommerce_mbe_tracking_url', true);
                $trackingName = $this->tracking_name = get_post_meta($orderId, 'woocommerce_mbe_tracking_name', true);
                if ($helper->isMbeShipping($order) && !empty($trackings)) {
                    echo "<h2>" . __('Mail Boxes ETC Tracking', 'mail-boxes-etc') . "</h2>";
                    echo '
					<table class="shop_table tracking_details">
						<thead>
							<tr>
								<th class="service-name">' . __('Service', 'mail-boxes-etc') . '</th>
								<th class="tracking-link">' . __('Tracking', 'mail-boxes-etc') . '</th>
							</tr>
						</thead>
						<tbody>';
                    foreach ($trackings as $track) {
                        echo '<tr class="order_item">
								<td class="tracking-name">' . $trackingName . '</td>
								<td class="product-total"><a target="_blank" href="' . $tracking_url . $track . '">' . $track . '</a></td>
							  </tr>';
                    }
                    echo '</tbody></table>';
                }
            }

        }

        new mbe_e_link_wooCommerce_shipping_setup();
    }
}