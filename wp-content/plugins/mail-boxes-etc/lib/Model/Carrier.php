<?php

class Mbe_Shipping_Model_Carrier
{

    const SHIPMENT_CONFIGURATION_MODE_ONE_SHIPMENT_PER_ITEM = 1;
    const SHIPMENT_CONFIGURATION_MODE_ONE_SHIPMENT_PER_SHOPPING_CART_SINGLE_PARCEL = 2;
    const SHIPMENT_CONFIGURATION_MODE_ONE_SHIPMENT_PER_SHOPPING_CART_MULTI_PARCEL = 3;

    const HANDLING_TYPE_PER_SHIPMENT = "S";
    const HANDLING_TYPE_PER_PARCEL = "P";

    const HANDLING_TYPE_FIXED = "F";


    /**
     * Carrier's code, as defined in parent class
     *
     * @var string
     */
    protected $_code = 'mbe_shipping';

    /** @var $logger Mbe_Shipping_Helper_Logger */
    protected $logger = null;
    /** @var $helper Mbe_Shipping_Helper_Data */
    protected $helper = null;


    public function __construct()
    {
        $this->helper = new Mbe_Shipping_Helper_Data();
        $this->logger = new Mbe_Shipping_Helper_Logger();
    }


    public function getRequestWeight($request)
    {
        $result = 0;
        foreach ($request['contents'] as $item) {
            $result += ($item['data']->get_weight() * $item['quantity']);
        }
        return $result;
    }

    public function collectRates($request)
    {

        $this->logger->log('collectRates');

        if (!$this->helper->isEnabled()) {
            $this->logger->log('module disabled');
            return false;
        }
        $this->logger->log('module enabled');


        $destCountry = $request['destination']['country'];
        $destRegion = $request['destination']['state'];
        //TODO:verify
        $city = $request['destination']['city'];

        $destPostCode = $request['destination']['postcode'];

        $this->logger->log("Destination: COUNTRY: " . $destCountry . " - REGION CODE: " . $destRegion . " - POSTCODE: " . $destPostCode);


        $shipmentConfigurationMode = $this->helper->getShipmentConfigurationMode();


        $shipments = array();
        //$baseSubtotalInclTax = $request['contents_cost'];//wrong because it is without taxes
        $baseSubtotalInclTax = 0;
        foreach ($request['contents'] as $item) {
            $baseSubtotalInclTax += $item['line_total'] + $item['line_subtotal_tax'];
        }

        $this->logger->log("SHIPMENTCONFIGURATIONMODE: " . $shipmentConfigurationMode);
        if ($shipmentConfigurationMode == self::SHIPMENT_CONFIGURATION_MODE_ONE_SHIPMENT_PER_ITEM) {

            $iteration = 1;
            foreach ($request['contents'] as $item) {
                for ($i = 1; $i <= $item['quantity']; $i++) {
                    $this->logger->log("Product Iteration: " . $iteration);
                    $weight = $item['data']->get_weight();
                    if ($this->helper->getShipmentsInsuranceMode() == Mbe_Shipping_Helper_Data::MBE_INSURANCE_WITH_TAXES) {
                        if (version_compare(WC()->version, '3', '>=')) {
                            $insuranceValue = wc_get_price_including_tax($item["data"]);
                        }
                        else {
                            $insuranceValue = $item["data"]->get_price_including_tax();
                        }
                    }
                    else {
                        if (version_compare(WC()->version, '3', '>=')) {
                            $insuranceValue = wc_get_price_excluding_tax($item["data"]);
                        }
                        else {
                            $insuranceValue = $item["data"]->get_price_excluding_tax();
                        }
                    }

                    $shipments = $this->getRates($destCountry, $destRegion, $city, $destPostCode, $baseSubtotalInclTax, $weight, 1, $shipments, $iteration, $insuranceValue);
                    $iteration++;
                }
            }
        }
        elseif ($shipmentConfigurationMode == self::SHIPMENT_CONFIGURATION_MODE_ONE_SHIPMENT_PER_SHOPPING_CART_SINGLE_PARCEL) {
            $maxPackageWeight = $this->helper->getMaxPackageWeight();
            $boxesWeights = array();
            $insuranceValue = 0;

            foreach ($request['contents'] as $item) {
                if ($this->helper->getShipmentsInsuranceMode() == Mbe_Shipping_Helper_Data::MBE_INSURANCE_WITH_TAXES) {
                    if (version_compare(WC()->version, '3', '>=')) {
                        $insuranceValue += wc_get_price_including_tax($item["data"], array('qty' => $item['quantity']));
                    }
                    else {
                        $insuranceValue += $item["data"]->get_price_including_tax() * $item['quantity'];
                    }
                }
                else {
                    if (version_compare(WC()->version, '3', '>=')) {
                        $insuranceValue += wc_get_price_excluding_tax($item["data"], array('qty' => $item['quantity']));
                    }
                    else {
                        $insuranceValue += $item["data"]->get_price_excluding_tax() * $item['quantity'];
                    }
                }
                for ($i = 1; $i <= $item['quantity']; $i++) {
                    $canAddToExistingBox = false;
                    for ($j = 0; $j <= count($boxesWeights); $j++) {
                        $val = isset($boxesWeights[$j]) ? $boxesWeights[$j] : 0;
                        $newWeight = $val + $item['data']->get_weight();
                        if ($newWeight < $maxPackageWeight) {
                            $canAddToExistingBox = true;
                            $boxesWeights[$j] = $newWeight;
                            break;
                        }
                    }
                    if (!$canAddToExistingBox) {
                        $boxesWeights[] = $item['data']->get_weight();
                    }
                }
            }
            $numBoxes = count($boxesWeights);
            $this->logger->log("Num Boxes: " . $numBoxes);
            $shipments = $this->getRates($destCountry, $destRegion, $city, $destPostCode, $baseSubtotalInclTax, $boxesWeights, $numBoxes, array(), 1, $insuranceValue);
        }
        elseif ($shipmentConfigurationMode == self::SHIPMENT_CONFIGURATION_MODE_ONE_SHIPMENT_PER_SHOPPING_CART_MULTI_PARCEL) {
            $boxesWeights = array();
            $numBoxes = 0;
            $insuranceValue = 0;
            foreach ($request['contents'] as $item) {
                if ($this->helper->getShipmentsInsuranceMode() == Mbe_Shipping_Helper_Data::MBE_INSURANCE_WITH_TAXES) {
                    if (version_compare(WC()->version, '3', '>=')) {
                        $insuranceValue += wc_get_price_including_tax($item["data"], array('qty' => $item['quantity']));
                    }
                    else {
                        $insuranceValue += $item["data"]->get_price_including_tax() * $item['quantity'];
                    }
                }
                else {
                    if (version_compare(WC()->version, '3', '>=')) {
                        $insuranceValue += wc_get_price_excluding_tax($item["data"], array('qty' => $item['quantity']));
                    }
                    else {
                        $insuranceValue += $item["data"]->get_price_excluding_tax() * $item['quantity'];
                    }
                }
                $numBoxes += $item['quantity'];
                for ($i = 1; $i <= $item['quantity']; $i++) {
                    $boxesWeights[] = $item['data']->get_weight();
                }
            }
            $this->logger->log("Num Boxes: " . $numBoxes);
            $shipments = $this->getRates($destCountry, $destRegion, $city, $destPostCode, $baseSubtotalInclTax, $boxesWeights, $numBoxes, array(), 1, $insuranceValue);

        }


        //TODO- remove subzone if it is the same for all shipping methods

        $subZones = array();
        foreach ($shipments as $shipment) {

            if (!in_array($shipment->subzone_id, $subZones)) {
                array_push($subZones, $shipment->subzone_id);
            }

        }
        $useSubZone = false;
        if (count($subZones) > 1) {
            $useSubZone = true;
        }

        $result = array();
        if (empty($shipments)) {
            $errorTitle = 'Unable to retrieve shipping methods';
            $this->logger->log($errorTitle);
        }
        else {

            foreach ($shipments as $shipment) {
                if ($useSubZone) {
                    $currentRate = $this->_getRate($shipment->title_full, $shipment->shipment_code, $shipment->price);
                }
                else {
                    $currentRate = $this->_getRate($shipment->title, $shipment->shipment_code, $shipment->price);
                }

                $result[] = $currentRate;
            }
        }

        return $result;
    }


    private function getRates($destCountry, $destRegion, $city, $destPostCode, $baseSubtotalInclTax, $weight, $boxes, $oldResults = array(), $iteration = 1, $insuranceValue = 0)
    {
        $this->logger->log("getRates");

        $ws = new Mbe_Shipping_Model_Ws();

        $result = array();
        $newResults = array();

        $ratesHelper = new Mbe_Shipping_Helper_Rates();

        if ($ratesHelper->useCustomRates($destCountry)) {
            $totalWeight = $weight;
            if (is_array($weight)) {
                $totalWeight = 0;
                foreach ($weight as $singleWeight) {
                    $totalWeight += $singleWeight;
                }
            }

            $shipments = $ratesHelper->getCustomRates($destCountry, $destRegion, $city, $destPostCode, $totalWeight, $insuranceValue);
        }
        else {
            $shipments = $ws->estimateShipping($destCountry, $destRegion, $destPostCode, $weight, $boxes, $insuranceValue);
        }
        $this->logger->logVar($shipments, 'ws estimateShipping result');


        if ($shipments) {

            $allowedShipmentServicesArray = $this->helper->getAllowedShipmentServicesArray();

            foreach ($shipments as $shipment) {

                //TODO: check if is sufficient use service
                $shipmentMethod = $shipment->Service;

                $shipmentMethodKey = $shipment->Service . "_" . $shipment->IdSubzone;

                if (in_array($shipmentMethod, $allowedShipmentServicesArray)) {
                    $shipmentTitle = __($shipment->ServiceDesc, 'mail-boxes-etc');
                    if ($shipment->SubzoneDesc) {
                        $shipmentTitle .= " - " . __($shipment->SubzoneDesc, 'mail-boxes-etc');
                    }

                    $shipmentPrice = $shipment->NetShipmentTotalPrice;

                    $shipmentPrice = $this->applyFee($shipmentPrice, $boxes);


                    $shippingThreshold = $this->helper->getThresholdByShippingServrice($shipmentMethod);

                    if ($shippingThreshold != '' && $baseSubtotalInclTax >= $shippingThreshold) {
                        $shipmentPrice = 0;
                    }


                    //TODO: check if is necessary to save in some mode the courier data


                    $current = new stdClass();
                    $current->title = __($shipment->ServiceDesc, 'mail-boxes-etc');
                    $current->title_full = $shipmentTitle;
                    $current->method = $shipmentMethod;
                    $current->price = $shipmentPrice;
                    $current->subzone = $shipment->SubzoneDesc;
                    $current->subzone_id = $shipment->IdSubzone;
                    $current->shipment_code = $shipmentMethodKey;


                    $newResults[$shipmentMethodKey] = $current;

                }
            }
            if ($iteration == 1) {
                $result = $newResults;
            }
            else {

                foreach ($newResults as $newResultKey => $newResult) {
                    if (array_key_exists($newResultKey, $oldResults)) {
                        $newResult->price += $oldResults[$newResultKey]->price;
                        $result[$newResultKey] = $newResult;
                    }

                }
            }

        }

        return $result;
    }

    protected function _getRate($title, $method, $price)
    {
        $price = $this->helper->round($price);

        $rate = array('label' => $title, 'method' => $method, 'price' => $price);

        return $rate;
    }


    public function applyFee($value, $packages = 1)
    {
        $handlingType = $this->helper->getHandlingType();
        $handlingAction = $this->helper->getHandlingAction();
        $handlingFee = $this->helper->getHandlingFee();

        if ($handlingAction == self::HANDLING_TYPE_PER_SHIPMENT) {
            $packages = 1;
        }

        if (self::HANDLING_TYPE_FIXED == $handlingType) {
            //fixed
            $result = $value + $handlingFee * $packages;
        }
        else {
            //percent
            $result = $value * (100 + $handlingFee) / 100;
        }

        return $result;

    }

    public function isTrackingAvailable()
    {
        return true;
    }

}