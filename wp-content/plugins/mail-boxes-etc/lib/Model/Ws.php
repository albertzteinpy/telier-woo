<?php

class Mbe_Shipping_Model_Ws
{
    private $helper;
    protected $logger = null;
    private $ws;

    public function __construct()
    {
        $this->helper = new Mbe_Shipping_Helper_Data();
        $this->logger = new Mbe_Shipping_Helper_Logger();

        $wsLogPath = MBE_E_LINK_PLUGIN_DIR . 'log';

        $debug = $this->helper->debug();
        if ($debug) {
            $this->helper->checkDir($wsLogPath);
        }
        $this->ws = new MbeWs($wsLogPath, $debug);
    }

    public function getCustomer()
    {
        $wsUrl = $this->helper->getWsUrl();
        $wsUsername = $this->helper->getWsUsername();
        $wsPassword = $this->helper->getWsPassword();
        $system = $this->helper->getCountry();
        $cacheKey = md5($wsUrl . $wsUsername . $wsPassword . $system);
        $cacheGroup = "mbe";
        if (wp_cache_get($cacheKey, $cacheGroup)) {
            return wp_cache_get($cacheKey, $cacheGroup);
        }

        $result = false;
        if ($wsUrl && $wsUsername && $wsPassword) {
            $result = $this->ws->getCustomer($wsUrl, $wsUsername, $wsPassword, $system);
            if ($result) {
                wp_cache_add($cacheKey, $result, $cacheGroup, 60);
            }
            $this->logger->logVar($result, 'WS getCustomer');
        }
        return $result;
    }

    public function getCustomerPermission($permissionName)
    {
        $result = null;
        $customer = $this->getCustomer();
        if ($customer) {
            $result = $customer->Permissions->$permissionName;
        }

        return $result;
    }

    public function getAvailableOptions()
    {
        $wsUrl = $this->helper->getWsUrl();
        $wsUsername = $this->helper->getWsUsername();
        $wsPassword = $this->helper->getWsPassword();
        $result = false;
        if ($wsUrl && $wsUsername && $wsPassword) {
            $result = $this->ws->getAvailableOptions($wsUrl, $wsUsername, $wsPassword);
        }

        return $result;
    }

    public function getAllowedShipmentServices()
    {
        $wsUrl = $this->helper->getWsUrl();
        $wsUsername = $this->helper->getWsUsername();
        $wsPassword = $this->helper->getWsPassword();
        $system = $this->helper->getCountry();

        $result = array();
        if ($wsUrl && $wsUsername && $wsPassword) {
            $customer = $this->ws->getCustomer($wsUrl, $wsUsername, $wsPassword, $system);
            if ($customer && $customer->Enabled) {
                if (isset($customer->Permissions->enabledServices)) {
                    $enabledServices = $customer->Permissions->enabledServices;
                    $enabledServicesDesc = $customer->Permissions->enabledServicesDesc;

                    $enabledServicesArray = explode(",", $enabledServices);
                    $enabledServicesDescArray = explode(",", $enabledServicesDesc);

                    for ($i = 0; $i < count($enabledServicesArray); $i++) {
                        $service = $enabledServicesArray[$i];
                        $serviceDesc = $enabledServicesDescArray[$i];


                        $serviceDesc .= ' (' . $service . ')';

                        $currentShippingType = array(
                            'value' => $service,
                            'label' => $serviceDesc,
                        );

                        if (!in_array($currentShippingType, $result)) {
                            array_push($result, $currentShippingType);
                        }

                        //SHIPPING WITH INSURANCE
                        if (isset($customer->Permissions->canSpecifyInsurance) && $customer->Permissions->canSpecifyInsurance) {
                            $currentShippingWithInsuranceType = array(
                                'value' => $this->helper->convertShippingCodeWithInsurance($service),
                                'label' => $this->helper->convertShippingLabelWithInsurance($serviceDesc),
                            );
                            if (!in_array($currentShippingWithInsuranceType, $result)) {
                                array_push($result, $currentShippingWithInsuranceType);
                            }
                        }

                    }
                }
            }
        }
        return $result;
    }

    public function getLabelFromShipmentType($shipmentCode)
    {
        $result = $shipmentCode;
        $allowedShipmentServices = $this->getAllowedShipmentServices();
        foreach ($allowedShipmentServices as $allowedShipmentService) {
            if ($allowedShipmentService["value"] == $shipmentCode) {
                $result = $allowedShipmentService["label"];
                break;
            }
        }
        return $result;
    }

    private function convertInsuranceShipping($shippingList)
    {
        $result = false;
        if ($shippingList) {
            $newShippingList = array();
            foreach ($shippingList as $shipping) {

                if ($shipping->InsuranceAvailable) {
                    $newShipping = $shipping;
                    $newShipping->Service = $this->helper->convertShippingCodeWithInsurance($newShipping->Service);
                    $newShipping->ServiceDesc = $this->helper->convertShippingLabelWithInsurance($newShipping->ServiceDesc);
                    array_push($newShippingList, $newShipping);
                }
            }
            if (!empty($newShippingList)) {
                $result = $newShippingList;
            }
        }
        return $result;
    }

    public function estimateShipping($country, $region, $postCode, $weight, $boxes, $insuranceValue)
    {
        $this->logger->log('ESTIMATESHIPPING');

        $wsUrl = $this->helper->getWsUrl();
        $wsUsername = $this->helper->getWsUsername();
        $wsPassword = $this->helper->getWsPassword();
        $system = $this->helper->getCountry();

        $result = false;

        if ($wsUrl && $wsUsername && $wsPassword) {

            $length = $this->helper->getDefaultLength();
            $width = $this->helper->getDefaultWidth();
            $height = $this->helper->getDefaultHeight();

            $shipmentType = $this->helper->getDefaultShipmentType();

            $items = array();

            for ($i = 0; $i < $boxes; $i++) {
                $item = new stdClass;
                if (is_array($weight)) {
                    $item->Weight = $weight[$i];
                }
                else {
                    $item->Weight = $weight;
                }
                $item->Dimensions = new stdClass;
                $item->Dimensions->Lenght = $length;
                $item->Dimensions->Height = $height;
                $item->Dimensions->Width = $width;
                array_push($items, $item);
            }


            $this->logger->logVar($items, 'ESTIMATESHIPPING ITEMS');
            //Shipping without insurance
            $resultWithoutInsurance = $this->ws->estimateShipping($wsUrl, $wsUsername, $wsPassword, $shipmentType, $system, $country, $region, $postCode, $items, false, $insuranceValue);

            //Shipping with insurance
            $resultWithInsurance = $this->ws->estimateShipping($wsUrl, $wsUsername, $wsPassword, $shipmentType, $system, $country, $region, $postCode, $items, true, $insuranceValue);
            $resultWithInsurance = $this->convertInsuranceShipping($resultWithInsurance);

            if ($resultWithInsurance && $resultWithoutInsurance) {
                $result = array_merge($resultWithInsurance, $resultWithoutInsurance);
            }
            else {
                if ($resultWithInsurance) {
                    $result = $resultWithInsurance;
                }
                if ($resultWithoutInsurance) {
                    $result = $resultWithoutInsurance;
                }
            }

        }
        return $result;
    }

    public function createShipping($country, $region, $postCode, $weight, $boxes, $products, $service, $subzone, $notes, $firstName, $lastName, $companyName, $address, $phone, $city, $email, $goodsValue = 0.0, $reference = "", $isCod = false, $codValue = 0.0, $insurance = false, $insuranceValue = 0.0)
    {
        $this->logger->log('CREATE SHIPPING');
        $this->logger->logVar($goodsValue, 'GOODS VALUE');
        $this->logger->logVar(func_get_args(), 'CREATE SHIPPING ARGS');
        $wsUrl = $this->helper->getWsUrl();
        $wsUsername = $this->helper->getWsUsername();
        $wsPassword = $this->helper->getWsPassword();
        $system = $this->helper->getCountry();
        $result = false;
        if ($wsUrl && $wsUsername && $wsPassword) {


            $length = $this->helper->getDefaultLength();
            $width = $this->helper->getDefaultWidth();
            $height = $this->helper->getDefaultHeight();

            $shipmentType = $this->helper->getDefaultShipmentType();

            $items = array();

            for ($i = 0; $i < $boxes; $i++) {
                $item = new stdClass;
                if (is_array($weight)) {
                    $item->Weight = $weight[$i];
                }
                else {
                    $item->Weight = $weight;
                }
                $item->Dimensions = new stdClass;
                $item->Dimensions->Lenght = $length;
                $item->Dimensions->Height = $height;
                $item->Dimensions->Width = $width;
                array_push($items, $item);
            }


            $this->logger->logVar($items, 'CREATE SHIPPING ITEMS');


            $shipperType = $this->getShipperType();
            $result = $this->ws->createShipping($wsUrl, $wsUsername, $wsPassword, $shipmentType, $service, $subzone, $system, $notes, $firstName, $lastName, $companyName, $address, $phone, $city, $region, $country, $postCode, $email, $items, $products, $shipperType, $goodsValue, $reference, $isCod, $codValue, $insurance, $insuranceValue);
        }
        return $result;
    }

    public function getShipperType()
    {
        //COURIERLDV or MBE
        $customer = $this->getCustomer();
        $shipperType = "MBE";
        if ($customer->Permissions->canCreateCourierWaybill) {
            $shipperType = "COURIERLDV";
        }

        return $shipperType;
    }

    public function mustCloseShipments()
    {
        $result = true;
        $customer = $this->getCustomer();
        if ($customer->Permissions->canCreateCourierWaybill) {
            $result = false;
        }
        return $result;
    }

    public function getCustomerMaxParcelWeight()
    {
        $customer = $this->getCustomer();
        return $customer->Permissions->maxParcelWeight;
    }

    public function getCustomerMaxShipmentWeight()
    {
        $customer = $this->getCustomer();
        return $customer->Permissions->maxShipmentWeight;
    }

    public function isCustomerActive()
    {
        $result = false;
        $customer = $this->getCustomer();
        if ($customer) {
            $result = $customer->Enabled;
        }
        return $result;
    }

    public function closeShipping(array $shipmentIds)
    {
        $trackingNumbers = array();
        foreach ($shipmentIds as $shipmentId) {
            $tracks = $this->helper->getTrackings($shipmentId);
            foreach ($tracks as $track) {
                array_push($trackingNumbers, $track);
            }
        }
        $this->closeTrackingNumbers($trackingNumbers);

    }

    public function closeTrackingNumbers(array $trackingNumbers)
    {
        $this->logger->log('CLOSE SHIPPING');

        $wsUrl = $this->helper->getWsUrl();
        $wsUsername = $this->helper->getWsUsername();
        $wsPassword = $this->helper->getWsPassword();
        $system = $this->helper->getCountry();

        $result = false;

        if ($wsUrl && $wsUsername && $wsPassword) {
            $result = $this->ws->closeShipping($wsUrl, $wsUsername, $wsPassword, $system, $trackingNumbers);

            if ($result) {
                foreach ($trackingNumbers as $trackingNumber) {
                    $filePath = $this->helper->getTrackingFilePath($trackingNumber);
                    file_put_contents($filePath, $result->Pdf);
                }
            }
        }

        return $result;
    }
}
