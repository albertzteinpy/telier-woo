<?php

class Mbe_Shipping_Helper_Logger
{
    public static $log = true;
    //private static $_logPath = MBE_E_LINK_PLUGIN_DIR . 'log';
    private static $_logFileName = 'mbe_general.log';

    private $helper;

    public function __construct()
    {
        $this->helper = new Mbe_Shipping_Helper_Data();
    }

    public function log($message)
    {
        if ($this->helper->debug()) {

            if (!file_exists(MBE_E_LINK_PLUGIN_DIR . 'log')) {
                mkdir(MBE_E_LINK_PLUGIN_DIR . 'log', 0755, true);
            }
            $row = date_format(new DateTime(), 'Y-m-d\TH:i:s\Z');
            $row .= " - ";
            $row .= $message . "\n\r";
            file_put_contents(MBE_E_LINK_PLUGIN_DIR . 'log' . DIRECTORY_SEPARATOR . self::$_logFileName, $row, FILE_APPEND);
        }
    }


    public function logVar($var, $message = null)
    {
        if ($message) {
            $this->log($message);
        }
        $this->log(print_r($var, true));
    }
}