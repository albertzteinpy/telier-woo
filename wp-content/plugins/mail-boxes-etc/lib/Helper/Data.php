<?php

class Mbe_Shipping_Helper_Data
{
    private $_mbeMediaDir = "mbe";
    private $_csvDir = 'csv';
    const MBE_SHIPPING_PREFIX = "mbe_shipping_";
    const MBE_SETTINGS = 'woocommerce_wf_mbe_shipping_settings';
    const SHIPMENT_SOURCE_TRACKING_NUMBER = "woocommerce_mbe_tracking_number";
    const SHIPMENT_SOURCE_TRACKING_FILENAME = 'woocommerce_mbe_tracking_filename';

    //MAIN
    const CONF_DEBUG = 'mbe_debug';
    const XML_PATH_ENABLED = 'mbe_active';
    const XML_PATH_COUNTRY = 'country';
    //WS
    const XML_PATH_WS_URL = 'url';
    const XML_PATH_WS_USERNAME = 'username';
    const XML_PATH_WS_PASSWORD = 'password';
    //OPTIONS
    const XML_PATH_DESCRIPTION = 'description';
    const XML_PATH_DEFAULT_SHIPMENT_TYPE = 'default_shipment_type';
    const XML_PATH_ALLOWED_SHIPMENT_SERVICES = 'allowed_shipment_services';
    const XML_PATH_SHIPMENT_CONFIGURATION_MODE = 'default_shipment_mode';
    const XML_PATH_DEFAULT_LENGTH = 'default_length';
    const XML_PATH_DEFAULT_WIDTH = 'default_width';
    const XML_PATH_DEFAULT_HEIGHT = 'default_height';
    const XML_PATH_MAX_PACKAGE_WEIGHT = 'max_package_weight';
    const XML_PATH_MAX_SHIPMENT_WEIGHT = 'max_shipment_weight';
    const XML_PATH_HANDLING_TYPE = 'handling_type';
    const XML_PATH_HANDLING_ACTION = 'handling_action';
    const XML_PATH_HANDLING_FEE = 'handling_fee';
    const XML_PATH_HANDLING_FEE_ROUNDING = 'handling_fee_rounding';
    const XML_PATH_HANDLING_FEE_ROUNDING_AMOUNT = 'handling_fee_rounding_amount';
    const XML_PATH_SALLOWSPECIFIC = 'sallowspecific';
    const XML_PATH_SPECIFICCOUNTRY = 'specificcountry';
    const XML_PATH_SORT_ORDER = 'sort_order';
    const XML_PATH_MAXIMUM_TIME_FOR_SHIPPING_BEFORE_THE_END_OF_THE_DAY = 'maximum_time_for_shipping_before_the_end_of_the_day';
    const XML_PATH_SPECIFICERRMSG = 'specificerrmsg';
    const XML_PATH_WEIGHT_TYPE = 'weight_type';
    const XML_PATH_SHIPMENTS_CLOSURE_MODE = 'shipments_closure_mode';
    const XML_PATH_SHIPMENTS_CLOSURE_TIME = 'shipments_closure_time';
    const XML_PATH_SHIPMENTS_CREATION_MODE = 'shipments_creation_mode';


    const XML_PATH_SHIPMENTS_CSV = 'shipments_csv';
    const XML_PATH_SHIPMENTS_CSV_FILE = 'shipments_csv_file';

    const XML_PATH_SHIPMENTS_CSV_MODE = 'shipments_csv_mode';
    const XML_PATH_SHIPMENTS_CSV_INSURANCE_MIN = 'mbe_shipments_csv_insurance_min';
    const XML_PATH_SHIPMENTS_CSV_INSURANCE_PERCENTAGE = 'mbe_shipments_csv_insurance_per';
    const XML_PATH_SHIPMENTS_INSURANCE_MODE = 'mbe_shipments_ins_mode';


    const XML_PATH_THRESHOLD = 'mbelimit';

    //const

    const MBE_SHIPMENT_STATUS_CLOSED = "Closed";
    const MBE_SHIPMENT_STATUS_OPEN = "Opened";

    const MBE_CLOSURE_MODE_AUTOMATICALLY = 'automatically';
    const MBE_CLOSURE_MODE_MANUALLY = 'manually';

    const MBE_CREATION_MODE_AUTOMATICALLY = 'automatically';
    const MBE_CREATION_MODE_MANUALLY = 'manually';


    const MBE_CSV_MODE_DISABLED = 'disabled';
    const MBE_CSV_MODE_TOTAL = 'total';
    const MBE_CSV_MODE_PARTIAL = 'partial';
    const MBE_INSURANCE_WITH_TAXES = 'insurance_with_taxes';
    const MBE_INSURANCE_WITHOUT_TAXES = 'insurance_without_taxes';
    const MBE_SHIPPING_WITH_INSURANCE_CODE_SUFFIX = '_INSURANCE';
    const MBE_SHIPPING_WITH_INSURANCE_LABEL_SUFFIX = ' + Insurance';
    const MBE_SHIPPING_TRACKING_SEPARATOR = ',';
    const MBE_SHIPPING_TRACKING_SEPARATOR__OLD = '--';


    public function __construct()
    {
        $this->_options = get_option(self::MBE_SETTINGS);
    }

    public function checkMbeDir($path)
    {
        $this->checkDir($path);
        if (!file_exists($path)) {
            mkdir($path);
        }
    }

    public function checkDir($dir)
    {
        if (!file_exists($dir)) {
            mkdir($dir, 0755, true);
        }
    }

    //TODO: check if is necessary to specify storeid or get current storeid
    public function isEnabled()
    {
        /** @var $ws Mbe_Shipping_Model_Ws */
        $ws = new Mbe_Shipping_Model_Ws();
        $result = $ws->isCustomerActive() && $this->getOption(self::XML_PATH_ENABLED);
        return $result;
    }

    public function debug()
    {
        $result = ($this->getOption(self::CONF_DEBUG) == 1);
        return $result;
    }


    public function enabledCountry($country = null)
    {
        if ($this->getSallowspecific()) {
            return in_array($country, $this->getSpecificcountry());
        }
        return true;
    }

    public function getOption($field)
    {
        if (isset($this->_options[$field])) {
            return $this->_options[$field];
        }
        return false;
    }

    public function getCountry()
    {
        return $this->getOption(self::XML_PATH_COUNTRY);
    }


    public function getWsUrl()
    {
        return $this->getOption(self::XML_PATH_WS_URL);
    }

    public function getWsUsername()
    {
        return $this->getOption(self::XML_PATH_WS_USERNAME);
    }

    public function getWsPassword()
    {
        return $this->getOption(self::XML_PATH_WS_PASSWORD);
    }

    public function getDescription()
    {
        return $this->getOption(self::XML_PATH_DESCRIPTION);
    }

    public function getDefaultShipmentType()
    {
        return $this->getOption(self::XML_PATH_DEFAULT_SHIPMENT_TYPE);
    }

    public function getAllowedShipmentServices()
    {
        return $this->getOption(self::XML_PATH_ALLOWED_SHIPMENT_SERVICES);
    }

    public function convertShippingCodeWithInsurance($code)
    {
        return $code . self::MBE_SHIPPING_WITH_INSURANCE_CODE_SUFFIX;
    }

    public function convertShippingLabelWithInsurance($label)
    {
        return $label . self::MBE_SHIPPING_WITH_INSURANCE_LABEL_SUFFIX;
    }

    public function convertShippingCodeWithoutInsurance($code)
    {
        return str_replace(self::MBE_SHIPPING_WITH_INSURANCE_CODE_SUFFIX, "", $code);
    }

    public function isShippingWithInsurance($code)
    {
        $result = false;
        /*
        $shippingSuffix = substr($code, -strlen(self::MBE_SHIPPING_WITH_INSURANCE_CODE_SUFFIX));
        if ($shippingSuffix == self::MBE_SHIPPING_WITH_INSURANCE_CODE_SUFFIX) {
            $result = true;
        }
        */
        if (strpos($code, self::MBE_SHIPPING_WITH_INSURANCE_CODE_SUFFIX) !== false) {
            $result = true;
        }
        return $result;
    }


    public function getAllowedShipmentServicesArray()
    {
        $allowedShipmentServicesArray = $this->getAllowedShipmentServices();
        $ws = new Mbe_Shipping_Model_Ws();
        $canSpecifyInsurance = $ws->getCustomerPermission('canSpecifyInsurance');

        $result = array();
        foreach ($allowedShipmentServicesArray as $item) {
            $canAdd = true;
            if (!$canSpecifyInsurance) {
                if (strpos($item, self::MBE_SHIPPING_WITH_INSURANCE_CODE_SUFFIX) !== false) {
                    $canAdd = false;
                }
            }
            if ($canAdd) {
                array_push($result, $item);
            }
        }

        return $result;
    }

    public function getShipmentConfigurationMode()
    {
        return $this->getOption(self::XML_PATH_SHIPMENT_CONFIGURATION_MODE);
    }

    public function getDefaultLength()
    {
        return $this->getOption(self::XML_PATH_DEFAULT_LENGTH);
    }

    public function getDefaultWidth()
    {
        return $this->getOption(self::XML_PATH_DEFAULT_WIDTH);
    }

    public function getDefaultHeight()
    {
        return $this->getOption(self::XML_PATH_DEFAULT_HEIGHT);
    }

    public function getMaxPackageWeight()
    {
        $result = $this->getOption(self::XML_PATH_MAX_PACKAGE_WEIGHT);
        $ws = new Mbe_Shipping_Model_Ws();

        if ($this->getDefaultShipmentType() == "ENVELOPE") {
            $maxParcelWeight = 0.5;
        }
        else {
            $maxParcelWeight = $ws->getCustomerPermission("maxParcelWeight");
        }

        if ($maxParcelWeight > 0 && $maxParcelWeight < $result) {
            $result = $maxParcelWeight;
        }
        return $result;
    }

    public function getMaxShipmentWeight($storeId = null)
    {
        $result = $this->getOption(self::XML_PATH_MAX_SHIPMENT_WEIGHT);
        $ws = new Mbe_Shipping_Model_Ws();

        if ($this->getDefaultShipmentType() == "ENVELOPE") {
            $maxShipmentWeight = 0.5;
        }
        else {
            $maxShipmentWeight = $ws->getCustomerPermission("maxShipmentWeight");
        }

        if ($maxShipmentWeight > 0 && $maxShipmentWeight < $result) {
            $result = $maxShipmentWeight;
        }
        return $result;
    }

    public function getHandlingType()
    {
        return $this->getOption(self::XML_PATH_HANDLING_TYPE);
    }

    public function getHandlingAction()
    {
        return $this->getOption(self::XML_PATH_HANDLING_ACTION);
    }

    public function getHandlingFee()
    {
        return $this->getOption(self::XML_PATH_HANDLING_FEE);
    }

    public function getHandlingFeeRounding()
    {
        return $this->getOption(self::XML_PATH_HANDLING_FEE_ROUNDING);
    }

    public function getHandlingFeeRoundingAmount()
    {
        $result = 1;
        if ($this->getOption(self::XML_PATH_HANDLING_FEE_ROUNDING_AMOUNT) == 2) {
            $result = 0.5;
        }
        return $result;
    }

    public function getSallowspecific()
    {
        return $this->getOption(self::XML_PATH_SALLOWSPECIFIC);
    }

    public function getSpecificcountry()
    {
        return $this->getOption(self::XML_PATH_SPECIFICCOUNTRY);
    }

    public function getSortOrder()
    {
        return $this->getOption(self::XML_PATH_SORT_ORDER);
    }

    public function getMaximumTimeForShippingBeforeTheEndOfTheDay()
    {
        return $this->getOption(self::XML_PATH_MAXIMUM_TIME_FOR_SHIPPING_BEFORE_THE_END_OF_THE_DAY);
    }

    public function getSpecificerrmsg()
    {
        return $this->getOption(self::XML_PATH_SPECIFICERRMSG);
    }

    public function getWeightType()
    {
        return $this->getOption(self::XML_PATH_WEIGHT_TYPE);
    }

    public function getShipmentsClosureMode()
    {
        return $this->getOption(self::XML_PATH_SHIPMENTS_CLOSURE_MODE);
    }

    public function getShipmentsCreationMode()
    {
        return $this->getOption(self::XML_PATH_SHIPMENTS_CREATION_MODE);
    }

    public function getShipmentsClosureTime()
    {
        return $this->getOption(self::XML_PATH_SHIPMENTS_CLOSURE_TIME);
    }

    public function isCreationAutomatically()
    {
        return $this->getShipmentsCreationMode() == self::MBE_CREATION_MODE_AUTOMATICALLY;
    }

    public function isClosureAutomatically()
    {
        return $this->getShipmentsClosureMode() == self::MBE_CLOSURE_MODE_AUTOMATICALLY;
    }

    public function hasTracking($post_id = null)
    {
        $post_id = is_null($post_id) ? $_GET['post'] : $post_id;
        $value = get_post_meta($post_id, self::SHIPMENT_SOURCE_TRACKING_NUMBER, true);
        return !empty($value);
    }


    public function round($value)
    {
        $result = $value;
        $handlingFeeRounding = $this->getHandlingFeeRounding();
        $handlingFeeRoundingAmount = $this->getHandlingFeeRoundingAmount();

        if ($handlingFeeRounding == 2) {

            if ($handlingFeeRoundingAmount == 1) {
                $result = round($value, 0);
            }
            else {
                $result = round($value, 2);
            }

        }
        elseif ($handlingFeeRounding == 3) {
            if ($handlingFeeRoundingAmount == 1) {
                $result = floor($value);
            }
            else {
                $result = floor($value * 2) / 2;
            }
        }
        elseif ($handlingFeeRounding == 4) {
            if ($handlingFeeRoundingAmount == 1) {
                $result = ceil($value);
            }
            else {
                $result = ceil($value * 2) / 2;
            }
        }
        return $result;
    }

    public function getNameFromLabel($label)
    {
        $name = $label;
        $name = strtolower($name);
        $name = str_replace(" ", "_", $name);
        $name = str_replace(" ", "_", $name);
        return $name;
    }

    public function getThresholdByShippingServrice($shippingService)
    {
        $shippingService = strtolower($shippingService);
        return $this->getOption(self::XML_PATH_THRESHOLD . "_" . $shippingService);
    }


    public function getTrackingStatus($trackingNumber)
    {
        $result = self::MBE_SHIPMENT_STATUS_OPEN;

        $mbeDir = $this->mbeUploadDir();
        $filePath = $mbeDir . DIRECTORY_SEPARATOR . 'MBE_' . $trackingNumber . "_closed.pdf";

        if (file_exists($filePath)) {
            $result = self::MBE_SHIPMENT_STATUS_CLOSED;
        }
        return $result;
    }

    public function isShippingOpen($shipmentId)
    {
        $result = true;
        $tracks = $this->getTrackings($shipmentId);
        foreach ($tracks as $track) {
            $result = $result && $this->isTrackingOpen($track);
        }
        return $result;
    }

    public function getFileNames($orderId)
    {
        $result = array();
        $files = get_post_meta($orderId, self::SHIPMENT_SOURCE_TRACKING_FILENAME, true);
        if ($files != '') {
            if (strpos($files, Mbe_Shipping_Helper_Data::MBE_SHIPPING_TRACKING_SEPARATOR) !== false) {
                $result = explode(Mbe_Shipping_Helper_Data::MBE_SHIPPING_TRACKING_SEPARATOR, $files);
            }
            else {
                $result = explode(Mbe_Shipping_Helper_Data::MBE_SHIPPING_TRACKING_SEPARATOR__OLD, $files);
            }
        }
        return $result;
    }

    public function getTrackings($shipmentId)
    {
        $tracking = get_post_meta($shipmentId, self::SHIPMENT_SOURCE_TRACKING_NUMBER, true);
        if (strpos($tracking, Mbe_Shipping_Helper_Data::MBE_SHIPPING_TRACKING_SEPARATOR) !== false) {
            $value = explode(Mbe_Shipping_Helper_Data::MBE_SHIPPING_TRACKING_SEPARATOR, $tracking);

        }
        else {
            $value = explode(Mbe_Shipping_Helper_Data::MBE_SHIPPING_TRACKING_SEPARATOR__OLD, $tracking);
        }

        return is_array($value) ? (array_filter($value, function ($value) {
            return $value !== '';
        })) : $value;
    }

    public function getTrackingsString($shipmentId)
    {
        $result = get_post_meta($shipmentId, self::SHIPMENT_SOURCE_TRACKING_NUMBER, true);
        //compatibility replace
        if (strpos($result, Mbe_Shipping_Helper_Data::MBE_SHIPPING_TRACKING_SEPARATOR__OLD) !== false) {
            $result = str_replace(Mbe_Shipping_Helper_Data::MBE_SHIPPING_TRACKING_SEPARATOR__OLD, Mbe_Shipping_Helper_Data::MBE_SHIPPING_TRACKING_SEPARATOR, $result);
        }
        return $result;
    }


    public function isTrackingOpen($trackingNumber)
    {
        return $this->getTrackingStatus($trackingNumber) == self::MBE_SHIPMENT_STATUS_OPEN;
    }

    public function mbeUploadDir()
    {
        $value = wp_upload_dir();
        //$path = $value['path'] . '/../..' . DIRECTORY_SEPARATOR . $this->_mbeMediaDir;
        $path = $value['basedir'] . DIRECTORY_SEPARATOR . $this->_mbeMediaDir;
        $this->checkMbeDir($path);
        return $path;
    }

    public function mbeCsvUploadDir()
    {
        $result = $this->mbeUploadDir() . DIRECTORY_SEPARATOR . $this->_csvDir;
        $this->checkMbeDir($result);
        return $result;
    }

    public function getMbeCsvUploadUrl()
    {
        $result = get_home_url() . '/wp-content/uploads/' . $this->_mbeMediaDir . '/' . $this->_csvDir;
        return $result;
    }

    public function getShipmentFilePath($shipmentIncrementId, $ext)
    {
        return $this->mbeUploadDir() . DIRECTORY_SEPARATOR . $shipmentIncrementId . "." . $ext;
    }

    public function getTrackingFilePath($trackingNumber)
    {
        $mbeDir = $this->mbeUploadDir();
        $filePath = $mbeDir . DIRECTORY_SEPARATOR . 'MBE_' . $trackingNumber . "_closed.pdf";
        return $filePath;
    }

    public function isMbeShipping($order)
    {
        $shippingMethod = $this->getShippingMethod($order);
        if (strpos($shippingMethod, 'wf_mbe_shipping') !== false) {
            return true;
        }
        return false;
    }

    public function getShippingMethod($order)
    {
        if (version_compare(WC()->version, '2.1', '>=')) {
            $order_item_id = null;
            foreach ($order->get_items('shipping') as $key => $item) {
                $order_item_id = $key;
            }
            if ($order_item_id) {
                return wc_get_order_item_meta($order_item_id, 'method_id', true);
            }
            else {
                return false;
            }
        }
        else {
            if (version_compare(WC()->version, '3', '>=')) {
                $orderId = $order->get_id();
            }
            else {
                $orderId = $order->id;
            }
            return get_post_meta($orderId, '_shipping_method', true);
        }
    }

    public function getServiceName($order)
    {
        if (version_compare(WC()->version, '2.1', '>=')) {
            foreach ($order->get_items('shipping') as $key => $item) {
                return $item['name'];
            }
        }
        else {
            if (version_compare(WC()->version, '3', '>=')) {
                $orderId = $order->get_id();
            }
            else {
                $orderId = $order->id;
            }
            return get_post_meta($orderId, '_shipping_method_title', true);
        }
    }

    public function getProduct($id_product)
    {
        if (version_compare(WC()->version, '2.1', '>=')) {
            return new WC_Product($id_product);
        }
        else {
            return get_product($id_product);
        }
    }

    public function getProductVariation($id_product)
    {
        return new WC_Product_Variation($id_product);
    }

    public function getProductFromItem($item)
    {
        $product_id = $item['product_id'];
        $variation_id = $item['variation_id'];
        if ($variation_id) {
            $product = $this->getProductVariation($variation_id);
        }
        else {
            $product = $this->getProduct($product_id);
        }
        return $product;
    }

    public function getProductSkuFromItem($item)
    {
        $product = $this->getProductFromItem($item);
        $sku = $product->get_sku();
        return $sku;
    }

    public function getProductTitleFromItem($item)
    {
        $product = $this->getProductFromItem($item);
        if (version_compare(WC()->version, '3.0', '>=')) {
            $title = $product->get_name();
        }
        else {
            $title = $product->get_title();
        }

        if (version_compare(WC()->version, '3.0', '>=')) {
            $productType = $product->get_type();
        }
        else {
            $productType = $product->product_type;
        }

        $variationsString = '';
        if ($productType == 'variation') {
            if (version_compare(WC()->version, '3.0', '>=')) {
                $available_variations = $product->get_variation_attributes();
            }
            else {
                $available_variations = $product->variation_data;
            }
            foreach ($available_variations as $key => $value) {
                $attributeLabel = str_replace('attribute_', '', $key);
                if ($variationsString != '') {
                    $variationsString .= ', ';
                }
                $variationsString .= $attributeLabel . ': ' . $value;
            }
        }

        if ($variationsString) {
            $title .= ' (' . $variationsString . ')';
        }

        return $title;
    }

    public function getShipmentsCsv()
    {
        return $this->getOption(self::XML_PATH_SHIPMENTS_CSV);
    }

    public function getShipmentsCsvFile()
    {
        return $this->getOption(self::XML_PATH_SHIPMENTS_CSV_FILE);
    }

    public function getShipmentsCsvFileUrl()
    {
        $result = $this->getShipmentsCsvFile();
        if ($result) {
            $result = $this->getMbeCsvUploadUrl() . $result;
        }
        return $result;
    }

    public function getShipmentsCsvTemplateFileUrl()
    {
        $result = MBE_E_LINK_PLUGIN_DIR . 'mbe_csv_template.csv';
        return $result;
    }

    public function getShipmentsCsvMode()
    {
        return $this->getOption(self::XML_PATH_SHIPMENTS_CSV_MODE);
    }

    public function getShipmentsCsvInsuranceMin()
    {
        return $this->getOption(self::XML_PATH_SHIPMENTS_CSV_INSURANCE_MIN);
    }

    public function getShipmentsCsvInsurancePercentage()
    {
        return $this->getOption(self::XML_PATH_SHIPMENTS_CSV_INSURANCE_PERCENTAGE);
    }

    public function getShipmentsInsuranceMode()
    {
        return $this->getOption(self::XML_PATH_SHIPMENTS_INSURANCE_MODE);
    }

}