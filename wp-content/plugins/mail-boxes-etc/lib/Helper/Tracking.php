<?php

class Mbe_Shipping_Helper_Tracking
{
    const ITALIAN_URL = "https://www.mbe.it/it/tracking?c=";
    const SPAIN_URL = "http://www.mbe.es/utility-menu/seguimiento-de-tu-envio/?accio=track&param1=";
    const GERMANY_URL = "http://www.mbe.de/sendung-verfolgen/?accio=track&param1=";
    const AUSTRIA_URL = "http://www.mbe.at/sendung-verfolgen/?accio=track&param1=";
    const FRANCE_URL = "http://www.mbefrance.fr/nc/utility-menu/suivi-de-votre-expedition/?accio=track&param1=";

    public function getTrackingUrlBySystem($system)
    {
        $result = "";
        if ($system == "IT") {
            $result = self::ITALIAN_URL;
        } elseif ($system == "ES") {
            $result = self::SPAIN_URL;
        } elseif ($system == "DE") {
            $result = self::GERMANY_URL;
        } elseif ($system == "AT") {
            $result = self::AUSTRIA_URL;
        } elseif ($system == "FR") {
            $result = self::FRANCE_URL;
        }
        return $result;
    }
}

