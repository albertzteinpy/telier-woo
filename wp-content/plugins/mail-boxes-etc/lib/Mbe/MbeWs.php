<?php

class MbeWs
{

    private $_logPath;
    private $_logFileName = 'mbe_ws.log';
    private $_log = true;

    public function __construct($logPath, $log = true)
    {

        $this->_logPath = $logPath;
        $this->_log = $log;
    }

    private function log($message)
    {
        if ($this->_log) {
            $row = date_format(new DateTime(), 'Y-m-d\TH:i:s\Z');
            $row .= " - ";
            $row .= $message . "\n\r";
            file_put_contents($this->_logPath . DIRECTORY_SEPARATOR . $this->_logFileName, $row, FILE_APPEND);
        }
    }

    public function logVar($var, $message = null)
    {
        if ($this->_log) {
            if ($message) {
                $this->log($message);
            }
            $this->log(print_r($var, true));
        }
    }

    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getCustomer($ws, $username, $password, $system)
    {
        $this->log('GET CUSTOMER');
        $result = false;

        try {
            $soapClient = new SoapClient($ws, array('encoding' => 'utf-8', 'trace' => 1));
            $internalReferenceID = $this->generateRandomString();

            //WS ARGS
            $args = new stdClass;
            $args->RequestContainer = new stdClass;

            $args->RequestContainer->Action = "GET";

            $args->RequestContainer->SystemType = $system;

            $args->RequestContainer->Customer = new stdClass;
            $args->RequestContainer->Customer->Login = "";

            $args->RequestContainer->Credentials = new stdClass;
            $args->RequestContainer->Credentials->Username = $username;
            $args->RequestContainer->Credentials->Passphrase = $password;

            $args->RequestContainer->InternalReferenceID = $internalReferenceID;

            $args->RequestContainer->Action = "GET";

            $this->logVar($args, 'GET CUSTOMER ARGS');
            $soapResult = $soapClient->__soapCall("ManageCustomerRequest", array($args));

            $lastResponse = $soapClient->__getLastResponse();
            $this->logVar($lastResponse, 'GET CUSTOMER RESPONSE');

            if (isset($soapResult->RequestContainer->Errors)) {
                $this->logVar($soapResult->RequestContainer->Errors, 'GET CUSTOMER ERRORS');
            }

            if (isset($soapResult->RequestContainer->Status) && $soapResult->RequestContainer->Status == "OK") {
                //if (isset($soapResult->RequestContainer->InternalReferenceID) && $soapResult->RequestContainer->InternalReferenceID == $internalReferenceID) {
                $result = $soapResult->RequestContainer->Customer;
                //}
            }
        }
        catch (Exception $e) {
            $this->log('GET CUSTOMER EXCEPTION');
            $this->log($e->getMessage());
        }
        $this->logVar($result, 'GET CUSTOMER RESULT');
        return $result;
    }


    public function estimateShipping($ws, $username, $password, $shipmentType, $system, $country, $region, $postCode, $items, $insurance = false, $insuranceValue = 0.00)
    {
        $this->log('ESTIMATE SHIPPING');
        $result = false;

        try {
            $soapClient = new SoapClient($ws, array('encoding' => 'utf-8', 'trace' => 1));
            $internalReferenceID = $this->generateRandomString();

            //WS ARGS
            $args = new stdClass;
            $args->RequestContainer = new stdClass;
            $args->RequestContainer->System = $system;

            $args->RequestContainer->Credentials = new stdClass;
            $args->RequestContainer->Credentials->Username = $username;
            $args->RequestContainer->Credentials->Passphrase = $password;

            $args->RequestContainer->InternalReferenceID = $internalReferenceID;


            $args->RequestContainer->ShippingParameters = new stdClass;

            $args->RequestContainer->ShippingParameters->DestinationInfo = new stdClass;

            $args->RequestContainer->ShippingParameters->DestinationInfo->ZipCode = $postCode;
            $args->RequestContainer->ShippingParameters->DestinationInfo->City = $region;
            //$args->RequestContainer->ShippingParameters->DestinationInfo->State = $region;
            $args->RequestContainer->ShippingParameters->DestinationInfo->Country = $country;
            //$args->RequestContainer->ShippingParameters->DestinationInfo->idSubzone = "";

            $args->RequestContainer->ShippingParameters->ShipType = "EXPORT";

            $args->RequestContainer->ShippingParameters->PackageType = $shipmentType;

            $args->RequestContainer->ShippingParameters->Items = $items;

            $args->RequestContainer->ShippingParameters->Insurance = $insurance;
            if ($insurance) {
                $args->RequestContainer->ShippingParameters->InsuranceValue = $insuranceValue;
            }

            $this->logVar($args, 'ESTIMATE SHIPPING ARGS');

            $soapResult = $soapClient->__soapCall("ShippingOptionsRequest", array($args));

            $lastResponse = $soapClient->__getLastResponse();
            $this->logVar($lastResponse, 'ESTIMATE SHIPPING RESPONSE');

            if (isset($soapResult->RequestContainer->Errors)) {
                $this->logVar($soapResult->RequestContainer->Errors, 'ESTIMATE SHIPPING ERRORS');
            }

            if (isset($soapResult->RequestContainer->Status) && $soapResult->RequestContainer->Status == "OK") {
                if (isset($soapResult->RequestContainer->InternalReferenceID) && $soapResult->RequestContainer->InternalReferenceID == $internalReferenceID) {
                    if (isset($soapResult->RequestContainer->ShippingOptions->ShippingOption)) {
                        if (is_array($soapResult->RequestContainer->ShippingOptions->ShippingOption)) {
                            $result = $soapResult->RequestContainer->ShippingOptions->ShippingOption;
                        }
                        else {
                            $result = array($soapResult->RequestContainer->ShippingOptions->ShippingOption);
                        }
                    }
                }
            }

        }
        catch (Exception $e) {
            $this->log('ESTIMATE SHIPPING EXCEPTION');
            $this->log($e->getMessage());
        }
        $this->logVar($result, 'ESTIMATE SHIPPING RESULT');
        return $result;
    }

    public function createShipping($ws, $username, $password, $shipmentType, $service, $subZone, $system, $notes, $firstName, $lastName, $companyName, $address, $phone, $city, $state, $country, $postCode, $email, $items, $products, $shipperType = 'MBE', $goodsValue = 0.0, $reference = "", $isCod = false, $codValue = 0.0, $insurance = false, $insuranceValue = 0.0)
    {
        $this->log('CREATE SHIPPING');

        $this->logVar(func_get_args(), 'CREATE SHIPPING');

        $result = false;


        try {
            $soapClient = new SoapClient($ws, array('encoding' => 'utf-8', 'trace' => 1));
            $internalReferenceID = $this->generateRandomString();

            //WS ARGS
            $args = new stdClass;
            $args->RequestContainer = new stdClass;
            $args->RequestContainer->System = $system;
            //$args->RequestContainer->Customer = new stdClass;
            //$args->RequestContainer->Customer->Login = "";

            $args->RequestContainer->Credentials = new stdClass;
            $args->RequestContainer->Credentials->Username = $username;
            $args->RequestContainer->Credentials->Passphrase = $password;

            $args->RequestContainer->InternalReferenceID = $internalReferenceID;

            //RequestContainer -> Recipient
            $args->RequestContainer->Recipient = new stdClass;

            $recipientName = $companyName;
            $RecipientCompanyName = $firstName . " " . $lastName;
            $recipientName = substr($recipientName, 0, 35);
            $RecipientCompanyName = substr($RecipientCompanyName, 0, 35);
            if (empty($recipientName)) {
                $recipientName = $RecipientCompanyName;
            }
            $args->RequestContainer->Recipient->Name = $recipientName;
            //$args->RequestContainer->Recipient->LastName = $lastName;
            $args->RequestContainer->Recipient->CompanyName = $RecipientCompanyName;
            $args->RequestContainer->Recipient->Address = $address;
            $args->RequestContainer->Recipient->Phone = $phone;
            $args->RequestContainer->Recipient->ZipCode = $postCode;
            $args->RequestContainer->Recipient->City = $city;
            $args->RequestContainer->Recipient->State = $state;
            $args->RequestContainer->Recipient->Country = $country;
            $args->RequestContainer->Recipient->Email = $email;
            if ($subZone) {
                $args->RequestContainer->Recipient->SubzoneId = $subZone;
            }

            //RequestContainer -> Shipment
            $args->RequestContainer->Shipment = new stdClass;

            $args->RequestContainer->Shipment->ShipperType = $shipperType;//"MBE";//COURIERLDV - MBE
            $args->RequestContainer->Shipment->Description = "ECOMMERCE SHOP PURCHASE";
            $args->RequestContainer->Shipment->COD = $isCod;
            if ($isCod) {
                $args->RequestContainer->Shipment->CODValue = $codValue;
                $args->RequestContainer->Shipment->MethodPayment = "CASH";//CASH - CHECK
            }

            $args->RequestContainer->Shipment->Insurance = $insurance;
            if ($insurance) {
                $args->RequestContainer->Shipment->InsuranceValue = $insuranceValue;
            }

            $args->RequestContainer->Shipment->Service = $service;//SEE /SSE

            //$args->RequestContainer->Shipment->Courier = "";
            //$args->RequestContainer->Shipment->CourierService = "SEE";
            //$args->RequestContainer->Shipment->CourierAccount = "";
            $args->RequestContainer->Shipment->PackageType = $shipmentType;
            //$args->RequestContainer->Shipment->Value = 0;
            $args->RequestContainer->Shipment->Referring = $reference;

            $args->RequestContainer->Shipment->Items = $items;
            $args->RequestContainer->Shipment->Products = $products;

            $args->RequestContainer->Shipment->Value = $goodsValue;

            $args->RequestContainer->Shipment->ShipmentOrigin = "MBE e-Link WooCommerce 1.1";

            //$args->RequestContainer->Shipment->ProformaInvoice = "";
            //$args->RequestContainer->Shipment->InternalNotes = "";
            $args->RequestContainer->Shipment->Notes = $notes;

            $this->logVar($args, 'CREATE SHIPPING ARGS');


            $soapResult = $soapClient->__soapCall("ShipmentRequest", array($args));


            $lastResponse = $soapClient->__getLastResponse();
            $this->logVar($lastResponse, 'CREATE SHIPPING RESPONSE');

            if (isset($soapResult->RequestContainer->Errors)) {

                $this->logVar($soapResult->RequestContainer->Errors, 'CREATE SHIPPING ERRORS');
            }
            if (isset($soapResult->RequestContainer->Status) && $soapResult->RequestContainer->Status == "OK") {
                if (isset($soapResult->RequestContainer->InternalReferenceID) && $soapResult->RequestContainer->InternalReferenceID == $internalReferenceID) {
                    $result = $soapResult->RequestContainer;
                }
            }

        }
        catch (Exception $e) {
            $this->log('CREATE SHIPPING EXCEPTION');
            $this->log($e->getMessage());
        }
        $this->logVar($result, 'CREATE SHIPPING RESULT');
        return $result;
    }


    public function closeShipping($ws, $username, $password, $system, $trackings)
    {
        $this->log('CLOSE SHIPPING');


        $result = false;


        try {
            $soapClient = new SoapClient($ws, array('encoding' => 'utf-8', 'trace' => 1));
            $internalReferenceID = $this->generateRandomString();

            //WS ARGS
            $args = new stdClass;
            $args->RequestContainer = new stdClass;
            $args->RequestContainer->SystemType = $system;

            $args->RequestContainer->Credentials = new stdClass;
            $args->RequestContainer->Credentials->Username = $username;
            $args->RequestContainer->Credentials->Passphrase = $password;

            $args->RequestContainer->InternalReferenceID = $internalReferenceID;

            $masterTrackingsMBE = array();
            foreach ($trackings as $track) {
                array_push($masterTrackingsMBE, $track);
            }


            $args->RequestContainer->MasterTrackingsMBE = $masterTrackingsMBE;

            $this->logVar($args, 'CLOSE SHIPPING ARGS');


            $soapResult = $soapClient->__soapCall("CloseShipmentsRequest", array($args));

            $lastResponse = $soapClient->__getLastResponse();

            $this->logVar($lastResponse, 'CLOSE SHIPPING RESPONSE');

            if (isset($soapResult->RequestContainer->Errors)) {
                $this->logVar($soapResult->RequestContainer->Errors, 'CLOSE SHIPPING ERRORS');
            }

            if (isset($soapResult->RequestContainer->Status) && $soapResult->RequestContainer->Status == "OK") {
                $result = $soapResult->RequestContainer;
            }

        }
        catch (Exception $e) {
            $this->log('CLOSE SHIPPING EXCEPTION');
            $this->log($e->getMessage());
        }
        $this->logVar($result, 'CLOSE SHIPPING RESULT');
        return $result;
    }
}