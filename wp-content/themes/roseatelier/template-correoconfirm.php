<?php /* Template Name: Correo Confirmación */ ?>
<style>
body {background: #f9f9f9; font-family: Playfair; margin: 20px auto; display: block; max-width: 480px;}
a {text-decoration: none; color: #333; }
h2 {text-align: center; margin-top: 80px; font-weight: 600; font-size: 24px; }
.mensaje { border-top: 1px solid #3A92A6; border-bottom: 1px solid #3A92A6; padding: 40px 0; line-height: 2;  display: block;margin: 0 auto;}
.logoredes {display: inline; height: 15px; width: auto;}
.mensaje p {display: block; margin: 5px auto;}
.mensaje .logo {max-width: 300px; height: auto; width: auto; margin: 0 auto; display: block;}
.fl {float: left;}
.fr {float: right;}
.quote {display: block; margin: 10px auto; padding: 10px; line-height: 2;}

</style>

 <body>
<h2>Voilà!</h2>

<div class="mensaje">
<p>Tú compra <b>No. A018-190</b> en Rosé Atelier®️ está confirmada.
<br>
<br>
	<p><b>Información del pedido:</b><br>
Colección Clásica Blanc<br>
4 rosas grands<br>
Un solo color<br>
	Rosas Champagne (CHA 1)</p>

<b>Fecha de entrega 06 julio 2018</b>
<br><br>
	<p>
		
<b>Datos de envío</b><br>
Jennifer Medina<br>
Montes Urales 445<br>
Piso 5<br>
Lomas de Chapultepec,<br>
Miguel Hidalgo, 11000<br>
Ciudad de México
		
	</p>

<br>



<p>Cuando tu arreglo sea confeccionado y esté en camino, te enviaremos el número de guía con el que podrás rastrear tu regalo.
Si requieres de ayuda adicional o mayor información, no dudes en escribirnos a help@roseatelier.mx</p>
<p>Gracias por elegirnos y alegrarle el día a alguien con un detalle único, inolvidable.</p>


<img class="logo" src="http://roseatelier.mx/wp-content/uploads/2018/05/RoseAtelier-firma-300x113.png" alt="" width="300" height="113" />



</div>

<div class="quote">
<em>“El tiempo es muy lento para los que esperan, muy rápido para los que temen, muy largo para los que sufren, muy corto para los que gozan; pero para quienes aman, el tiempo es eternidad.”</em>
— W. Shakespeare.
</div>
<div class="mensaje">
    <div class="fl">
<a href="http://facebook.com/roseatelier.mx/" target="_blank">Facebook @roseatelier.mx</a></div>
<div class="fr">
<a href="http://instagram.com/_roseatelier" target="_blank">Instagram @roseatelier.mx</a></div>
</div>

</body>
