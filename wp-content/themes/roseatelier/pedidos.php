<?php /* Template Name: Pedidos */ ?>
<!doctype html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">

  <title>Ros&eacute; Atelier | Personaliza tu arreglo</title>
  <meta name="description" content="">
	
<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i" rel="stylesheet">

  <link rel="stylesheet" href="http://roseatelier.jaimeeee.com/assets/css/style.css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.4.6/flatpickr.min.css">

</head>
<body>
<!--
  <header>
    <h1 id="logo"><a href="http://roseatelier.jaimeeee.com"><span>Ros&eacute; Atelier</span></a></h1>

    <nav class="navigation" role="navigation">
  <ul id="menu">
    <li class="menu-item">
      <a href="http://roseatelier.jaimeeee.com/eternal-roses">Eternal Roses</a>
    </li>
    <li class="menu-item is-active">
      <a href="http://roseatelier.jaimeeee.com/colecciones">Personaliza tu arreglo</a>
    </li>
    <li class="menu-item">
      <a href="http://roseatelier.jaimeeee.com/contact">Contacto</a>
    </li>
  </ul>
</nav>

  </header>
-->
<style>
	span {font-family: 'Playfair Display', serif;}
	main.content h2::before {content: ''}
	main.content h4::before {content: ''}
	main.content h2 {font-size: 24pt; font-weight: 400; border-top: 1px solid #333; border-bottom: 1px solid #333; padding: 20px 0; font-family: 'Playfair Display', serif;text-transform: uppercase; letter-spacing: 2px}
	main.content h4 {font-size: 18pt; font-weight: 400; border-top: 1px solid #333; border-bottom: 1px solid #333; padding: 20px 0; font-family: 'Playfair Display', serif;text-transform: uppercase; letter-spacing: 2px}
	#order-form button {
    background: #333;
    border: none;
    color: #fff;
    cursor: pointer;
    float: right;
		font-family: 'Playfair Display', serif;
    font-size: 24pt;
		margin-top: 80px;
		padding: 20px;}
	#collection-colors a{text-align: center;}
	#collection-colors span {display: block;margin: 0 auto;}
	#collection-options .collection-option a {border: none;}
	#collections .collection {width: 25%;
    float: left;
    margin-left: 20px;
    /* margin-left: calc(6.25% - 21.25px + 40px); */
    /* left: calc(6.25% - 21.25px + 20px); */
    position: relative;
    background-size: cover;
    opacity: 0.65;
    overflow: hidden;
    max-height: 120px;}
	
	</style>
  <main class="content" role="main">
    <h2>Personaliza tu arreglo</h2>

    <div class="container">
      <div id="collections">
        <div class="collection" style="background-image: url(http://roseatelier.jaimeeee.com/content/2-colecciones/1-clasica/clasica.jpg);">
          <a data-collection="cl" href="http://roseatelier.jaimeeee.com/colecciones/clasica">Cl&aacute;sica</a>
        </div>
        <div class="collection" style="background-image: url(http://roseatelier.jaimeeee.com/content/2-colecciones/2-nude/nude.jpg);">
          <a data-collection="nd" href="http://roseatelier.jaimeeee.com/colecciones/nude">Nude</a>
        </div>
        <div class="collection" style="background-image: url(http://roseatelier.jaimeeee.com/content/2-colecciones/3-glam/glam.jpg);">
          <a data-collection="gm" href="http://roseatelier.jaimeeee.com/colecciones/glam">Glam</a>
        </div>
      </div>
      <div id="collection-options">
        <h4>Elige el tipo de caja</h4>
        <div id="collection-cl" class="collection-option">
          <div class="box">
            <a data-print="bla" href="#" style="background-image: url(http://roseatelier.mx/wp-content/uploads/2018/05/blanc.jpg); background-size: cover;"></a>
			  <span>Blanc</span>
          </div>
          <div class="box">
            <a data-print="neg" href="#" style="background-image: url(http://roseatelier.mx/wp-content/uploads/2018/05/onix.jpg); background-size: cover;"></a>
			    <span>&Oacute;nix</span>
          </div>
        </div>
        <div id="collection-nd" class="collection-option">
          <div class="box">
            <a data-print="azl" href="#" style="background-image: url(http://roseatelier.mx/wp-content/uploads/2018/05/print-joy.jpg); background-size: cover;"></a>
			  <span>Joy</span>
          </div>
          <div class="box">
            <a data-print="grs" href="#" style="background-image: url(http://roseatelier.mx/wp-content/uploads/2018/05/glum.jpg); background-size: cover;"></a>
			  <span>Gl&uuml;m</span>
          </div>
          <div class="box">
            <a data-print="rsa" href="#" style="background-image: url(http://roseatelier.mx/wp-content/uploads/2018/05/print-bubble.jpg); background-size: cover;"></a>
			  <span>Bubble</span>
          </div>
        </div>
        <div id="collection-gm" class="collection-option">
          <div class="box">
            <a data-print="drd" href="#" style="background-image: url(http://roseatelier.mx/wp-content/uploads/2018/05/foxy.jpg); background-size: cover;"></a>
			  <span>Foxy</span>
          </div>
          <div class="box">
            <a data-print="plt" href="#" style="background-image: url(http://roseatelier.mx/wp-content/uploads/2018/05/polka.jpg); background-size: cover;"></a>
			  <span>Polka</span>
          </div>
          <div class="box">
            <a data-print="glr" href="#" style="background-image: url(http://roseatelier.mx/wp-content/uploads/2018/05/copper.jpg); background-size: cover;"></a>
			  <span>Carisma</span>
          </div>
        </div>
      </div>
      <div id="box-options">
        <h4>Elige el tamaño de tu caja</h4>
        <div id="box-size">
          <div id="box-cl-bla" class="box-sizes">
            <div class="box">
                          <a data-box-volume="X1" href="#"><img src="http://roseatelier.jaimeeee.com/content/sizes/x1/x1.png"></a>
                        </div>
            <div class="box">
                          <a data-box-volume="X4" href="#"><img src="http://roseatelier.jaimeeee.com/content/sizes/x4/x4.png"></a>
                        </div>
            <div class="box">
                          <a data-box-volume="X9" href="#"><img src="http://roseatelier.jaimeeee.com/content/sizes/x9/x9.png"></a>
                        </div>
            <div class="box">
                          <a data-box-volume="X16" href="#"><img src="http://roseatelier.jaimeeee.com/content/sizes/x16/x16.png"></a>
                        </div>
            <div class="box">
                          <a data-box-volume="X25" href="#"><img src="http://roseatelier.jaimeeee.com/content/sizes/x25/x25.png"></a>
                        </div>
            <div class="box">
                          <a data-box-volume="X50" href="#"><img src="http://roseatelier.jaimeeee.com/content/sizes/x50/x50.png"></a>
                        </div>
          </div>
          <div id="box-cl-neg" class="box-sizes">
            <div class="box">
                          <a data-box-volume="X1" href="#"><img src="http://roseatelier.jaimeeee.com/content/sizes/x1/x1.png"></a>
                        </div>
            <div class="box">
                          <a data-box-volume="X4" href="#"><img src="http://roseatelier.jaimeeee.com/content/sizes/x4/x4.png"></a>
                        </div>
            <div class="box">
                          <a data-box-volume="X9" href="#"><img src="http://roseatelier.jaimeeee.com/content/sizes/x9/x9.png"></a>
                        </div>
            <div class="box">
                          <a data-box-volume="X16" href="#"><img src="http://roseatelier.jaimeeee.com/content/sizes/x16/x16.png"></a>
                        </div>
            <div class="box">
                          <a data-box-volume="X25" href="#"><img src="http://roseatelier.jaimeeee.com/content/sizes/x25/x25.png"></a>
                        </div>
          </div>
          <div id="box-nd-azl" class="box-sizes">
            <div class="box">
                          <a data-box-volume="X9" href="#"><img src="http://roseatelier.jaimeeee.com/content/sizes/x9/x9.png"></a>
                        </div>
            <div class="box">
                          <a data-box-volume="X16" href="#"><img src="http://roseatelier.jaimeeee.com/content/sizes/x16/x16.png"></a>
                        </div>
            <div class="box">
                          <a data-box-volume="X25" href="#"><img src="http://roseatelier.jaimeeee.com/content/sizes/x25/x25.png"></a>
                        </div>
          </div>
          <div id="box-nd-grs" class="box-sizes">
            <div class="box">
                          <a data-box-volume="X9" href="#"><img src="http://roseatelier.jaimeeee.com/content/sizes/x9/x9.png"></a>
                        </div>
            <div class="box">
                          <a data-box-volume="X16" href="#"><img src="http://roseatelier.jaimeeee.com/content/sizes/x16/x16.png"></a>
                        </div>
            <div class="box">
                          <a data-box-volume="X25" href="#"><img src="http://roseatelier.jaimeeee.com/content/sizes/x25/x25.png"></a>
                        </div>
          </div>
          <div id="box-nd-rsa" class="box-sizes">
            <div class="box">
                          <a data-box-volume="X9" href="#"><img src="http://roseatelier.jaimeeee.com/content/sizes/x9/x9.png"></a>
                        </div>
            <div class="box">
                          <a data-box-volume="X16" href="#"><img src="http://roseatelier.jaimeeee.com/content/sizes/x16/x16.png"></a>
                        </div>
            <div class="box">
                          <a data-box-volume="X25" href="#"><img src="http://roseatelier.jaimeeee.com/content/sizes/x25/x25.png"></a>
                        </div>
          </div>
          <div id="box-gm-drd" class="box-sizes">
            <div class="box">
                          <a data-box-volume="X25" href="#"><img src="http://roseatelier.jaimeeee.com/content/sizes/x25/x25.png"></a>
                        </div>
            <div class="box">
                          <a data-box-volume="X50" href="#"><img src="http://roseatelier.jaimeeee.com/content/sizes/x50/x50.png"></a>
                        </div>
          </div>
          <div id="box-gm-plt" class="box-sizes">
            <div class="box">
                          <a data-box-volume="X25" href="#"><img src="http://roseatelier.jaimeeee.com/content/sizes/x25/x25.png"></a>
                        </div>
            <div class="box">
                          <a data-box-volume="X50" href="#"><img src="http://roseatelier.jaimeeee.com/content/sizes/x50/x50.png"></a>
                        </div>
          </div>
          <div id="box-gm-glr" class="box-sizes">
            <div class="box">
                          <a data-box-volume="X25" href="#"><img src="http://roseatelier.jaimeeee.com/content/sizes/x25/x25.png"></a>
                        </div>
            <div class="box">
                          <a data-box-volume="X50" href="#"><img src="http://roseatelier.jaimeeee.com/content/sizes/x50/x50.png"></a>
                        </div>
          </div>
        </div>
      </div>
      <div id="collection-colors">
        <h4>Elige el color de tus rosas</h4>
        <div class="color">
          <a href="#" data-identifier="BLU 3">
            <span style="background-color: #001ec2"></span>
            Azul          </a>
        </div>
        <div class="color">
          <a href="#" data-identifier="RED 2">
            <span style="background-color: #f51616"></span>
            Rojo          </a>
        </div>
        <div class="color">
          <a href="#" data-identifier="YEL 2">
            <span style="background-color: #e7c743"></span>
            Amarillo          </a>
        </div>
        <div class="color">
          <a href="#" data-identifier="GRE 2">
            <span style="background-color: #2b4329"></span>
            Verde          </a>
        </div>
        <div class="color">
          <a href="#" data-identifier="VIO 1">
            <span style="background-color: #494895"></span>
            Indigo          </a>
        </div>
        <div class="color">
          <a href="#" data-identifier="ORA 3">
            <span style="background-color: #d5524a"></span>
            Pomelo          </a>
        </div>
        <div class="color">
          <a href="#" data-identifier="PEA 1">
            <span style="background-color: #e3ad80"></span>
            Durazno          </a>
        </div>
        <div class="color">
          <a href="#" data-identifier="BLA 1">
            <span style="background-color: #000000"></span>
            Negra          </a>
        </div>
        <div class="color">
          <a href="#" data-identifier="PUR 2">
            <span style="background-color: #772c84"></span>
            Morado          </a>
        </div>
        <div class="color">
          <a href="#" data-identifier="PIN 4">
            <span style="background-color: #f57b7b"></span>
            Rosa          </a>
        </div>
        <div class="color">
          <a href="#" data-identifier="CHA 1">
            <span style="background-color: #f8eada"></span>
            Champagne          </a>
        </div>
        <div class="color">
          <a href="#" data-identifier="ASO 4">
            <span style="background-color: #d0934b"></span>
            Golden          </a>
        </div>
        <div class="color">
          <a href="#" data-identifier="PIN 7">
            <span style="background-color: #e43d99"></span>
            Mexique          </a>
        </div>
        <div class="color">
          <a href="#" data-identifier="RED 1">
            <span style="background-color: #951515"></span>
            Carm&iacute;n          </a>
        </div>
        <div class="color">
          <a href="#" data-identifier="WHI 4">
            <span style="background-color: #fbfef0"></span>
            Blanco          </a>
        </div>
        <div class="color">
          <a href="#" data-identifier="PIN 2">
            <span style="background-color: #872744"></span>
            Guinda          </a>
        </div>
        <div class="color">
          <a href="#" data-identifier="VIO 3">
            <span style="background-color: #e4bade"></span>
            Lila          </a>
        </div>
      </div>
      <form id="order-form" method="post">
        <input id="order-collection-field" type="hidden" name="collection">
        <input id="order-print-field" type="hidden" name="print">
        <input id="order-volume-field" type="hidden" name="volume">
        <input id="order-colors-field" type="hidden" name="colors">
        <button type="submit" disabled="disabled">&gt; Enviar pedido</button>
      </form>
    </div>
  </main>

<!--
  <footer>
    <hr>
  </footer>
-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.4.6/flatpickr.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.4.6/l10n/es.js"></script>
  <script src="http://roseatelier.jaimeeee.com/assets/js/roseatelier.min.js"></script>

</body>
</html>
