<?php /* Template Name: Nosotros */ ?>
<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<style>
    .margentip {margin-top: 480px !important;}
    
    @media screen and (min-width: 240px) and (max-width: 1024px){
        .margentip {margin-top:320px !important;}
    }
</style>

  <div id="home">
	  
	    <section class="destacada atelier">

	<?php 
	
	$thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');
 	$galeria = get_post_gallery($post->ID, false);
    $attachs = explode(",",$galeria['ids']);

    if (!empty($galeria)) { ?>
    	<ul class="rslides"z id="slider1" style="max-height:420px">
				 <?php
                 foreach($attachs as $idAttach):
                    $imgSlide = wp_get_attachment_url($idAttach);
                    $imgGal = get_post($idAttach); ?>
  					<li><img src="<?php echo $imgSlide; ?>" alt=""></li>
				     <?php endforeach ;?>

</ul>
			
			
			
      
	  	
			  	<?php } else { ?>
			  	
			  	
			  		<div class="thumbportada" style="background-image: url(<?php echo $thumb[0];?>);">	</div>
			  	 
			  <?php } ?>
			
			 </section>
	  
	<section class="tagline margentip">
		
				<h1><?php the_title()?></h1>
			  	<div class="clasica shortdesc">
					<p><?php echo apply_filters("the_content",strip_shortcodes($post->post_content));?></p>
				</div>
			  
	  </section>
	  
	  


		<?php endwhile;endif; ?>



<?php get_footer(); ?>


</div>