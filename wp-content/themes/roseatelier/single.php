<?php get_header();
if (have_posts()) : while (have_posts()) : the_post();
			$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID),false, '' );
			$content = wp_strip_all_tags( get_the_content() );
			$postID = $post->ID;
			$postType = $post->post_type;
			$galeria = get_post_gallery($post->ID, false);
			$url = get_post_meta(get_post_thumbnail_id($post->ID), 'web_url', true);
			$attachs = explode(",",$galeria['ids']);
			$time = get_the_time('U');
			$melorama = $post->post_type;
			$single_mood = get_the_term_list($post->ID,'mood','','','');
		 	$piedefotoPortada = get_post_thumbnail_id($catre->ID);
			$youtube = get_post_meta(get_post_thumbnail_id($post->ID), 'youtube_link_img', true);


			?>
<div class="single_content">
					<?php	if ( has_post_thumbnail()): ?>
														<?php if ( has_post_format('gallery')):?>

											<div class="track-single galeria">
														<div id='mySwipe' class='swipe'>
																<div class='swipe-wrap'>
																		<?php foreach($attachs as $idAttach):
																				$imgSlide = wp_get_attachment_url($idAttach);
																				$url = get_post_meta(get_post_thumbnail_id($idAttach->ID), 'web_url', true);
																				?>
																				<div id="<?php echo $track->post_name;?>">
																								<div class="card" style="background-image:url(<?php echo $imgSlide; ?>);">
																								</div>
																								<div class="caption_tracks">
																										<span>Imagen:<a href="<?php echo $url;?>" target="_blank"> <?php echo get_post($idAttach)->post_title;?></a></span>
																								</div>
																				</div>
									                   <?php endforeach ;?>
														 </div>
														 <div class="botones">
																<a onclick='mySwipe.prev()'><div class="left"> << </div></a>
																<a onclick='mySwipe.next()'><div class="right"> >> </div></a>
													 </div>
												  </div>

															<div class="cuerpotexto">
									                <h1><?php the_title(); ?></h1>
																	<div class="metadatos">
																			<p><?php echo  'publicado hace ' . esc_html( human_time_diff( get_the_time('U'), current_time('timestamp') ) ) . ' por <a href="' . get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ) . '">'; ?>
																					<?php the_author(); ?></a></p>
																			</div>

									                    <article>
																				<?php echo apply_filters("the_content",strip_shortcodes($post->post_content)); ?>
																				<div class="single_pagination">
																							<ul>
																									<?php previous_post_link('<li class="fl pagright">%link</li>'); ?>&laquo;
																									<?php next_post_link('<li class="fr pagleft">%link</li>'); ?> &raquo;
																							</ul>
																					</div>
																			</article>
																		</div>
														</div>
															<?php elseif(has_post_format('video')): ?>

																		 <div class="track-single formatovideo">
																			 <iframe src="https://www.youtube.com/embed/<?php parse_str( parse_url( $youtube, PHP_URL_QUERY ), $iframeYT );
			 													echo $iframeYT['v'] . '?controls=0&amp;showinfo=0';?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

																												                 <h1><?php the_title(); ?></h1>
											 								<div class="metadatos">
																				<p><?php echo  'publicado hace ' . esc_html( human_time_diff( get_the_time('U'), current_time('timestamp') ) ) . ' por <a href="' . get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ) . '">'; ?>
																						<?php the_author(); ?></a></p>
											 								</div>

											                     <article>
											 											<?php echo apply_filters("the_content", $content); ?>
																						<div class="single_pagination">
																									<ul>
																											<?php previous_post_link('<li class="fl pagright">%link</li>'); ?>&laquo;
																											<?php next_post_link('<li class="fr pagleft">%link</li>'); ?> &raquo;
																									</ul>
																							</div>
											 										</article>

																					<div class="feat_image" style="background-image:url(<?php echo $src[0]; ?>);">
																						<div class="caption_tracks">
																								<span>Imagen:<b> <a href="<?php echo $url;?>" target="_blank"><?php echo get_post($piedefotoPortada)->post_title;?></b></a></span>
																						</div>
																					</div>



        														</div>

																		<?php else: ?>
																			<div class="feat_image" style="background-size: cover; background-image:url(<?php echo $src[0]; ?>);">
																		 	 <div class="caption_tracks">
																		 			 <span>Imagen:<b> <a href="<?php echo $url;?>" target="_blank"><?php echo get_post($piedefotoPortada)->post_title;?></b></a></span>
																		 	 </div>
																		  </div>
																			<div class="track-single">
																				<h1><?php the_title(); ?></h1>
																				<div class="metadatos">
																						<p><?php echo  'publicado hace ' . esc_html( human_time_diff( get_the_time('U'), current_time('timestamp') ) ) . ' por <a href="' . get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ) . '">'; ?>
																								<?php the_author(); ?></a></p>
																						</div>

																						<article>
																							<?php echo apply_filters("the_content", $content) ?>
																							<div class="single_pagination">
																										<ul>
																												<?php previous_post_link('<li class="fl pagright">%link</li>'); ?>&laquo;
																												<?php next_post_link('<li class="fr pagleft">%link</li>'); ?> &raquo;
																										</ul>
																								</div>
																						</article>
																		</div>
														 <?php endif;?>
					<?php endif;?>



									<?php endwhile; else: ?>
                  <p><?php _e('Lo siento, no encontramos ninguna nota relacionada );','themnific');?>.</p>
                <?php endif; ?>


						<?php include('includes/homeblocks/share.php'); ?>

		 <?php
		 		if($melorama = 'melorama'):
		 include('includes/homeblocks/related.php');
	 else:
		  include('includes/homeblocks/related-melorama.php');
endif;
		  ?>
</div>
<?php get_footer(); ?>
