<?php /* Template Name: Galeria */ ?>
<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <div id="home">
      
  
	    <section class="galeria">
			
			 <div id='mySwipe' class='swipe'>
         		<div class='swipe-wrap'>
            
							<?php 
								$portada = get_page_by_path("colecciones/clasica");
								$titulo = $portada->post_title;
								$content = $portada->post_content;
								$thumbID = get_post_thumbnail_id($portada->ID);
								$thumb = wp_get_attachment_image_src(get_post_thumbnail_id($portada->ID),'full');
						  ?>
					
								<div>
									<a href="<?php echo get_the_permalink($portada->ID);?>">
                             		 <div class="thumbportada fl" style="background-image: url(<?php echo $thumb[0];?>);"></div>
									
									<div class="shortdesc fr">
										<h2><?php echo $titulo;?></h2>
									
										<article><?php echo strip_shortcodes($portada->post_content);?></article>
										
										 	<span style="font-size: 14px; font-weight: 400; display:block; margin-top: 20px; "><?php echo 'Foto: ' . get_post($thumbID)->post_excerpt;?></span>
										
									 </div>
									</a>
								</div>
				
					
							<?php 
								$portada = get_page_by_path("colecciones/nude");
								$titulo = $portada->post_title;
								$content = $portada->post_content;
									$thumbID = get_post_thumbnail_id($portada->ID);
								$thumb = wp_get_attachment_image_src(get_post_thumbnail_id($portada->ID),'full');
								?>
					
							<div>
								<a href="<?php echo get_the_permalink($portada->ID);?>">
								<div class="thumbportada fl" style="background-image: url(<?php echo $thumb[0];?>);"></div>
										<div class="shortdesc fr">
										<h2><?php echo $titulo;?></h2>
										<article><?php echo strip_shortcodes($portada->post_content);?></article>
											<span style="font-size: 14px; font-weight: 400; display:block; margin-top: 20px; "><?php echo 'Foto: ' . get_post($thumbID)->post_excerpt;?></span>
									 </div>
									</a>
							</div>
				
				
							<?php 
								$portada = get_page_by_path("colecciones/glam");
								$titulo = $portada->post_title;
								$content = $portada->post_content;
									$thumbID = get_post_thumbnail_id($portada->ID);
								$thumb = wp_get_attachment_image_src(get_post_thumbnail_id($portada->ID),'full');
						  ?>

							<div>
									<div class="thumbportada fl" style="background-image: url(<?php echo $thumb[0];?>);"></div>
							
									<a href="<?php echo get_the_permalink($portada->ID);?>">
										<div class="shortdesc fr">
										<h2><?php echo $titulo;?></h2>
										<article><?php echo strip_shortcodes($portada->post_content);?></article>
											<span style="font-size: 14px; font-weight: 400; display:block; margin-top: 20px; "><?php echo 'Foto: ' . get_post($thumbID)->post_excerpt;?></span>
									 </div>
									</a>
								</div>
					</div>
     </div>
            <div class="botones">
               <a onclick='mySwipe.prev()'><div class="left"> << </div></a>
               <a onclick='mySwipe.next()'><div class="right"> >> </div></a>
       	    </div>
                
			  
	  </section>

		<?php endwhile;endif; ?>




<?php get_footer(); ?>
</div>
