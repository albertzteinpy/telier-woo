<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]--><head><meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

<title>Rosé Atelier | <?php global $post; echo $post->post_title;?>
</title>

<!-- Set the viewport width to device width for mobile -->
<meta name="google-site-verification" content="3twPtmuNb8BWNSZIU4ofmlC7S8BHk-86bELyVo_BOx4" />
<meta name="viewport" content="minimum-scale=1.0; maximum-scale=1.0; user-scalable=false; initial-scale=1.0;"/>
<meta name="google-site-verification" content="7AfeBnR0rNqfI1BKcMUzBY-K70sb1qm5eHL0gyhZWak" />
  <meta name="description" content="Un regalo &uacute;nico. Nuestras rosas eternas alegrar&aacute;n tus d&iacute;as durante mucho tiempo." />
  <meta name="keywords" content="Rosas eternas Mexico, Rose Atelier, rosa telier, Rose, Atelier, Rosas, Rosas eternas, rosas ecuatorinas, envio de regalos, regalo aniverario, dia de las madres, mejores regalos, ideas originales para regalar, ciudad de m&eacute;xico, cdmx, distrito federal, df, regalos mam&aacute;, regalos mama, regalos originales, unico, rosas preservadas, rosa ecuador, ecuador, ramo de flores, envia flores, enviar flores, envios" />
  <meta property="og:title" content="Rosé Atelier | <?php echo $page->title;?>" />
  <meta property="og:type" content="article" />
  <meta property="og:url" content="<?php echo get_permalink($post->ID);?>" />
  <meta property="og:description" content="Un regalo &uacute;nico. Nuestras rosas eternas alegrarán tus días durante mucho tiempo." />
  <meta property="og:image" content="<?php echo $thumb[0] ;?>" />
  <meta property="og:image:height" content="405">
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="<?php echo bloginfo('title');?>">
  <meta name="twitter:title" content="<?php echo $post->post_title;?>"> <meta property="og:image:height" content="405">
  <meta name="twitter:creator" content="<?php echo  bloginfo('title');?>">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />	
<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" media="screen" />
<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i" rel="stylesheet">
<link href="<?php echo get_template_directory_uri() . '/css/mediaqueries.css';?>" rel="stylesheet" type="text/css" />
<link href="<?php echo get_template_directory_uri() . '/css/font-awesome.min.css';?>" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="//newsharecounts.s3-us-west-2.amaz	onaws.com/nsc.js"></script>
<script type="text/javascript">window.newShareCountsAuto="smart";</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri() . '/js/responsiveslides.min.js';?>"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119745935-1"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>

  <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-119745935-1');

	   $(function () { $("#slider1").responsiveSlides({maxwidth: 800,speed: 1200}); });
	
	<!-- Google Tag Manager -->
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MG2JMTC');
	</script>
<!-- End Google Tag Manager -->


<?php wp_head(); ?>
</head>

<body>
  

	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MG2JMTC"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<header>
	<div class="logo"><a href="<?php echo get_home_url();?>"><img src="<?php echo get_option('logo');?>"></a></div>
		<a id="menu"><span class="fa fa-bars"></span></span></a>
	   
    <?php
      include('includes/menu/movil.php');
      include('includes/menu/principal.php');
      ?>

</header>
<div style="clear: both;"></div>

<style>
   section.galeriaclasica {display: block; position: relative; margin: 0 auto; min-height: 540px;}
   
@media screen and (min-width: 1334px) and (max-width: 1555px) {
    #mySwipe {width:75%;}
     section.galeriaclasica {width: 80%;}
    .botones {left: 12%; max-width: 400px;}
    section.tagline {margin-top: 640px;}
}
@media screen and (min-width: 1555px) and (max-width: 1738px){
    
    section.homepage ul.rslides  {width: 90%;}
    section.homepage .rslides li {width: 100%;}
    section.homepage .rslides img {}
    section.tagline{margin-top: 640px;}
     section.galeriaclasica {width: 70%;}
    #mySwipe {width:60%;}
    .botones {left: 20%; width: 360px;}
    

}
  
  @media screen and (min-width: 1739px) {

    section.galeriaclasica {width: 60%;}
    #mySwipe {width:85%;}
    
    section.tagline{margin-top: 640px;}
    .botones {left: 10%;}
    

}

@media screen and (min-width: 1160px) and (max-width: 1333px) {
    #mySwipe {width:75%; float: left; left: 80px; max-height: 480px;}
    section.galeriaclasica  {width: 97%;}
    section.galeriaclasica .rslides { width: 320px; top: 80px; margin: 20px 20px; display: inline-block;}
    section.galeriaclasica .rslides li {min-height: 320px;}
    section.galeriaclasica .rslides img {width: 320px;height: 320px;}
    section.galeriaclasica .shortdesc {width: 320px;}
    .botones {left: 80px; width: 320px; bottom: 0;}
    section.galeria {min-height: 480px !important;}
    section.galeria .thumbportada {width: 320px; height: 320px;min-width: 320px;min-height: 320px;}
    section.tagline{margin-top: 540px;}
    section.homepage .rslides {width: 70%;}
    section.homepage .rslides img {}
 

}

@media screen and (min-width: 1026px) and (max-width: 1160px) {
    #mySwipe {width:65%; }
    section.galeriaclasica {width: 60%; }
    section.galeriaclasica .rslides {top: 0; margin: 40px 20px 0; display: inline-block;}
    section.galeriaclasica .shortdesc h2 {margin-top: 10px;}


}
@media screen and (max-width: 1024px) and (min-width: 240px) {
    section.tagline {margin-top: 250px; padding-top: 0;}
}

@media screen and (max-width: 320px) and (max-height: 568px){
    section.galeria {min-height: 480px;}
    section.galeria .shortdesc {width: calc(100% - 40px);}
    #mySwipe {max-height: 540px}
    section.rosas ul li {min-height: 140px;}
    section.rosas .imagenrosa {min-height: 60px;}
    section.rosas span.pedidos {margin: 10px auto 40px;}
    section.galeriaclasica {width: 90% !important; margin-top: 20px;}
    .framepersonalizacion iframe {height: calc(100% - 60px); position: absolute; top: 80px;}
}



    section.galeria .thumbportada {left: 0;}
</style>