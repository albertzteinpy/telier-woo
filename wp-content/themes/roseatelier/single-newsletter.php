<?php
global $post;
$titulo = $post->post_title;
$newsletter = $post->post_type;
$conteo = get_post_meta($post->ID, 'idPeli', true);
$logo = get_option ('logo');
$subtitulo = get_post_meta($post->ID, 'subtitulo', true);
$contenido = apply_filters("the_content",strip_shortcodes($post->post_content));
$irA = get_post_meta($post->ID, 'irA', true);
$textoBoton = get_post_meta($post->ID, 'textoBoton', true);
$textosecundario = get_post_meta($post->ID, 'segundoBloque', true);
$segundoBoton = get_post_meta($post->ID, 'segundoBoton', true);
$textoSBoton = get_post_meta($post->ID, 'textoSBoton', true);
$thumb = wp_get_attachment_url( get_post_thumbnail_id($post->ID));
$galeria =  get_post_gallery_images($post->ID, false);
$attachs = explode(",",$galeria['ids']); ?>

<html>
<head>
<title><?php echo $titulo . ' | ' . $newsletter . ' #'. $conteo;?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/styles/newsletter2.css">

</head>
<body>
	
<style>

.fa {
  cursor: pointer;
  background-position: bottom center;
  background-repeat:no-repeat;
  background-size: cover;
  height: 20px;
  width: 20px;
  margin: 0 10px;
  display: inline-block;text-align: center;

}

.fa-facebook {margin-bottom: 2px; background-image: url('<?php echo get_template_directory_uri(); ?>/img/social/facebook.png');)}
.fa-twitter { background-image: url('<?php echo get_template_directory_uri(); ?>/img/social/twitter.png');}
.fa-instagram { background-image: url('<?php echo get_template_directory_uri(); ?>/img/social/instagram.png');}
.fa-youtube { background-image: url('<?php echo get_template_directory_uri(); ?>/img/social/youtube.png');}
	
@media screen and (max-width: 786px){

  .datos {width: 90%; display: block; margin: 0 auto; float: inherit;}
  .datos .cuadroder {width: 40%; float: left;}
	.datos .cuadroizq {width: 40%; float: right;}
  .botonprincipal {background-size: 90%; width: 60%!important}
  .contenido {max-width: 1080px; width: 100%;}
  .botonprincipal span {font-size: 18pt;padding-top: 20px}
  .encabezado {width: 80%; padding: 20px; }
	  .datos h2, .datos h3 {font-size: 10pt;}
	.infocontacto .cuadroder {text-align: left; padding-left: 20px}
	.botonprincipal span {font-size: 12pt; padding-top: 24px}
  .imagengrande {background-size: 250%;min-height: 480px; }
  .redes { text-align: center; display:block; float: inherit;width: 100%;}
   span.website {position: relative; font-size: 10pt; text-align: center;display: block; margin: 20px auto 0}

  .infocontacto {width: 100%; text-align: center;float: inherit; margin-top: 20px}
  .infocontacto .cuaderder {width: 50%; float: inherit;text-align: left;}
  .infocontacto .cuadroizq {text-align: right;width: 45%;}
	
}
	
	@media screen and (min-width: 787px){
			.botonprincipal {background-size: 90%; width: 60%;}
			.datos .cuadroder {width: 25%; display: inline-block; float: left;}
			.datos .cuadroizq {width: 25%; display: inline-block; float: right; margin-left: 10px}
		 .redes { text-align: center; display: inline-block; float: right;width: auto;}
		.imagengrande {min-height: 640px; }
span.website {margin-left: 3em; position: relative; font-size: 10pt; text-align: center;display: inline-block; bottom: 40px}
		
	}

@media screen and (max-width: 520px){
  .datos .logo {max-height: 120px; width: 100%; margin-bottom: 20px}
  .datos .logo img {max-height: 120px; max-width:100%; width: auto; float: inherit; }
  .datos h2, .datos h3 {font-size: 10pt;}
  .datos .cuadroder {width: 40%}
  .imagengrande {background-size: 350%; min-height: 480px;}
	.encabezado h1 {font-size: 16pt;}
	.encabezado p {font-size: 10pt}
  .botonprincipal {background-size: 90%;width: 100%;}
  .botonprincipal span {font-size: 10pt;padding-top: 28px}
  .infocontacto .cuadroder {width: 40%;text-align: left; padding-left: 20px }
  .infocontacto .cuadroizq {width: 40%;}
}
	
	@media screen and (max-width: 320px){
		.botonprincipal {background-size: 90%;}
		.imagengrande {background-size: 350%; min-height: 240px}
		.datos .cuadroder, .datos .cuadroizq {display: block; text-align: center; width: 100%;}
		.datos h2, .datos h3 {text-align: center;}
		.infocontacto {margin: 0}
		.infocontacto .cuadroder {width: 40%;text-align: left; padding-left: 20px }
  		.infocontacto .cuadroizq {width: 40%;}
		.datos h2, .datos h3 {font-size: 10pt;}
		.botonprincipal span {font-size:10pt; padding-top: 28px;}
		.infocontacto .cuadroder, .infocontacto .cuadroizq {width: 90%; display: block; float: inherit; text-align: center; position: relative; margin: 10px auto; padding: 0; font-size: 10pt; line-height: 2}
		.piedepag {padding: 0; margin: 0}
		.infocontacto a {font-size: 10pt}
		.redes {width: 100%; margin: 0 auto; display: block; text-align: center; float: inherit}
	}

</style>
    <div class="contenido">
      <header>
      <div class="datos">
          <div class="logo">
             <a href="http://premiosfenix.com/" target="_blank">
             <img src="http://premiosfenix.com/wp-content/uploads/2018/02/fenix_logo_negro.png" />
           </a>
            </div>
            <div class="cuadroder">
            <!---  <h4>Newsletter no. <?php echo $conteo;?></h4> -->
              <h3>Organizado por <a style="color: #000;" href="http://cinema23.com/" target="_blank"><strong>Cinema23</strong></a></h3>
      			</div>

  				  <div class="cuadroizq">
                <h2>_<?php the_time('F.y');?></h2>
          </div>
  	</div>

      </header>



<?php if (!empty($galeria)){?>

    <a href="<?php echo $irA;?>" target="_blank" style="color: #000;">
      <div class="imagengrande"style="background-image: url(<?php echo $galeria[0];?>); max-height: 320px;"> </div>
          <div class="botonprincipal">
            <span><?php echo $textoBoton;?></span>
          </div>
    </a>

    <div class="encabezado">
      <h1><?php echo $subtitulo;?></h1>
      <p><?php echo $contenido;?></p>
    </div>

<a style="color: #000;" href="<?php echo $segundoBoton;?>" target="_blank">
  <div class="segundanota">
    <div class="segundaimagen" style="background-image: url(<?php echo $galeria[1];?>); max-height: 240px;">
        </div>
    <div style="width: 30%; height: 240px; display: inline-block; float: right; position: absolute; background: #fff; right:0; padding: 10px; font-size: 12pt">
      <p><?php echo $textosecundario;?></p>
      <div class="botonsecundario">
        <span><?php echo $textoSBoton;?></span>
      </div>
    </div>

</div>

</a>

<?php } else { ?>

  <a style="color: #000;" href="<?php echo $irA;?>" target="_blank">
    <div class="imagengrande"style="background-image: url(<?php echo $thumb;?>)"> </div>
    <div class="encabezado">
      <h1><?php echo $subtitulo;?></h1>
      <p><?php echo $contenido;?></p>
    </div>

        <div class="botonprincipal" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/botones/negativo.gif'); ">
          <span><?php echo $textoBoton;?></span>
        </div>
  </a>



<?php } ?>

<div class="piepag">
    <div class="redes">
    			<a href="http://facebook.com/<?php echo get_option ('facebook'); ?>" target="_blank"><span class="fa fa-facebook"></span></a>
    			<a href="http://twitter.com/<?php echo get_option ('twitter');?>"  target="_blank"><span class="fa fa-twitter"></span></a>
    			<a href="http://instagram.com/<?php echo get_option ('instagram');?>"  target="_blank"><span class="fa fa-instagram"></span></a>
    			<a href="http://youtube.com/user/<?php echo get_option ('youtube');?>" target="_blank"><span class="fa fa-youtube"></span></a>
    </div>
    <a style="color: #000;" href="http://premiosfenix.com/" target="_blank"><span class="website">premiosfenix.com</span></a>


    <div class="infocontacto">
        <div class="cuadroder">
              <a href="<?php echo get_option ('correo');?>"><?php echo get_option ('correo');?></a>
              <p>	T. <?php echo get_option ('telefono');?><br>
        </div>

        <div class="cuadroizq">
          <p><?php echo get_option ('domicilio');?></p>
          </div>
    </div>

  </div>

  </div>


</body>
	</html>
