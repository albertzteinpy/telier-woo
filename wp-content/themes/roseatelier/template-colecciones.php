<?php /* Template Name: Colecciones */ ?>
<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>


  <div id="home">
	  
	    <section class="galeriaclasica">

	<?php 
	
	$thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');
 	$galeria = get_post_gallery($post->ID, false);
    $attachs = explode(",",$galeria['ids']);
    $thumbID = get_post_thumbnail_id($post->ID);

    if (!empty($galeria)) { ?>
    	<ul class="rslides fl" z id="slider1">
				 <?php
                 foreach($attachs as $idAttach):
                    $imgSlide = wp_get_attachment_url($idAttach);
                    $imgGal = get_post($idAttach); ?>
  					<li style="background:#fff"><img src="<?php echo $imgSlide; ?>" alt="">
  					    	
  					</li>
				     <?php endforeach ;?>
</ul>
		  	<?php } else { ?>
			  		<div class="thumbportada" style="background-image: url(<?php echo $thumb[0];?>);">
			  		</div>
			  	 
			  <?php } ?>
				
			  	<div class="shortdesc fr">
					<h2 style="margin: 80px auto 10px;"><?php the_title()?></h2>
					 <?php if (is_page('colecciones/clasica')): ?>
					 <span style="font-size: 14px; font-weight: 400; display:block; margin-bottom: 20px; margin-top:0 ">(Precios desde $723)</span>
				  <?php endif;?>
					 
					<?php if (is_page('colecciones/nude')): ?>
					<span style="font-size: 14px; font-weight: 400; display:block; margin-bottom: 20px; margin-top:0 ">(Precios desde $2,553)</span>
				  <?php endif;?>
				   <?php if (is_page('colecciones/glam')): ?>
				  	<span style="font-size: 14px; font-weight: 400; display:block; margin-bottom: 20px; margin-top:0 ">(Precios desde $7,633)</span>
				  	<?php endif;?>
				<article><?php echo apply_filters("the_content",strip_shortcodes($post->post_content));?>
					  <br>
				  <?php if (is_page('colecciones/clasica')): ?>
			
				  <div class="box" data-print="bla">
                    <a  class="image" style="background-image: url(http://roseatelier.jaimeeee.com/content/2-colecciones/1-clasica/1-blanco/blanc.jpg)"></a>
                       Blanc
                    </div>
                  <div class="box" data-print="neg">
                      <a class="image" style="background-image: url(http://roseatelier.jaimeeee.com/content/2-colecciones/1-clasica/2-negro/onix.jpg)"></a>
                    Ónix
                  </div>
				  
				  <?php endif;?>
				   <?php if (is_page('colecciones/nude')): ?>
				   <div class="box" data-print="azl">
             <a class="image" style="background-image: url(http://roseatelier.mx/wp-content/uploads/2018/06/caja-joy.jpg)"></a>
            joy.
          </div>
          <div class="box" data-print="grs">
            <a  class="image" style="background-image: url(http://roseatelier.mx/wp-content/uploads/2018/06/caja-glum.jpg)"></a>
          glüm.
          </div>
          <div class="box" data-print="rsa">
            <a  class="image" style="background-image: url(http://roseatelier.mx/wp-content/uploads/2018/06/caja-bubble.jpg)"></a>
         bubble.
          </div>
				    <?php endif;?>
				    <?php if (is_page('colecciones/glam')): ?>
				    <div class="box" data-print="drd">
             <a class="image" style="background-image: url(http://roseatelier.jaimeeee.com/content/2-colecciones/3-glam/1-foxy/foxy.jpg)"></a>
             Foxy
          </div>
          <div class="box" data-print="plt">
            <a  class="image" style="background-image: url(http://roseatelier.mx/wp-content/uploads/2018/06/caja-polka.jpg)"></a>
              Polka
          </div>
          <div class="box" data-print="glr">
            <a class="image" style="background-image: url(http://roseatelier.jaimeeee.com/content/2-colecciones/3-glam/3-carisma/copper.jpg)"></a>
             Carisma
          </div>
				 <?php endif;?>
				</article>
					
				
			   <a href="/colecciones"> <span class="otrascolecciones">Ver otras colecciones</span></a>
					
			  </div>
		
			  
			 
			  
			  
	  </section>
	  
	  


		<?php endwhile;endif; ?>


<?php get_footer(); ?>

</div>

