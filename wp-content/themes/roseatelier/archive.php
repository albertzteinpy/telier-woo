<?php get_header(); ?>

<div class="sect">
<div id="acervo">
	<ul>
       	<?php while (have_posts()) : the_post();
        				$large_image =  wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'fullsize', false, '' );
                $large_image = $large_image[0];
                $piedefotoPortada = get_post_thumbnail_id($post->ID);
                $url = get_post_meta(get_post_thumbnail_id($post->ID), 'web_url', true);
            ?>

            <li id="post-<?php the_ID(); ?>" >

                  <div class="galeria" style="background-image: url(<?php echo $large_image;?>);">
                      <div class="caption_tracks">
                              <span>Imagen: <a href="<?php echo $url;?>" target="_blank"><?php echo get_post($piedefotoPortada)->post_title;?></a></span>

                              <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

                      </div>
                  </div>
                  <?php the_content(); ?>

            </li><!-- #post-<?php the_ID(); ?> -->

		<?php endwhile; ?>
   </ul>

        <div class="clear"></div>
		<div class="pagination"><?php pagination('«', '»'); ?></div>
	<?php wp_reset_query(); ?>

</div>
</div>


<?php get_footer(); ?>
