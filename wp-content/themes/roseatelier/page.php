<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <div id="home" style="min-height: 1080px">
      
      <style>
          
          .regularpag {width: calc(100% - 360px); margin: 0 auto; display: block;}
          
          	@media screen and (min-width: 240px) and (max-width: 1040px){

 .regularpag {width: 90%; }
	
}
      </style>
	   
	  
	   <section class="regularpag">
	        	<h2 style="text-align: center; margin-top: 80px; font-weight: 400"><?php the_title();?></h2>
			
					<p style="border-top:1px solid #3A92A6; border-bottom: 1px solid #3A92A6; padding: 40px 0;line-height:2"><?php echo strip_shortcodes($post->post_content);?></p>
		
</section>


	<?php endwhile;endif; ?>

</div>




<?php get_footer(); ?>