<?php


add_action("login_head", "my_login_head");
function my_login_head() {
  echo "
  <style>
  body.login #login h1 a {
    background: url('http://roseatelier.mx/wp-content/uploads/2018/05/roseatelier_color.png') no-repeat scroll center top transparent ;
		background-size: 100%;
		height: 240px;
    width: 320px;
  }
  </style>
  "; }

	add_filter( 'login_headerurl', 'custom_loginlogo_url' );
function custom_loginlogo_url($url) {
    return 'http://www.roseatelier.mx';
}
if ( ! isset( $content_width ) )

	$content_width = 787;




//Add Custom Post Types
include ('functions/post_types.php');
include ('functions/taxonomies.php');
include ('functions/custom-fields.php');


/* Get shares on FB */

function wp_get_shares( $post_id ) {
	$cache_key = 'misha_share' . $post_id;
	$access_token = '666987483466439|b3e1a0948513223b0bc51b32a735e2cf';
	$count = get_transient( $cache_key ); // try to get value from Wordpress cache

	// if no value in the cache
	if ( $count === false ) {
		$response = wp_remote_get('https://graph.facebook.com/v2.7/?id=' . urlencode( get_permalink( $post_id ) ) . '&access_token=' . $access_token );
		$body = json_decode( $response['body'] );
		//print_r($body);

		$count = intval( $body->share->share_count );
		set_transient( $cache_key, $count, 3600 ); // store value in cache for a 1 hour
    update_post_meta( $post_id, 'facebook_share_count', $count );
	}
	return $count;
}


/* Get shares on TW */

function wp_get_tweets ($post_id) {
  $url = get_permalink( $post_id );
  $check_url = 'http://public.newsharecounts.com/count.json?url=' . urlencode( $url );
  $response = wp_remote_retrieve_body( wp_remote_get( $check_url ) );
  $encoded_response = json_decode( $response, true );
  $share_count = intval( $encoded_response['count'] );
  return $share_count;

	}


function redesSocialesIn( $form_fields, $post ) {
  $form_fields['youtube_link_img'] = array(
         'label' => 'Youtube',
         'input' => 'text',
         'value' => get_post_meta( $post->ID, 'youtube_link_img', true ),
     );

      $form_fields['vimeo_link_vid'] = array(
         'label' => 'Vimeo URL',
         'input' => 'text',
         'value' => get_post_meta( $post->ID, 'vimeo_link_vid', true ),
     );

 		$form_fields['web_url'] = array(
 			 'label' => 'URL del sitio',
 			 'input' => 'text',
 			 'value' => get_post_meta( $post->ID, 'web_url', true ),
 	 );

     return $form_fields;

 }

 add_filter( 'attachment_fields_to_edit','redesSocialesIn', 10, 2 );
 	/**/

 function guardaRedesAttach( $post, $attachment ) {
 var_dump($post);

      if( isset( $attachment['youtube_link_img'] ) ){

 update_post_meta( $post['ID'], 'youtube_link_img', esc_url( $attachment['youtube_link_img'] ) );
     }
      if( isset( $attachment['vimeo_link_vid'] ) ){

 update_post_meta( $post['ID'], 'vimeo_link_vid', esc_url( $attachment['vimeo_link_vid'] ) );
     }
 		if( isset( $attachment['web_url'] ) ){

 update_post_meta( $post['ID'], 'web_url', esc_url( $attachment['web_url'] ) );
 	 }


     return $post;

 }

 add_filter( 'attachment_fields_to_save', 'guardaRedesAttach', 10, 2 );






/*-----------------------------------------------------------------------------------

- Loads all the .php files found in /admin/widgets/ directory

----------------------------------------------------------------------------------- */




	$preview_template = _preview_theme_template_filter();



	if(!empty($preview_template)){

		$widgets_dir = WP_CONTENT_DIR . "/themes/".$preview_template."/functions/widgets/";

	} else {

    	$widgets_dir = WP_CONTENT_DIR . "/themes/".get_option('template')."/functions/widgets/";

    }



    if (@is_dir($widgets_dir)) {

		$widgets_dh = opendir($widgets_dir);

		while (($widgets_file = readdir($widgets_dh)) !== false) {
			if(strpos($widgets_file,'.php') && $widgets_file != "widget-blank.php") {
				include_once($widgets_dir . $widgets_file);
			}
		}

		closedir($widgets_dh);

	}

// Post thumbnail support

if (function_exists('add_theme_support')) {

	add_theme_support('post-thumbnails');

	set_post_thumbnail_size(640, 265, true); // Normal post thumbnails

	add_image_size('fbop', 1200, 650, true ); //(cropped)
	add_image_size('format-gallery', 622, 400, true);
	add_image_size('format-image', 622, 9999);
	add_image_size('format-single', 622, 220, true); //(cropped)
}


function thumb_url(){
$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 2100,2100 ));
return $src[0];
}

// Theme Support Functions

add_editor_style();
add_theme_support( 'post-formats', array( 'video','audio', 'gallery', 'image', 'quote', 'link' ) );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'custom-background' );



/* Canales - [canales] */

add_shortcode('canales', 'shortcode_canales');
	function shortcode_canales($attr, $content) {
    ob_start();
    get_template_part('/includes/canales');
    $ret = ob_get_contents();
    ob_end_clean();
  return $ret;
}





/* HostingMusic. - [hostingmusic] */

add_shortcode('hostingmusic', 'tmnf_shortcode_hostingmusic');
	function tmnf_shortcode_hostingmusic($attr, $content) {
    ob_start();
    get_template_part('/includes/homeblocks/hostingmusic');
    $ret = ob_get_contents();
    ob_end_clean();
  return $ret;

}


/*  Video resaltado - [video_resaltado] */

add_shortcode('video_resaltado', 'tmnf_shortcode_video_resaltado');
function tmnf_shortcode_video_resaltado($attr, $content) {

    ob_start();
    get_template_part('/includes/homeblocks/video_resaltado');
    $ret = ob_get_contents();
    ob_end_clean();
    return $ret;

}


/* Melorama - [melorama] */


add_shortcode('melorama', 'tmnf_shortcode_melorama');
	function tmnf_shortcode_melorama($attr, $content) {
    ob_start();
    get_template_part('/includes/homeblocks/melorama');
    $ret = ob_get_contents();
    ob_end_clean();
  return $ret;

}


/* Agenda - [eventos] */

add_shortcode('eventos', 'tmnf_shortcode_eventos');
	function tmnf_shortcode_eventos($attr, $content) {
    ob_start();
    get_template_part('/includes/homeblocks/agenda');
    $ret = ob_get_contents();
    ob_end_clean();
  return $ret;

}

/* Laboratorio - [lab_slider] */



add_shortcode('lab_slider', 'tmnf_shortcode_lab_slider');

function tmnf_shortcode_lab_slider($attr, $content)

{

    ob_start();

    get_template_part('/includes/homeblocks/laboratorio');

    $ret = ob_get_contents();

    ob_end_clean();

    return $ret;

}



// Use shortcodes in text widgets.

add_filter('widget_text', 'do_shortcode');

// navigation menu

function register_main_menus() {

	register_nav_menus(

		array(

			'home-menu' => "Home Menu" ,

			'main-menu' => "Main Menu"

		)

	);

};

if (function_exists('register_nav_menus')) add_action( 'init', 'register_main_menus' );



// pagination

function pagination($prev = '&laquo;', $next = '&raquo;') {

    global $wp_query, $wp_rewrite;

    $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;

    $pagination = array(

        'base' => @add_query_arg('paged','%#%'),

        'format' => '',

        'total' => $wp_query->max_num_pages,

        'current' => $current,

        'prev_text' => __('Previous','themnific'),

        'next_text' => __('Next','themnific'),

        'type' => 'plain'

);

    if( $wp_rewrite->using_permalinks() )

        $pagination['base'] = user_trailingslashit( trailingslashit( remove_query_arg( 's', get_pagenum_link( 1 ) ) ) . 'page/%#%/', 'paged' );



    if( !empty($wp_query->query_vars['s']) )

        $pagination['add_args'] = array( 's' => get_query_var( 's' ) );



    echo paginate_links( $pagination );

};







//Breadcrumbs

function the_breadcrumb() {

	if (!is_home()) {



		echo '<a href="'. home_url().'">';

		echo '<i class="icon-home"></i> ';

		echo "</a> &raquo; ";

		if (is_category() || is_single()) {

		the_category(', ');

		if (is_single()) {

		echo " &raquo; ";

	echo short_title('...', 6);

	}

	} elseif (is_page()) {

	echo the_title();}

	}

}











function attachment_toolbox($size = thumbnail) {



	if($images = get_children(array(

		'post_parent'    => get_the_ID(),

		'post_type'      => 'attachment',

		'numberposts'    => -1, // show all

		'post_status'    => null,

		'post_mime_type' => 'image',

	))) {

		foreach($images as $image) {

			$attimg   = wp_get_attachment_image($image->ID,$size);

			$atturl   = wp_get_attachment_url($image->ID);

			$attlink  = get_attachment_link($image->ID);

			$postlink = get_permalink($image->post_parent);

			$atttitle = apply_filters('the_title',$image->post_title);



			echo '<p><strong>wp_get_attachment_image()</strong><br />'.$attimg.'</p>';

			echo '<p><strong>wp_get_attachment_url()</strong><br />'.$atturl.'</p>';

		}

	}

}


?>
