<?php 

/******* Post Types********/



	/*****************BANNER SUPERIOR***************/

function rosasSupport() {
    $labels = array(
        'name'                  => _x( 'Rosas', 'Post type general name', 'textdomain' ),
        'singular_name'         => _x( 'Rosa', 'Post type singular name', 'textdomain' ),
        'menu_name'             => _x( 'Rosas', 'Admin Menu text', 'textdomain' ),
        'name_admin_bar'        => _x( 'Rosas', 'Add New on Toolbar', 'textdomain' ),
        'add_new'               => __( 'Agregar', 'textdomain' ),
        'add_new_item'          => __( 'Añadir nuevo', 'textdomain' ),
        'new_item'              => __( 'Nuevo', 'textdomain' ),
        'edit_item'             => __( 'Seleccionar', 'textdomain' ),
        'view_item'             => __( 'Ver', 'textdomain' ),
        'all_items'             => __( 'Rosas', 'textdomain' ),


    );

    $args = array(
        'labels'             => $labels,
	 //'label'  => __( 'Banner Superior', 'textdomain' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'rosas' ),
        'capability_type'    => 'post',
		'map_meta_cap' => true,
        'has_archive'        => true,
        'hierarchical'       => false,
        'show_in_menu'      => true,
        'supports'            => array('title', 'thumbnail','editor')
			);

    register_post_type( 'rosas', $args );

}

add_action( 'init', 'rosasSupport' );

/** Portada **/

function portadaSupport() {
    $labels = array(
        'name'                  => _x( 'Portada', 'Post type general name', 'textdomain' ),
        'singular_name'         => _x( 'Portada', 'Post type singular name', 'textdomain' ),
        'menu_name'             => _x( 'Portada', 'Admin Menu text', 'textdomain' ),
        'name_admin_bar'        => _x( 'Portada', 'Add New on Toolbar', 'textdomain' ),
        'add_new'               => __( 'Agregar', 'textdomain' ),
        'add_new_item'          => __( 'Nuevo', 'textdomain' ),
        'new_item'              => __( 'Nuevo', 'textdomain' ),
        'edit_item'             => __( 'Editar', 'textdomain' ),
        'view_item'             => __( 'Ver', 'textdomain' ),
        'all_items'             => __( 'Portada', 'textdomain' ),
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
	  		'menu_icon'					 =>'dashicons-images-alt2',
        'rewrite'            => array( 'slug' => 'portada' ),
        'capability_type'    => 'post',
	  	  'map_meta_cap'			 => true,
        'has_archive'        => true,
        'hierarchical'       => false,
        'show_in_menu'       => true,
	  		'menu_position'	 => 3,
        'supports'           => array( 'title','editor','thumbnail','post-formats')

    );
    register_post_type( 'portada', $args );
}


add_action( 'init', 'portadaSupport' );







/** Newsletter **/

function newsletterSupport() {
    $labels = array(
        'name'                  => _x( 'Newsletters', 'Post type general name', 'textdomain' ),
        'singular_name'         => _x( 'Newsletter', 'Post type singular name', 'textdomain' ),
        'menu_name'             => _x( 'Newsletter', 'Admin Menu text', 'textdomain' ),
        'name_admin_bar'        => _x( 'Newsletter', 'Add New on Toolbar', 'textdomain' ),
        'add_new'               => __( 'Agregar', 'textdomain' ),
        'add_new_item'          => __( 'Nuevo', 'textdomain' ),
        'new_item'              => __( 'Nuevo', 'textdomain' ),
        'edit_item'             => __( 'Editar', 'textdomain' ),
        'view_item'             => __( 'Ver', 'textdomain' ),
        'all_items'             => __( 'Newsletter', 'textdomain' ),
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
	  		'menu_icon'					 =>'dashicons-editor-table',
        'rewrite'            => array( 'slug' => 'newsletter' ),
        'capability_type'    => 'post',
	  	  'map_meta_cap'			 => true,
        'has_archive'        => true,
        'hierarchical'       => false,
        'show_in_menu'       => true,
	  		'menu_position'	 => 7,
        'supports'           => array( 'title','editor','thumbnail','post-formats')

    );
    register_post_type( 'newsletter', $args );
}


add_action( 'init', 'newsletterSupport' );



	

?>
