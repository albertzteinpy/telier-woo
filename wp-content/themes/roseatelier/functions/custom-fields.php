<?php


/* Social Media & Logo */

add_filter('admin_init', 'logo_register_fields');

function logo_register_fields()
{
    register_setting('general', 'logo', 'esc_attr');
    add_settings_field('logo', '<label for="logo">'.__('Logo' , 'logo' ).'</label>' , 'logo_settings_fields_html', 'general');
}

function logo_settings_fields_html()
{
    $value = get_option( 'logo', '' );
    echo '<input type="text" id="logo" name="logo" value="' . $value . '" />';
}

add_filter('admin_init', 'facebook_register_fields');

function facebook_register_fields()
{
    register_setting('general', 'facebook', 'esc_attr');
    add_settings_field('facebook', '<label for="facebook">'.__('Facebook' , 'facebook' ).'</label>' , 'facebook_settings_fields_html', 'general');
}

function facebook_settings_fields_html()
{
    $value = get_option( 'facebook', '' );
    echo '<input type="text" id="facebook" name="facebook" value="' . $value . '" />';
}


add_filter('admin_init', 'instagram_register_fields');

function instagram_register_fields()
{
    register_setting('general', 'instagram', 'esc_attr');
    add_settings_field('instagram', '<label for="instagram">'.__('Instagram' , 'instagram' ).'</label>' , 'instagram_settings_fields_html', 'general');
}

function instagram_settings_fields_html()
{
    $value = get_option( 'instagram', '' );
    echo '<input type="text" id="instagram" name="instagram" value="' . $value . '" />';
}


add_filter('admin_init', 'youtube_register_fields');

function youtube_register_fields()
{
    register_setting('general', 'youtube', 'esc_attr');
    add_settings_field('youtube', '<label for="youtube">'.__('YouTube' , 'youtube' ).'</label>' , 'youtube_settings_fields_html', 'general');
}

function youtube_settings_fields_html()
{
    $value = get_option( 'youtube', '' );
    echo '<input type="text" id="youtube" name="youtube" value="' . $value . '" />';
}



/**   DATOS DE CONTACTO  **/


add_filter('admin_init', 'telefono_register_fields');

function telefono_register_fields()
{
    register_setting('general', 'telefono', 'esc_attr');
    add_settings_field('telefono', '<label for="telefono">'.__('Teléfono' , 'telefono' ).'</label>' , 'telefono_settings_fields_html', 'general');
}

function telefono_settings_fields_html()
{
    $value = get_option( 'telefono', '' );
    echo '<input type="text" id="telefono" name="telefono" value="' . $value . '" />';
}

add_filter('admin_init', 'correo_register_fields');

function correo_register_fields()
{
    register_setting('general', 'correo', 'esc_attr');
    add_settings_field('correo', '<label for="correo">'.__('Correo' , 'correo' ).'</label>' , 'correo_settings_fields_html', 'general');
}

function correo_settings_fields_html()
{
    $value = get_option( 'correo', '' );
    echo '<input type="text" id="correo" name="correo" value="' . $value . '" />';
}


add_filter('admin_init', 'domicilio_register_fields');

function domicilio_register_fields()
{
    register_setting('general', 'domicilio', 'esc_attr');
    add_settings_field('domicilio', '<label for="domicilio">'.__('Domicilio' , 'domicilio' ).'</label>' , 'domicilio_settings_fields_html', 'general');
}

function domicilio_settings_fields_html()
{
    $value = get_option( 'domicilio', '' );
    echo '<input type="text" id="domicilio" name="domicilio" value="' . $value . '" />';
}


/**   CAMPOS PARA TIPOS DE ENTRADA **/

add_action('admin_menu', 'crearCampos');

	function crearCampos(){

add_meta_box('irA','Botón IR A <small>Si este campo está vacio usará el permalink del post</small>','irA', ['newsletter','post','page','entrevista','trayecto23','cuadernos','cine-salon'],'normal','high');
add_meta_box('subtitulo','Subtitulo','subtitulo', ['newsletter','post','page','entrevista','trayecto23','cuadernos','cine-salon'],'advanced','high');

	}


	add_action('edit_form_after_title', function() {
		    global $post, $wp_meta_boxes;
		    do_meta_boxes(get_current_screen(), 'advanced', $post);
		    unset($wp_meta_boxes[get_post_type($post)]['advanced']);
		});


function irA(){
		global $post;
		$irA = get_post_meta($post->ID, 'irA', true);
		$textoBoton = get_post_meta($post->ID, 'textoBoton', true);
		?>
		<label for="tituloBoton" style="width:420px; text-align:left; font-size:12px; font-weight:bold; display:block; line-height:14px;">Texto del botón</label>
		<input type="text" value="<?php echo $textoBoton?>" name="textoBoton" id="textoBoton" style="width:100%" class="campomultilenguaje" /><br /><br />
		<label for="linkBoton" style="width:420px; text-align:left; font-size:12px; font-weight:bold; display:block; line-height:14px;">Hipervínculo del botón</label>
		<input type="text" value="<?php echo $irA?>" name="irA" id="irA" style="width:100%" class="campomultilenguaje" />
		<?php
		}




			function creditosBloque(){
				global $post;
				$creditos = get_post_meta($post->ID, 'creditos', true);
				?>
					<input type="text" name="creditos" id="creditos" value="<?php echo $creditos?>" style="width:100%" class="campomultilenguaje" />
				<?php
			}


	function subtitulo(){
		global $post;
		$subtitulo = get_post_meta($post->ID, 'subtitulo', true);
		?>
		<input type="text" value="<?php echo $subtitulo ?>" name="subtitulo" id="subtitulo" style="width:100%" class="campomultilenguaje" />
		<?php
		}

		function idPeli(){
			global $post;
			$idPeli = get_post_meta($post->ID, 'idPeli', true);
			?>
			<input type="text" value="<?php echo $idPeli ?>" name="idPeli" id="idPeli" style="width:100%;" />
			<?php
			}


		add_action( 'save_post', 'save_meta_details' );


function save_meta_details( $post_id ) {
    global $post;

    if(!empty($post)){
		
		if( $post->post_type == 'post' ) {
		   if( isset($_POST['irA']) ) { update_post_meta( $post->ID, 'irA', $_POST['irA'] );}
				if( isset($_POST['subtitulo']) ) { update_post_meta( $post->ID, 'subtitulo', $_POST['subtitulo'] );}
					     
	}
			if( $post->post_type == 'entrevista' ) {
			   if( isset($_POST['irA']) ) { update_post_meta( $post->ID, 'irA', $_POST['irA'] );}
				 if( isset($_POST['subtitulo']) ) { update_post_meta( $post->ID, 'subtitulo', $_POST['subtitulo'] );}

	}


	if( $post->post_type == 'newsletter' ) {
		   if( isset($_POST['irA']) ) { update_post_meta( $post->ID, 'irA', $_POST['irA'] );}
			 	if( isset($_POST['subtitulo']) ) { update_post_meta( $post->ID, 'subtitulo', $_POST['subtitulo'] );}
	}
	


}
}



?>