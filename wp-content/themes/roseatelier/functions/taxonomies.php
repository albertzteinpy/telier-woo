<?php

/* Taxonomies */
function be_register_taxonomies() {

  $taxonomies = array(
					array(
							'slug'         => 'entrevistador',
							'single_name'  => 'En colaboración con',
							'plural_name'  => 'En colaboración con',
							'post_type'    => ['entrevista','colaboracion'],
							'hierarchical' => false,
						),

						array(
							'slug'         => 'año',
							'single_name'  => 'Año',
							'plural_name'  => 'Año',
							'post_type'    => ['integrantes','entrevista'],
							'hierarchical' => false,
						),

						array(
							'slug'         => 'pais',
							'single_name'  => 'País',
							'plural_name'  => 'País',
							'post_type'    => ['integrantes','entrevista','festival', 'trayecto23'],
							'hierarchical' => false,
						),

						array(
							'slug'         => 'pais-copro',
							'single_name'  => 'Coproducción',
							'plural_name'  => 'Coproducción',
							'post_type'    => 'entrevista',
							'hierarchical' => false,
						),
	  			
	  					array(
							'slug'         => 'rol',
							'single_name'  => 'Rol',
							'plural_name'  => 'Rol',
							'post_type'    => ['integrantes','entrevista'],
							'hierarchical' => true,
						),

						array(
						 'slug'         => 'alianzas',
						 'single_name'  => 'Rol',
						 'plural_name'  => 'Rol',
						 'post_type'    => ['patrocinador','colaboracion'],
						 'hierarchical' => true,
					 ),
	  					array(
						 'slug'         => 'cuadernos',
						 'single_name'  => 'Rol',
						 'plural_name'  => 'Rol',
						 'post_type'    => 'cuadernos',
						 'hierarchical' => true,
					 ),
  );

  foreach( $taxonomies as $taxonomy ) {
    $labels = array(
      'name' => $taxonomy['plural_name'],
      'singular_name' => $taxonomy['single_name'],
      'search_items' =>  'Buscar ' . $taxonomy['plural_name'],
      'all_items' => 'Todos los ' . $taxonomy['plural_name'],
      'parent_item' => 'Parent ' . $taxonomy['single_name'],
      'parent_item_colon' => 'Parent ' . $taxonomy['single_name'] . ':',
      'edit_item' => 'Editar ' . $taxonomy['single_name'],
      'update_item' => 'Actualizar ' . $taxonomy['single_name'],
      'add_new_item' => 'Añadir nuevo ' . $taxonomy['single_name'],
      'new_item_name' => 'Nuevo ' . $taxonomy['single_name'] . ' Name',
      'menu_name' => $taxonomy['plural_name']
    );

    $rewrite = isset( $taxonomy['rewrite'] ) ? $taxonomy['rewrite'] : array( 'slug' => $taxonomy['slug'] );
    $hierarchical = isset( $taxonomy['hierarchical'] ) ? $taxonomy['hierarchical'] : true;

    register_taxonomy( $taxonomy['slug'], $taxonomy['post_type'], array(
      'hierarchical' => $hierarchical,
      'labels' => $labels,
      'show_ui' => true,
      'query_var' => true,
      'rewrite' => $rewrite,
      'show_in_nav_menus' => true,
			'show_admin_column' => true,

    ));
  }

}
add_action( 'init', 'be_register_taxonomies' );
function curPageURL() {
 $pageURL = 'http';
 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 }
 return $pageURL;
}

add_action('restrict_manage_posts', 'tsm_filter_post_type_by_taxonomy');
function tsm_filter_post_type_by_taxonomy() {
	global $typenow;
	$post_type = ['hostingmusic','sesiones','Playlist']; // change to your post type
	$taxonomy  = 'mood'; // change to your taxonomy
	if ($typenow == $post_type) {
		$selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
		$info_taxonomy = get_taxonomy($taxonomy);
		wp_dropdown_categories(array(
			'show_option_all' => __("Mostrar todos {$info_taxonomy->label}"),
			'taxonomy'        => $taxonomy,
			'name'            => $taxonomy,
			'orderby'         => 'name',
			'selected'        => $selected,
			'show_count'      => true,
			'hide_empty'      => true,
		));
	};
}

/**
 * Filter posts by taxonomy in admin
 * @author  Mike Hemberger
 * @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
 */

add_filter('parse_query', 'tsm_convert_id_to_term_in_query');
function tsm_convert_id_to_term_in_query($query) {
	global $pagenow;
	$post_type = ['hostingmusic','sesiones','Playlist']; // change to your post type
	$taxonomy  = 'mood'; // change to your taxonomy
	$q_vars    = &$query->query_vars;
	if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
		$term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
		$q_vars[$taxonomy] = $term->slug;
	}
}
?>