<?php /* Template Name: Rosas */ ?>
<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	

  <div id="home">
	  
	    <section class="destacada desroses">

	<?php 
	
	$thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');
 	$galeria = get_post_gallery($post->ID, false);
    $attachs = explode(",",$galeria['ids']);

    if (!empty($galeria)) { ?>
    	<ul class="rslides"z id="slider1" style="max-height:480px">
				 <?php
                 foreach($attachs as $idAttach):
                    $imgSlide = wp_get_attachment_url($idAttach);
                    $imgGal = get_post($idAttach); ?>
  					<li><img style="margin-top: -60px" src="<?php echo $imgSlide; ?>" alt=""></li>
				     <?php endforeach ;?>

</ul>
			
			
			
      
	  	
			  	<?php } else { ?>
			  	
			  	
			  		<div class="thumbportada" style="background-image: url(<?php echo $thumb[0];?>);">	</div>
			  	 
			  <?php } ?>
			
			 </section>
	  

	<section class="tagline rosas">
		<h1><?php the_title()?></h1>
		<div class="shortdesc">
		
			<p><?php echo apply_filters("the_content",strip_shortcodes($post->post_content));?></p>
			</div>
			  						
					<ul>
							
						
						
					<?php $rosas = get_posts(array('post_type'=>'rosas','posts_per_page'=>20, 'order'=>'ASC', 'orderby'=> 'date'));
					foreach ($rosas as $rosa):
						$nombre = $rosa->post_title;
						$thumb = wp_get_attachment_image_src(get_post_thumbnail_id($rosa->ID),'full');?>

						<li><div class="imagenrosa" style="background-image: url(<?php echo $thumb[0];?>)"></div>
							<h2><?php echo $nombre; ?></h2>
						</li>
						
			
					
					
					<?php endforeach;?>
					</ul>
		
			   <a href="/colecciones"> <span class="pedidos">Conoce las colecciones</span></a>
			   
					
	  </section>
	  
	  


		<?php endwhile;endif; ?>



<?php get_footer(); ?>


</div>