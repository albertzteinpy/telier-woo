<?php /* Template Name: Pedidos Urgentes */ ?>
<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>


<style>
.margtop {margin-top: 20px !important;}
form {width: 60%;}
@media screen and (min-width: 240px) and (max-width: 1040px){

	img.alignnone {width: 90% !important; height: auto !important;}
	section.destacada .thumbportada {width: 80%; min-height: 240px; margin: 0 auto; min-width: 320px;}
	form {width: 100%;}
}

</style>

  <div id="home">
	  

	<section class="tagline margtop">
		
				<h1><?php the_title()?></h1>
			  	<div class="clasica shortdesc" style="column-count:1" >
					<p><?php echo strip_shortcodes($post->post_content);?></p>
						<br>
					
					<?php echo do_shortcode('[contact-form-7 id="371" title="Pedidos Urgentes"]');?>
				</div>
			  
	  </section>


	  


		<?php endwhile;endif; ?>



<?php get_footer(); ?>


</div>