<div class="principal">
	<ul>
 <?php
		$menu = wp_get_nav_menu_items('Principal');
		$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
														foreach ($menu as $link) {
						$id = $link->object_id;
						if ($link->menu_item_parent == 0) {
								$post_slug = get_post($link->object_id);
							$url = $link->url;
					$active = ($url == $actual_link)? 'style="border-bottom: 2px solid #f5b6a1; font-weight: 600;"':'' ;
								?>
		<li>
			<a href="<?php echo $url; ?>" <?php echo $active; ?>>
				<?php echo $link->title; ?>
				</li>
			</a>
		<?php } } ?>
			</ul>
	 <ul style="margin-left: 20px; padding-top: 0px; margin-bottom: 40px;">
	<?php
		$menu = wp_get_nav_menu_items('Colecciones');
		$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
				foreach ($menu as $link) {
						$id = $link->object_id;
						if ($link->menu_item_parent == 0) {
							$post_slug = get_post($link->object_id);
							$url = $link->url;
							$active = ($url == $actual_link)? 'style="border-bottom: 2px solid #f5b6a1; font-weight: 600;"':'' ;
								?>
		<li>
			<a href="<?php echo $url; ?>" <?php echo $active; ?>>
				<?php echo $link->title; ?>
				</li>
			</a>
		<?php } } ?>
	</ul>
</div>
<div class="copyright desk">
	<a href="<?php echo get_home_url() . '/ecommerce/colecciones';?>"><span class="pedidos" style="margin-bottom: 20px;"> Haz tu pedido aquí </span></a>
	<a href="<?php echo get_home_url() . '/preguntas-frecuentes';?>">Preguntas frecuentes</a> | <a href="<?php echo get_home_url() . '/facturacion';?>">Facturación</a><br><br>
	<a href="<?php echo get_home_url() . '/aviso-privacidad';?>">Aviso de privacidad</a> | <?php echo get_bloginfo("name") . ' &reg; ' . date('Y');?>
</div>		
