<div class="menu_mobile">
    <nav style="height: auto; overflow-y: scroll;">
      <ul>
      <?php
              $menu = wp_get_nav_menu_items('Principal');
              $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
              //print_r($menu);
                foreach ($menu as $link) :
                $id = $link->object_id;
                if ($link->menu_item_parent == 0) :
                    $post_slug = get_post($link->object_id);
                    $url = $link->url;
                    $active = ($url == $actual_link)? 'style="border-bottom: 2px solid  #f5b6a1; font-weight: 800;"':''; ?>
      <li <?php echo $active; ?>><a href="<?php echo $url; ?>"><?php echo $link->title; ?></a></li>
    <?php endif; endforeach; ?>
    </ul>

<ul style="padding-top: 0px; margin-bottom: 40px; margin-left: 20px">
	<?php
		$menu = wp_get_nav_menu_items('Colecciones');
		$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
				foreach ($menu as $link) {
						$id = $link->object_id;
						if ($link->menu_item_parent == 0) {
							$post_slug = get_post($link->object_id);
							$url = $link->url;
							$active = ($url == $actual_link)? 'style="border-bottom: 2px solid #f5b6a1; font-weight: 600;"':'' ;
								?>
		<li <?php echo $active; ?>>
			<a href="<?php echo $url; ?>">
				<?php echo $link->title; ?>
				</li>
			</a>
		<?php } } ?>
	</ul>
	<a href="<?php echo get_home_url() . '/ecommerce/colecciones';?>"><span class="pedidos" style="text-align: center;"> Haz tu pedido aquí </span></a>
	
	<ul style="width: 95%;">
	    
	    <li style="font-size: 8pt" <?php echo $active; ?>>
	        	<a href="<?php echo get_home_url() . '/preguntas-frecuentes';?>">Preguntas frecuentes</a>
	    </li>
	  
	     <li style="font-size: 8pt" <?php echo $active; ?>>
	        <a href="<?php echo get_home_url() . '/facturacion';?>">Facturación</a>
	    </li>
	        <li style="font-size: 8pt"  <?php echo $active; ?>>
	    <a href="<?php echo get_home_url() . '/aviso-privacidad';?>">Aviso de privacidad</a>
	    	    </li>
	</ul>
  </nav>
</div>
