<?php /* Template Name: Homepage */ ?>
<?php get_header(); ?>


  <div id="home">
	 
	    <section class="destacada homepage">
		  <?php 
				$portada = get_page_by_path("inicio");
				$titulo = $portada->post_title;
				$content = $portada->post_content;
				$thumb = wp_get_attachment_image_src(get_post_thumbnail_id($portada->ID),'full');
				$galeria = get_post_gallery($portada->ID, false);
				$attachs = explode(",",$galeria['ids']);

				if (!empty($galeria)) { ?>

			
			<ul class="rslides" id="slider1">
				 <?php
                 foreach($attachs as $idAttach):
                    $imgSlide = wp_get_attachment_url($idAttach);
                    $imgGal = get_post($idAttach); ?>
  					<li><img src="<?php echo $imgSlide; ?>" alt=""></li>
				     <?php endforeach ;?>
			</ul>
      
	  	
			  	<?php } else { ?>
			  		<div class="thumbportada" style="background-image: url(<?php echo $thumb[0];?>);"></div>
			  <?php } ?>
	  </section>
	  
	<section class="tagline">
		<h1>Bienvenidos a nuestro taller</h1>
	  	  	<div class="shortdesc">
				<h2><?php echo strip_shortcodes($portada->post_content);?></h2>
					
			  </div>
	  
	  </section>
	  
	  <section class="partners">
		   <div class="servicios">
				  	 
				  
						<div class="beneficios">
							<div class="columna">
								<span class="fa fa-clock-o"></span>
								<p>Programa la fecha de envío</p>
							</div>
							<div class="columna">
								<span class="fa fa-users"></span>
								<p>Si necesitas ayuda especial con tu pedido, ponte en contacto con nosotros:<br>
								<?php echo get_option('telefono');?>
								<?php echo get_option('correo');?></p>
							</div>
							<div class="columna">
								<span class="fa fa-lock"></span>
								<p>Tu compra y tus datos están protegidos</p>

							</div>
						</div>
			   <a href="<?php echo get_home_url() . '/ecommerce/colecciones';?>"> <span class="pedidos">Haz tu pedido</span></a>
			   
				  <div class="powered">
						  <div class="fl"><img alt="PayPal" src="http://roseatelier.mx/wp-content/uploads/2018/06/PayPal.png"></div>
						  <div class="fr"><img alt="Mailboxes" src="http://roseatelier.mx/wp-content/uploads/2018/06/logo-01.png"></div>
				  </div>
			  
			   
			</div>
	  </section>
	  
	    <section class="partners ultimoslide">
			
			
	  	  	<div class="shortdesc">
				<div class="redes">
					  <div class="fl"><h1>Síguenos</h1></div>
					  <div class="fr">
						<a href="http://instagram.com/<?php echo get_option ('instagram');?>"  target="_blank"><span class="fa fa-instagram"></span>  @_roseatelier</a>
						<a href="http://facebook.com/<?php echo get_option ('facebook'); ?>" target="_blank"><span class="fa fa-facebook"></span>  /roseateliermx</a>
					  </div>
				</div> 
			 </div>
					
					
			
<!-- Begin MailChimp Signup Form -->
<div id="mc_embed_signup">
<form action="https://roseatelier.us17.list-manage.com/subscribe/post?u=5249266e550d8d8d6c605fb6a&amp;id=9f7034ddca" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">	<h2>Déjanos saber un poco más de ti y de la persona a la que te gustaría sorprender.</h2>

<div class="mc-field-group">
	<label for="mce-EMAIL">Correo  <span class="asterisk">*</span>
</label>
	<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
</div>
<div class="mc-field-group">
	<label for="mce-FNAME">Nombre </label>
	<input type="text" value="" name="FNAME" class="" id="mce-FNAME">
</div>
<div class="mc-field-group">
	<label for="mce-LNAME">Código Postal </label>
	<input type="text" value="" name="LNAME" class="" id="mce-LNAME">
</div>
<div class="mc-field-group">
	<label for="mce-MMERGE5">Tu edad </label>
	<select name="MMERGE5" class="" id="mce-MMERGE5">
	<option value=""></option>
	<option value="Menor de 20">Menor de 20</option>
<option value="De 21 a 30">De 21 a 30</option>
<option value="De 31 a 40">De 31 a 40</option>
<option value="De 41 a 50">De 41 a 50</option>
<option value="De 51 a 60">De 51 a 60</option>
<option value="Mayor de 60">Mayor de 60</option>

	</select>
</div>
<div class="mc-field-group">
	<label for="mce-MMERGE4">¿A quién quisieras regalarle? </label>
	<select name="MMERGE4" class="" id="mce-MMERGE4">
	<option value=""></option>
	<option value="A mi pareja">A mi pareja</option>
<option value="Algún familiar">Algún familiar</option>
<option value="Amig@s">Amig@s</option>
<option value="Para mí">Para mí</option>

	</select>
</div>
<div class="mc-field-group">
	<label for="mce-MMERGE6">La edad de quien recibe </label>
	<select name="MMERGE6" class="" id="mce-MMERGE6">
	<option value=""></option>
	<option value="Menor de 20">Menor de 20</option>
<option value="De 21 a 30">De 21 a 30</option>
<option value="De 31 a 40">De 31 a 40</option>
<option value="De 41 a 50">De 41 a 50</option>
<option value="De 51 a 60">De 51 a 60</option>
<option value="Mayor de 60">Mayor de 60</option>

	</select>
</div>
<div class="mc-field-group">
	<label for="mce-MMERGE3">Género </label>
	<select name="MMERGE3" class="" id="mce-MMERGE3">
	<option value=""></option>
	<option value="Mujer">Mujer</option>
<option value="Hombre">Hombre</option>

	</select>
</div>
<div class="indicates-required"><span class="asterisk">*</span> Campos obligatorios</div>
	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_5249266e550d8d8d6c605fb6a_9f7034ddca" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Enviar datos" name="Enviar datos" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
</div>

<!--End mc_embed_signup-->

			
			

	  </section>
	  
	  
	  
	  


<?php get_footer(); ?>

</div>
